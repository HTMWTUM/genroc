.. _Bibliography:


.. only:: html

   ============
   Bibliography
   ============


.. [Meyn18] Meyn, L. A., “Rotorcraft Optimization Tools: Incorporating Rotorcraft Design 
   Codes into Multi- Disciplinary Design, Analysis, and Optimization,” American Helicopter 
   Society (AHS) International Technical Meeting on Aeromechanics Design for Transformative 
   Vertical Flight 2018, San Francisco, California, USA, Jan 16, 2018.
   https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20180000894.pdf

.. [Johnson15] Johnson, W., “NDARC - NASA Design and Analysis of Rotorcraft,”
    NASA/TP-2015-218751, NASA Ames Research Center, Moffett Field, CA United States, 
    April 1, 2015. http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20150021267.pdf
        
.. [Gray19] Gray, J. S., Hwang, J. T., Martins, J. R. R. A., Moore, K. T., & Naylor, 
	B. A. (2019). OpenMDAO: An Open-Source Framework for Multidisciplinary Design, Analysis, 
	and Optimization. Structural and Multidisciplinary Optimization, 59(4), 
	1075–1104. https://doi.org/10.1007/s00158-019-02211-z
	
.. [Johnson13] Johnson, W. (2013). Rotorcraft Aeromechanics. 
	Cambridge University Press. https://doi.org/10.1017/CBO9781139235655

  
