.. _Introduction:


Introduction
++++++++++++
This is the user guide for genroc. This package is a calibration tool 
for the rotor performance models of NDARC. 
The NASA Design and Analysis of RotorCraft (NDARC) [Johnson15]_ is a conceptual
aircraft design software tool with a primary focus on rotorcraft. It is
written in the Fortran programming language and uses namelist-based input. 
NDARC uses extended Momentum Theory for induced rotor power modelling and a mean drag coefficient
for profile power modelling. NDARC employs parametric submodels for
the  non-ideal induced power factor :math:`\kappa_i=f(\frac{C_T}{{\sigma}}, \mu, \mu_z)` 
and the mean drag coefficient :math:`c_{d,mean}=f(\frac{C_T}{{\sigma}}, \mu, \mu_z, M_{tip}, ...)`.
These submodels aim to model effects such as stall or compressibility. The current 
method to calibrate these parametric submodels is to use the Excel sheet
and procedure described in the rotor calibration manual. 
This requires executing specific CAMRAD sweeps and manually adjusting the
model parameters in the Excel sheet. This tool aims to make the calibration 
process easier, more flexible and more consistent. The tool mainly consists
of three Python classes.

The :obj:`~genroc.class_NDARCrotor.NDARCrotor` 
class includes all relevant NDARC rotor power equations. Therefore, NDARC 
does not have to be called when using the tool. The 
:obj:`~genroc.class_Calibration.NDARC_Rotor_Calibration` class includes all
functions necessary to execute the calibration process. The calibration uses the
SimpleGADriver genetic algorithm from OpenMDAO [Gray19]. The OpenMDAO problem definition 
requires the target value to be calculated by a child class of OpenMDAO's
:obj:`~openmdao.api.ExplicitComponent`. Here the :obj:`~genroc.class_Target.RotorTarget`
class handles this task. 

The tool has been discussed in more detail in the VFS 79 Paper 
"Fuel Cell Sizing for a UAV with Intermeshing Rotors using a Genetic Algorithm for NDARC Rotor Performance Calibration".

The following figure from the paper shows the architetcure of the tool:



.. raw:: latex

    \clearpage
    
.. only:: latex

	.. image:: includes/architecture.pdf
	   :width: 500
    
.. only:: html

	.. image:: includes/architecture.png
	   :width: 500


.. raw:: latex

    \clearpage

.. _Installation_Guide:

Installation Guide
++++++++++++++++++++

.. _Installation:


Installation
^^^^^^^^^^^^
To install GeNRoC follow these steps::

	git clone https://gitlab.lrz.de/HTMWTUM/genroc.git
	cd genroc
	python setup.py install	

The following sections are taken from the RCOtools documentation:

.. _OpenMDAO_Installation:


OpenMDAO Installation
^^^^^^^^^^^^^^^^^^^^^

OpenMDAO can be installed using the following command::

	pip install openmdao

or through `Anaconda Python <https://www.anaconda.com>`_ with::
	
	conda install openmdao


.. _OpenMDAO_Parallel:


OpenMDAO Parallel Processing Support
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

OpenMDAO uses MPI and PETSc for parallel processing. 
Installing an OpenMDAO compatible MPI and PETSc can be quite difficult, however 
`Anaconda Python <https://www.anaconda.com>`_ now allows installation of these utilities 
using their ``conda`` command. Before installation OpenMDAO should be installed and
"conda-forge" should be added to the conda "channels" list. To add "conda-forge",
enter the following commands in a terminal window::

	conda config --add channels conda-forge
	conda config --set channel_priority strict

Once access to "conda-forge" is setup, MPI and PETSc can be installed using the 
following commands::

	conda install openmpi mpi4py
	conda install petsc petsc4p


**Note: OpenMPI Not Available for Windows**  
OpenMPI does not currently seem to be available for Windows systems.
Windows users might consider using the Ubuntu subsystem.

.. _Generate_Documentation:


Generate Documentation
^^^^^^^^^^^^^^^^^^^^^^

GeNRoC documentation can be generated using the python package 
`Sphinx <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_
and the files in the ``docs``
directory of the rcotools distribution by using the following commands::

	cd path_to_docs
	make html BUILDDIR=directory_path

if ``BUILDDIR`` argument is not provided, the HTML documents will be generated in
the ``docs/build/html`` directory.  Different document formats can be
built, just execute ``make`` without an argument to see which format targets are
available.


**LaTex PDF generation:**
If you have TeX and LaTeX available, run ``make latexpdf``, which runs
the LaTeX builder and invokes the pdfTeX toolchain for you. The PDF 
document will be generated in the ``docs/build/latex`` directory.


.. _Reference_Data:


Reference Data
+++++++++++++++
The most crucial part of the calibration process is the preparation of the
reference data. Since the genetic algorithm cannot extrapolate trends 
based on engineering knowledge, the user must ensure that the reference data 
includes all relevant flight regimes. In general, the more different flight 
cases can be considered, the better. For more information see the NDARC rotor
calibration manual by Johnson. 
While this tool is not restricted to the sweeps defined in the manual, they can serve as 
a suitable selection of flight cases for conventional helicopters.
The data created with CAMRAD or a different software has to be formatted in a .csv file to be read in as a dataframe.
The .csv file should have the following columns:

``induced``, ``profile``, ``CTs``, ``mu``, ``muz``, ``Mtip`` (optional), ``offset`` (optional), 
``kappa``, ``cdmean`` (or ``CPs_ind``, ``CPs_pro`` or ``CPs``), ``i_plot`` (optional), ``weighting`` (optional)

``CTs``, ``mu``, ``muz``, ``Mtip`` (optional), ``offset`` (optional) define the flight case. 
If ``Mtip`` is not given a default value has to be set in the initialization of the
:obj:`~genroc.class_NDARCrotor.NDARCrotor` object. 
If the lift offset is not given, it is set to 0 for all cases. ``kappa`` and ``cdmean`` or the respective 
power coefficients are the values for each flight case.
The columns ``induced`` and ``profile`` are used to assign flight cases to calibration 
steps for the induced and profile model respectively. The names assigned to the flight 
cases correspond to the keys for each calibration step in the dictionary 
``dict_cal_order`` (see :ref:`Calibration_Procedure`). If one flight case is to be used in multiple
calibration steps of the same model or just different configurations are
desired, the keys must be seperated with '+'.
Flight cases that have the same integer number in the column ``i_plot`` will be plotted together
in post-processing. 
The ``weighting`` column can optionally be used to give flight cases different weighting
during error calculation.

The file ``Example/UH60A/UH60A_Calibration_Validation.csv`` provides an example of
reference data. In this case only one of kappa or cdmean are given for each
flight case because the data was digitized from plots in the NDARC Validation paper.
Normally both would be populated for each case.


.. _Calibration_Procedure:


Calibration Procedure (Example)
+++++++++++++++++++++++++++++++

Calibration_Process_exmpl.py_ provides an example of a full calibration of a UH60A model using
reference data digitized from the NDARC validation paper 
(``Example/UH60A/UH60A_Calibration_Validation.csv``). After reading in the .csv file
as a pandas dataframe, a distinction is made between whether the code is executed in the terminal
for a full calibration or only for post-processing in an IDE (here Spyder). This enables seperate execution
on cluster CPUs from post-processing. Alternatively this could of course be done with seperate scripts. For calibration first an instance of 
:obj:`~genroc.class_NDARCrotor.NDARCrotor` is created. The minimum 
amount of input is the solidity ``sigma`` and the (equivalent) linear blade
twist ``twistL``. Then the custom linear stall model is activated (see :ref:`Stall_Models`).
Here the calibration setup is done in the function :func:`~calibration`. 

An important part of the calibration procedure is the setup of the calibration steps. The 
dictionary ``dict_cal_order`` defines which parameters are calibrated using which
flight cases in which order. The keys of the top-level dictionary are 
'induced' and 'profile'. For each of the models a sub-dictionary is defined.
Its keys correspond to the keys in the ``induced`` and ``profile`` columns of the 
reference .csv file (see :ref:`Reference_Data`). When setting up ``dict_cal_order`` several factors have to be 
taken into account. For an overview over which parameters correspond to which flight
regimes see the NDARC rotor calibration manual by Johnson. In general, the 
amount of parameters that can be calibrated is dependant on the number of 
reference flight cases. Should limited data be available the number of parameters
should be reduced as well.

Through testing it was found that for conventional helicopters, such as the UH60A,
seperating flight cases into hover and edgewise flight for the ``induced`` model improves calibration and reduces execution time
since hover and edgewise flight are completely independent equations in this case.
The stall model for profile power calculation leads to an interdependency 
between hover and edgewise flight, which is why executing only one calibration step
using all flight cases was found to give the best results. If a key in ``dict_cal_order`` 
is set to 'all' the tool will automatically use all flight cases except those
that have 'none' in the respective column or that have a weighting of ``0``.
Should a repeat run of one of the steps be desired add '_2' to the end of the key
in ``dict_cal_order`` and the tool will repeat this step after setting all default
parameter values in OpenMDAO to the result of the first run. In most cases 
wehere a conventional helicopter is being calibrated the ``dict_cal_order`` from
Calibration_Process_exmpl.py_ can be used as a basis.

When this many parameters are adjusted at once by the algorithm, it is especially
important to have sensible constraints set for all parameters. The function
:func:`~genroc.class_NDARCrotor.NDARCrotor.dict_pars_MDAO_default` initializes
the dictionary ``dict_pars_MDAO`` with default values and constraints for all parameters. 
Should different values be desired ``dict_pars_MDAO`` can be given to
:obj:`~genroc.class_Calibration.NDARC_Rotor_Calibration` as a keyword
argument.

After setting up ``dict_cal_order`` the instance of 
:obj:`~genroc.class_Calibration.NDARC_Rotor_Calibration` is created. 
Before the actual calibration is started some parameters such as ``Ki_hover`` 
and ``d0_hel`` can be set directly if the reference data allows it.
For this purpose the function 
:func:`~genroc.class_Calibration.NDARC_Rotor_Calibration.set_vals_pre_calc`
can be used. If no list with parameters is input all possible parameters are set.
This function can also be extended to allow for additional parameters.

Next, the calibration is executed. The definition of the SimpleGAdriver parameters 
can be found on the OpenMDAO website. After the calibration is completed the 
comparison dataframe is saved for post-processing/debugging and the function
:func:`~genroc.class_Calibration.NDARC_Rotor_Calibration.set_axial`
is executed. This function sets parameter values of axial parameters according to
the NDARC rotor calibration manual. Lastly the results are saved in different
formats for use in NDARC and post-processing. 

If ``plot_only`` is set to ``True`` the script is executed in post-processing mode in this example.
The :obj:`~genroc.class_NDARCrotor.NDARCrotor` instance is initialized
from the .yaml file saved after the calibration run. A new instance of 
:obj:`~genroc.class_Calibration.NDARC_Rotor_Calibration` is created and
the plotting function :func:`~genroc.class_Calibration.NDARC_Rotor_Calibration.plot_cases`
is called to view the results for ``kappa`` and ``cdmean``.


.. raw:: latex

    \clearpage

.. literalinclude:: ../../Example/Calibration_Process_exmpl.py
   :caption: Example calibration using digitized data from NDARC Validation paper for UH60A.
   :name: Calibration_Process_exmpl.py
   

.. _NDARCrotor:


NDARCrotor
+++++++++++
The :obj:`~genroc.class_NDARCrotor.NDARCrotor` class is the basis 
of the calibration tool. This class enables the use of genroc 
without ever having to call NDARC. This vastly improves execution time 
since only the necessary power equations have to be calculated and no 
file I/O is needed during execution. At its core the functions
:func:`~genroc.class_NDARCrotor.NDARCrotor.calc_induced` and 
:func:`~genroc.class_NDARCrotor.NDARCrotor.calc_profile` include
the submodels used in NDARC's extended Momentum Theory to calculate 
:math:`\kappa_i` and :math:`c_{d,mean}` respectively. The ideal inflow
factor :math:`\lambda_{ideal}` is caluclated using either
:func:`~genroc.class_NDARCrotor.NDARCrotor.ideal_inflow` or 
:func:`~genroc.class_NDARCrotor.NDARCrotor.ideal_inflow_twin` 
depending on whether a twin configuration is analysed (for a twin configuration 
set ``NDARCrotor.twin`` to ``True``). With this the induced 
power coefficient :math:`\frac{C_P}{\sigma}_{ind}=\kappa_i C_T \lambda_{ideal}`
can be calculated. The profile power coefficient 
:math:`\frac{C_P}{\sigma}_{pro}=\frac{1}{8}c_{d,mean}F_P`
is calculated with :func:`~genroc.class_NDARCrotor.NDARCrotor.calc_FP`.
If only the power coefficients are present in the reference data 
:math:`\kappa_i` and :math:`c_{d,mean}` can be calculated in reverse order.
All equations are copied directly from the NDARC source code. The only exceptions 
are the calculation of the lift offset and the thrust coefficient of the 'other' 
rotor in twin configurations. In the NDARC source code these rely on the trim i
teration. Here both parameters have to be given in the reference data (see :ref:`Reference Data`).

In addtion :obj:`~genroc.class_NDARCrotor` includes a dictionary
containing default values for all relevant NDARC parameters. The initialization
of these parameters can also be done by reading from a .soln or .acd NDARC 
output file using the function 
:func:`~genroc.class_NDARCrotor.NDARCrotor.read_acd`. 
The function :func:`~genroc.class_NDARCrotor.NDARCrotor.write_namelist` returns a 
string that can be copied and pasted directly into the Rotor section of a
NDARC namelist file. 
:func:`~genroc.class_NDARCrotor.NDARCrotor.write_dict` and 
:func:`~genroc.class_NDARCrotor.NDARCrotor.read_dict` are used to save
and read the state of all :obj:`~genroc.class_NDARCrotor.NDARCrotor`
attributes e.g. after execution on a cluster and postprocessing on a different
machine.

.. _Stall_Models:


Stall Model
^^^^^^^^^^^^
.. image:: includes/stall_johnson.png
   :width: 400
   :alt: Stall mdoels, from [Johnson13] 

The stall model implemented in NDARC is based on a list of
:math:`\left. \frac{C_T}{\sigma} \right|_{stall}` at the corresponding 
values of :math:`\mu_{stall}`. To achieve a sensibly smooth variation of
:math:`\left. \frac{C_T}{\sigma} \right|_{stall}` the length of this list 
``n_stall`` should be at least around 5. Each of the points in the list
would have to be an independent variable in the calibration which would also negate
the desired smooth variation. Therefore stall surrogate models are implemented
that reduce the number of independant variables and ensure a smooth variation.
These stall models can be activated by setting the attribute ``MODEL_stall_custom``
of :obj:`~genroc.class_NDARCrotor.NDARCrotor`. ``0`` deactivates the custom stall model,
``1`` activates the Harris stall model and ``2`` activates the linear stall model.

The simplest model is the linear stall model ``2`` which has a linear variation from 
``CTs_stall_hov`` (:math:`\left. \frac{C_T}{\sigma} \right|_{stall,\mu=0}`) to 
``CTs_stall_04`` (:math:`\left. \frac{C_T}{\sigma} \right|_{stall,\mu=0.4}`) and a constant
variation from there. It therefore reduces the list for 
:math:`\left. \frac{C_T}{\sigma} \right|_{stall}` to two independant variables.

.. math::
	\left. \frac{C_T}{\sigma} \right|_{stall}=\left. \frac{C_T}{\sigma} \right|_{stall,\mu=0}+\frac{\left. \frac{C_T}{\sigma} \right|_{stall,\mu=0.4}-\left. \frac{C_T}{\sigma} \right|_{stall,\mu=0}}{0.4}\mu

The Harris stall model as described by Johnson [Johnson13] further reduces 
this to only one independant variable ``Cl_max_harris``. This has the disadvantage that
the algorithm cannot influence stall in hover and cruise independently which might
reduce calibration accuracy in some cases.

.. math::
	\left. \frac{C_T}{\sigma} \right|_{stall}=\frac{c_{l,max,harris}}{6}\frac{1-\mu^2+\frac{9}{4}\mu^4}{1+\frac{8}{3}\mu+\frac{3}{2}\mu^2}

If different stall characteristics are expected or desired that cannot be modeled 
with either model additional models can be implemented with relatively few
changes to the code.
Note: A model similar the linear model but with a square function instead of linear :func:`~genroc.class_NDARCrotor.NDARCrotor.model_stall_square` 
has been implemented and it is possible to use the shape of the stall behaviors of the NDARC reference models and only use :math:`f_{stall}` for scaling (:func:`~genroc.class_NDARCrotor.NDARCrotor.model_stall_square`).

.. _Limitations:


Limitations
+++++++++++

GeNRoC currently has some missing or limited features:

* **Duct model**: No ducted fan equations are present in :obj:`~genroc.class_NDARCrotor`.

* **Multirotor interference**: Only twin rotor interference is currently implemented.

* **Ground effect**: No ground effect equations are implemented.

* **Lift Offset**: Since no trim iterations are implemented and all equations are kept dimensionless, no lift offset calculation is implemented either. Therefore lift offset has to be input if it is relevant.

* **Twin models**: The thrust coefficient of the second rotor CT2 has to be input and cannot be taken from the trim iteration. Also it is assumed that both rotors are identical and :math:`\sigma_{R1}=\sigma_{R2}`.

.. _Validation:


Validation of NDARCrotor
++++++++++++++++++++++++

The Validation of all implemented NDARC equations is done using the example and 
reference models provided on the NDARC website in the script 
``Validation_NDARCrotor\NDARC_Rotor_Validation.py.``
Sweeps are executed with NDARC and the .perf files are read in using RCOtools
and :func:`~genroc.tools.parse_ndarc_perf.parse_perf`. The 
:obj:`~genroc.class_NDARCrotor.NDARCrotor` object is initialized 
by reading in the parameter values from the corresponding .acd file and
the comparison dataframe, that includes is calculated. The maximum error
between the output of NDARC (ref) and the calculated values of 
:obj:`~genroc.class_NDARCrotor.NDARCrotor` (ndarc). If all investigated 
flight cases show reasonably low errors then :obj:`~genroc.class_NDARCrotor.NDARCrotor`
can be viewed as validated. This has been executed using every model in the folder
``Validation_NDARCrotor``. The tested configurations are:

helicopter, tandem, tiltrotor, coaxial and coaxial compound with lift offset.

To double check the validity of calibration results this could also be done 
with the new NDARC model after calibration. 


