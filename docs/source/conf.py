# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# The master toctree document.
source_suffix = '.rst'
master_doc = 'index'

# -- Project information -----------------------------------------------------

project = r'genroc'
copyright = '2021, Victor Zappek'
author = 'Victor Zappek'
author_latex = r'Victor Zappek \\ Institute of Helicopter Technology \\ Technical University of Munich'
projdesc = 'Genetic NDARC Rotor Calibration'
doctitle = r'Documentation for the \\ Genetic NDARC Rotor Calibration tool'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc',
#              'sphinx.ext.intersphinx',  # Link to docs in other projects
              'sphinx.ext.mathjax',
              'sphinx.ext.ifconfig',
              'sphinx.ext.viewcode',  # Comment this line to suppress code
              ]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinxdoc'
# html_theme = 'alabasteu'
# html_theme = 'pyramid'

latex_logo = 'includes/HT_TUM.png'
latex_elements = {
    'preamble': r'''
\usepackage{makeidx} 
\usepackage[columns=1]{idxlayout} 
\makeindex
\setcounter{tocdepth}{2}\usepackage{placeins}
''',
  'extraclassoptions': 'openany,oneside'
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc,u'genroc_doc.tex', doctitle,
     author_latex, 'manual'),
]

# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, u'genroc', doctitle,
     [author], 1)
]


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, u'genroc', doctitle,
     author, u'genroc', projdesc, 'Miscellaneous'),
]


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
