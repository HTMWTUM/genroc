.. genroc documentation master file, created by
   sphinx-quickstart on Wed Dec 22 08:31:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========================================
Welcome to GeNRoC's documentation!
========================================

GeNRoC is a calibration tool for the induced and profile power model of
NDARC. It employs the genetic algorithm SimpleGADriver from
the open-source optimization framework OpenMDAO. All parameters of the
induced and profile submodels, including those for twin rotors, can be calibrated
using reference data, e.g. from CAMRAD. The methodology is derived from the 
NDARC rotor calibration manual. The necessary equations from the NDARC source
code are present in the python package so that NDARC is not required for execution.
For fast execution the calibration can be run in parallel on multiple CPUs.

**Currently supported NDARC version: v1.16**

.. _User_Guide:

`User Guide`_
=============


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   userguide
   bib

.. _Source Code Documentation:

`Source Code Documentation`_
============================


.. toctree::
   :maxdepth: 2


genroc.class_NDARCrotor
+++++++++++++++++++++++++++++
:obj:`~genroc.class_NDARCrotor.NDARCrotor` class includes all equations
necessary to run the calculations without calling NDARC itself. In addition
functions for I/O from and to different file formats are included. 
See :ref:`NDARCrotor` section in :ref:`User_Guide` for more details.

.. automodule:: genroc.class_NDARCrotor
   :members:
   :private-members:


genroc.class_Calibration
++++++++++++++++++++++++++++++
The classes :obj:`~genroc.class_Calibration.NDARC_Rotor_Calibration`
and :obj:`~genroc.class_Target.RotorTarget` define all functions
used to execute the calibration.
The calibration is started using 
:func: `~genroc.class_Calibration.NDARC_Rotor_Calibration.calibrate`.

.. automodule:: genroc.class_Calibration
   :members:
   :private-members:


genroc.class_Target
+++++++++++++++++++++++++
:obj:`~genroc.class_Target.RotorTarget` is a child class of the 
:obj:`~openmdao.api.ExplicitComponent` class of OpenMDAO. It handles the 
calculation of the error value that is to be minimized. The default method
for error calculation is the root mean square method. Other error metrics
can optionally be implemented.


.. automodule:: genroc.class_Target
   :members:
   :private-members:


genroc.utl.parse_ndarc_perf
+++++++++++++++++++++++++++++++++++

.. automodule:: genroc.tools.parse_ndarc_perf
   :members:
   :private-members:



.. only:: html

	Indices and tables
	==================

	* :ref:`genindex`
	* :ref:`modindex`
	* :ref:`search`
