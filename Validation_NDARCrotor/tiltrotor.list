 ! Tiltrotor
 ! March 2009
 &DEFN action='ident',created='March 2009',title='Tiltrotor',&END
 !##############################################################################
 ! default tiltrotor
 &DEFN action='configuration',&END
 &VALUE config='tiltrotor',&END
 !==============================================================================
 &DEFN quant='Cases',&END
 &VALUE
     title='Tiltrotor',
     FILE_design='tiltrotor.design',FILE_perf='tiltrotor.perf',
     FILE_geometry='tiltrotor.geom',FILE_sketch='tiltrotor.dxf',
     FILE_aircraft='tiltrotor.acd',FILE_solution='tiltrotor.soln',
     FILE_aero='tiltrotor.aero',FILE_engine='tiltrotor.eng',
     FILE_error='tiltrotor.err',
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Size',&END
 &VALUE
     title='Tiltrotor',
     SIZE_perf='none',SET_rotor='radius+Vtip+sigma','radius+Vtip+sigma',
     SET_wing='WL+width',
     FIX_DGW=1,FIX_WE=0,
     SET_tank='input',SET_SDGW='input',SET_WMTO='input',SET_limit_ds='input',
 &END
 &DEFN quant='OffDesign',&END
 &VALUE title='mission analysis',&END
 &DEFN quant='Performance',&END
 &VALUE title='performance analysis',&END
 !------------------------------------------------------------------------------
 &DEFN quant='Solution',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Cost',&END
 &VALUE
     FuelPrice=2.00,
     Npass=8,
 &END
 &DEFN quant='Emissions',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Aircraft',&END
 &VALUE
     title='Tiltrotor Aircraft',
     DGW=15000.,SDGW=15000.,WMTO=16000.,nz_ult=4.5,
     altitude=0.,SET_atmos='std',
     FIX_drag=0,FIX_DL=0,  ! FIX_drag=1 DoQ, 2 CD, 3 kDrag; FIX_DL=1 DoQV, 2 kDL
        DoQ=10.,CD=0.009,kDrag=1.5,
        DoQV=200.,kDL=.10,
     Vconv_hover=80.,Vconv_cruise=140.,
     INPUT_geom=2,KIND_scale=1,kScale=1,  ! scaled geometry (x/R,y/R,z/R)
        KIND_Ref=2,kRef=1,  ! wing location reference
     kx=0.9,ky=0.6,kz=1.0,
   ! H-tail
     nTail=3,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Systems',&END
 &VALUE
     title='Tiltrotor Systems',
   ! weight
     Wcrew=700.,Wtrap=80.,
     SET_Wvib=1,Wvib=0.,fWvib=0.,
     SET_Wcont=2,Wcont=0.,
   ! systems and equipment
     Wfc_cc=100.,Wfc_afcs=200.,
     WEQhyd=100.,
     Wauxpower=200.,Winstrument=200.,Wpneumatic=0.,Welectrical=400.,
     WMEQ=400.,
     Wfurnish=600.,Wenviron=100.,
   ! flight control
     MODEL_WRWfc=2,KIND_WRWfc=1,xRWfc_red=2.,fRWhyd=.4,
     fFWfc_nb=.4,fFWhyd=.6,
     fCVfc_mb=.01,fCVfc_nb=.4,fCVhyd=.6,
   ! anti-icing
     MODEL_DI=0,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Fuselage',&END
 &VALUE
     title='Tiltrotor Fuselage',
     SET_length=4,SET_nose=1,Length_nose=16.,SET_aft=2,fLength_aft=2.,
        fRef_fus=0.4,Width_fus=6.,
     SET_Swet=4,fSwet=0.7,fSproj=1.0,
        Height_fus=6.,
   ! aerodynamics
     DoQ_cont=0.,DoQV_cont=0.,
     AoA_zl=0.,AoA_max=20.,SS_max=35.,
     SET_lift=2,dLoQda=0.,dCLda=0.,
     SET_moment=2,CM0=-0.005,dCMda=0.025,
     SET_side=2,dCYdb=-0.12,
     SET_yaw=2,CN0=0.,dCNdb=-0.05,
     SET_drag=2,SET_Dfit=2,SET_Drb=2,SET_Vdrag=2,SET_Sdrag=2,
     CD=0.0050,CD_fit=0.0020,CD_rb=0.,0.,CDV=0.0070,CDS=0.,
     MODEL_drag=2,MODEL_trans=1,AoA_Dmin=-4.,AoA_tran=30.,
   ! weight
     MODEL_body=1,KIND_ramp=0,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='LandingGear',&END
 &VALUE
     title='Tiltrotor Landing Gear',
     speed=40.,
   ! drag
     DoQ=0.,
   ! weight
     MODEL_LG=2,
     nLG=3,fWLG_crash=0.,fWLG_ret=0.25,
 &END
 !==============================================================================
 &DEFN quant='Rotor 1',&END
 &VALUE
     title='Tiltrotor Right Rotor',
     diskload=14.,CWs=0.12,  ! design conditions
     Vtip_ref=750.,
        INPUT_Vtip=2,fRPM_cruise=0.8,fRPM_man=1.,fRPM_oei=1.,fRPM_xmsn=0.8,
     Plimit_rs=2000.,fPlimit_rs=2.,
     radius=12.,sigma=0.10,rotate=1,nblade=3,
        clearance_fus=1.0,
     SET_chord=2,taper=.8,SET_twist=2,twistL=-45.,nprop=7,
        rprop=0.,0.18,0.25,0.325,0.425,0.575,1.,
        twist=38.6,25.685,21.10,16.19,10.425,4.65,-6.4,
     KIND_hub=2,flapfreq=1.02,conefreq=1.35,gamma=4.,precone=1.5,delta3=-15.,  ! hingeless
     dclda=5.7,tiploss=.97,xroot=0.10,thick=.12,
     cant_hub=0.,dihedral_pivot=0.,pitch_pivot=0.,sweep_pivot=0.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! induced power
     Ki_hover=1.05,Ki_climb=1.05,Ki_prop=3.,Ki_edge=2.,
     Ki_min=1.,Ki_max=80.,
     CTs_Hind=.105,kh1=-.7,kh2=40.,Xh2=2.,
     CTs_Pind=-.001,kp1=0.,kp2=.06,Xp2=-1.5,kpa=-1000.,Xpa=2.,
     mu_prop=0.5,ka1=0.,ka2=0.,ka3=1.,Xa=1.4,
     mu_edge=0.3,ke1=0.,ke2=0.,ke3=1.,Xe=3.,
   ! profile power
     MODEL_basic=2,
     CTs_Dmin=.02,d0_hel=.0092,d1_hel=0.,d2_hel=.55,d0_prop=.0088,d1_prop=0.,d2_prop=0.,dprop=.7,Xprop=2.,
     CTs_sep=.07,dsep=5.,Xsep=3.,
     MODEL_stall=1,nstall=5,mu_stall=0.,.05,.10,.15,.20,CTs_stall=.13,.10,.09,.08,.08,
     fstall=1.,dstall1=2.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,dm1=.005,dm2=2.,Xm=3.,Mdd0=0.7,Mddcl=0.,
   ! drag
     SET_Spylon=3,kSwet_pylon=0.9,
     SET_Sspin=2,fSwet_spin=3.0,fRadius_spin=0.10,
     SET_Dhub=2,SET_Vhub=2,SET_Dpylon=2,SET_Dspin=2,SET_Vpylon=2,
     CD_pylon=0.007,CDV_pylon=0.3,MODEL_Dpylon=1,X_pylon=3.,
   ! interference
     MODEL_int=2,Vint_low=5.,Vint_high=10.,
     MODEL_boundary=3,Xboundary=.1,Kint_wing=2.,
     KIND_int_wing=1,Cint_wing=-.06,
   ! weight
     fWfold=0.,KIND_rotor=1,
     flapfreq_blade=1.35,rblade=0.45,
 &END
 !------------------------------------------------------------------------------
 ! left rotor: title,KIND_xmsn=0,INPUT_gear=2,gear=1.,rotate=-1
 &DEFN quant='Rotor 2',action='copy',source=1,&END
 &DEFN quant='Rotor 2',&END
 &VALUE
     title='Tiltrotor Left Rotor',
     KIND_xmsn=0,INPUT_gear=2,
     gear=1.,rotate=-1,
   ! configuration dependent
     otherRotor=1,
     T_coll(2,1)= 1.,T_lngcyc(4,1)=-1.,
 &END
 !==============================================================================
 &DEFN quant='Wing',&END
 &VALUE
     title='Tiltrotor Wing',
     wingload=80.,
     sweep_panel=-6.,dihedral_panel=2.,thick=.22,fWidth_box=.45,
     fchord_flap=.25,fspan_flap=.25,
     fchord_flaperon=.25,fspan_flaperon=.50,fAC_aileron=.65,
     incid=3.,
     nVflap=5,flap=60.,60.,40.,40.,0.,Vflap=0.,20.,21.,60.,61.,
     KIND_flaperon=1,nVflaperon=1,flaperon=.625,
   ! aerodynamics
     AoA_zl=-4.0,CLmax=2.00,SET_lift=3,dCLda=5.2,
     Tind=.1,Eind=0.95,
     eta0=1.2,eta1=0.6,
     CMac=0.,
     SET_drag=2,SET_Vdrag=3,SET_wb=2,
     CD=0.012,cd90=0.72,fd90=4.6,
     MODEL_drag=2,MODEL_sep=0,AoA_Dmin=-4.0,AoA_tran=30.,
     MODEL_sep=3,AoA_sep=10.,Ksep=200.,
   ! interference at tail
     Etail=.4,0.,0.,
   ! tiltrotor wing weight
     CTs_jump=.22,n_jump=2.0,Vtip_jump=750.,
     thickTR=.22,SET_Attach=1,fAttach=.80,fRG_pylon=.30,
     freq_beam=0.4,freq_chord=0.7,freq_tors=0.8,Vtip_freq=600.,
     MODEL_form=1,
     eff_spar=.8,eff_box=.8,C_t=.75,C_j=.5,C_m=1.5,
     E_spar=10.E6,E_box=10.E6,G_box=4.E6,
     StrainU_spar=.007,StrainU_box=.007,
     density_spar=.10,density_box=.10,
     Ufair=2.,Uflap=3.,fWfitTR=.0076,fWfoldTR=0.,
     UextTR=3.,Uext=3.,
   ! area wing weight model
   ! MODEL_weight=1,MODEL_wing=1,
     Uprim=5.50,fWfair=.11,fWfit=.13,fWflap=.10,
   ! parametric wing weight model
   ! MODEL_weight=1,MODEL_wing=2,
     fWfair=.11,fWfit=.13,fWflap=.10,
 &END
 !==============================================================================
 &DEFN quant='Tail 1',&END
 &VALUE
     title='Tiltrotor Horizontal Tail',
     SET_tail='vol+aspect',
     KIND_TailVol=1,TailVol=1.25,AspectRatio=3.5,
     thick=.15,
     fspan_cont=.85,fchord_cont=.30,
   ! aerodynamics
     AoA_zl=0.,CLmax=1.05,SET_lift=3,dCLda=4.,
     Tind=.1,Eind=0.9,
     eta0=1.15,eta1=0.,
     SET_drag=2,SET_Vdrag=2,
     CD=0.012,CDV=0.012,AoA_Dmin=0.,
   ! weight
     Vdive=400.,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Tail 2',&END
 &VALUE
     title='Tiltrotor Right Vertical Tail',
     SET_tail='vol+aspect',
     KIND_TailVol=1,TailVol=0.10,AspectRatio=2.5,
     sweep=30.,thick=.09,
     fspan_cont=.6,fchord_cont=.25,
   ! aerodynamics
     AoA_zl=0.,CLmax=1.05,SET_lift=3,dCLda=3.0,
     Tind=.25,Eind=.8,
     eta0=1.1,eta1=0.,
     SET_drag=2,SET_Vdrag=2,
     CD=0.0070,CDV=0.0070,AoA_Dmin=0.,
   ! weight
     Vdive=400.,
     place_AntiQ=1,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Tail 3',action='copy',source=2,&END
 &DEFN quant='Tail 3',&END
 &VALUE
     title='Tiltrotor Left Vertical Tail',
 &END
 !==============================================================================
 &DEFN quant='FuelTank',&END
 &VALUE
     title='Tiltrotor Fuel Tank',
     Wfuel_cap=1500.,
   ! aux tanks
     Waux_cap=5000.,DoQ_auxtank=0.,fWauxtank=0.11,
   ! weight
     ntank_int=4,
     KIND_crash=1,Ktoler=1.,
     nplumb=4,K0_plumb=40.,K1_plumb=1.,
 &END
 !==============================================================================
 &DEFN quant='Propulsion',&END
 &VALUE
     title='Tiltrotor Propulsion',
   ! losses
     fPloss_xmsn=.07,Pacc_0=20.,
   ! geometry
     SET_length=2,fLength_ds=1.000,
   ! torque limit
     Plimit_ds=3000.,fPlimit_ds=0.75,
   ! weight
     ngearbox=5,ndriveshaft=2,fShaft=.05,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='EngineGroup 1',&END
 &VALUE
     title='Tiltrotor Right Engine',
     nEngine=1,nEngine_main=1,
     IDENT_engine='GEN2000',
     Peng=1500.,rating_to='MRP',
   ! torque limit
     Plimit_es=1500.,fPlimit_es=0.75,
   ! installation
     eta_d=.98,
     Kffd=1.05,fPloss_inlet=0.,fPloss_ps=0.,fPloss_exh=0.,
     fMF_auxair=0.,eta_auxair=0.,
     fPloss_exh_IRon=0.,fMF_auxair_IRon=0.,eta_auxair_IRon=0.,
   ! geometry
     SET_Swet=3,kSwet=0.9,
   ! drag
     SET_drag=2,SET_Vdrag=2,
     CD=0.,CDV=0.,
   ! weight
     fWpylon=0.004,fWair=.19,
     Kwt0_exh=0,Kwt1_exh=.006,
     MODEL_lub=2,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='EngineGroup 2',action='copy',source=1,&END
 &DEFN quant='EngineGroup 2',&END
 &VALUE
     title='Tiltrotor Left Engine',RotorForEngine=2,
 &END
 !==============================================================================
 &DEFN quant='Geometry',&END
 &VALUE
   ! wing location reference
     loc_wing(1)%FIX_geom='xyz',
   ! fixed geometry (INPUT_geom=1); SL +aft, BL +right, WL +up
     loc_wing(1)%SL=0.,loc_wing(1)%BL=0.,loc_wing(1)%WL=0.,
   ! scaled geometry (INPUT_geom=2); x +aft, y +right, z +up
     loc_cg%XoL          =0.03,loc_cg%YoL          = 0.00,loc_cg%ZoL          =-0.10,
     loc_fuselage%XoL    =0.00,loc_fuselage%YoL    = 0.00,loc_fuselage%ZoL    =-0.24,
     loc_gear%XoL        =0.05,loc_gear%YoL        = 0.00,loc_gear%ZoL        =-0.50,
     loc_rotor(1)%XoL    =0.05,loc_rotor(1)%YoL    = 1.30,loc_rotor(1)%ZoL    = 0.40,
     loc_pylon(1)%XoL    =0.05,loc_pylon(1)%YoL    = 0.00,loc_pylon(1)%ZoL    = 0.04, ! SET_geom=tiltrotor: YoL relative hub
     loc_pivot(1)%XoL    =0.05,loc_pivot(1)%YoL    = 0.00,loc_pivot(1)%ZoL    = 0.04, ! SET_geom=tiltrotor: YoL relative hub
     loc_naccg(1)%XoL    =0.05,loc_naccg(1)%YoL    = 0.00,loc_naccg(1)%ZoL    = 0.15, ! SET_geom=tiltrotor: YoL relative hub
     loc_rotor(2)%XoL    =0.05,loc_rotor(2)%YoL    =-1.30,loc_rotor(2)%ZoL    = 0.40,
     loc_pylon(2)%XoL    =0.05,loc_pylon(2)%YoL    = 0.00,loc_pylon(2)%ZoL    = 0.04, ! SET_geom=tiltrotor: YoL relative hub
     loc_pivot(2)%XoL    =0.05,loc_pivot(2)%YoL    = 0.00,loc_pivot(2)%ZoL    = 0.04, ! SET_geom=tiltrotor: YoL relative hub
     loc_naccg(2)%XoL    =0.05,loc_naccg(2)%YoL    = 0.00,loc_naccg(2)%ZoL    = 0.15, ! SET_geom=tiltrotor: YoL relative hub
     loc_wing(1)%XoL     =0.00,loc_wing(1)%YoL     = 0.00,loc_wing(1)%ZoL     = 0.00,
     loc_tail(1)%XoL     =1.80,loc_tail(1)%YoL     = 0.00,loc_tail(1)%ZoL     = 0.05,
     loc_tail(2)%XoL     =1.90,loc_tail(2)%YoL     = 0.50,loc_tail(2)%ZoL     = 0.15,
     loc_tail(3)%XoL     =1.90,loc_tail(3)%YoL     =-0.50,loc_tail(3)%ZoL     = 0.15,
     loc_auxtank(1,1)%XoL=0.05,loc_auxtank(1,1)%YoL= 0.00,loc_auxtank(1,1)%ZoL= 0.00,
     loc_engine(1)%XoL   =0.05,loc_engine(1)%YoL   = 1.30,loc_engine(1)%ZoL   = 0.04,
     loc_engine(2)%XoL   =0.05,loc_engine(2)%YoL   =-1.30,loc_engine(2)%ZoL   = 0.04,
 &END
 !==============================================================================
 &DEFN quant='TechFactors',&END
 &VALUE
   ! cost
     TECH_cost_af=1.,TECH_cost_maint=1.,
   ! flight control
     TECH_RWfc_b=1.,TECH_RWfc_mb=1.,TECH_RWfc_nb=1., ! rotary wing
     TECH_FWfc_mb=1.,TECH_FWfc_nb=1.,                ! fixed wing
     TECH_CVfc_mb=1.,TECH_CVfc_nb=1.,                ! conversion
     TECH_RWhyd=1.,TECH_FWhyd=1.,TECH_CVhyd=1.,      ! hydraulics
   ! anti-icing
     TECH_DIelect=1.,TECH_DIsys=1.,
   ! fuselage
     TECH_body=1.,TECH_mar=1.,TECH_press=1.,TECH_crash=1.,TECH_ftfold=1.,TECH_fwfold=1.,
     TECH_LG=1.,TECH_LGret=1.,TECH_LGcrash=1.,
   ! rotor profile power
     TECH_drag=1.,1.,
   ! rotor
     TECH_blade=2*1.,TECH_hub=2*1.,TECH_spin=2*1.,TECH_rfold=1.,1.,TECH_tr=1.,1.,TECH_aux=1.,1.,
   ! wing
     TECH_prim=1.,TECH_ext=1.,TECH_fair=1.,TECH_fit=1.,TECH_flap=1.,TECH_wfold=1.,TECH_efold=1.,
   ! tail
     TECH_tail=1.,2*1.,
   ! fuel tank
     TECH_tank=1.,TECH_plumb=1.,
   ! drive system
     TECH_gb=1.,TECH_rs=1.,TECH_ds=1.,TECH_rb=0.,
   ! engine group
     TECH_eng=2*1.,TECH_cowl=2*1.,TECH_pylon=2*1.,TECH_supt=2*1.,TECH_air=2*1.,TECH_exh=2*1.,TECH_acc=2*1.,
 &END
 !##############################################################################
 &DEFN action='endoffile',&END
