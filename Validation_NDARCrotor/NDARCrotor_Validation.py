#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
import pandas as pd

from genroc.class_NDARCrotor import NDARCrotor
from genroc.utl.parse_ndarc_perf import parse_perf
from genroc.class_Calibration import NDARC_Rotor_Calibration


try:
    from mpi4py import MPI
    mpiloaded=True
except:
    print("mpi not loaded")
    mpiloaded=False

model='uh60a'
perffile = model+'.perf'
solnfile = model+'.acd'
twin=False
i_rot=2

# Use this script to verify that NDARCrotor calculates the same values for kappa, cdmean, CPs_ind and CPs_pro as NDARC
# Create a perf file (e.g. with the included job files in models_validation_NDARCrotor and check error values in df_comp


df_ref=pd.DataFrame()
n_rot=1
if twin: n_rot=2
if not twin: i_rot=1
n_cond=3
lst_cond=list(range(1,n_cond+1))
for i in range(1, n_rot+1):
    if n_rot==1: 
        str_r=''
    else:
        str_r='r'+str(i)+'_'
    
    df=copy.deepcopy(parse_perf(perffile, lst_vars=[], lst_cond=lst_cond, str_r=str_r, i_rot=i))
    df[str_r+'mu']=(df[str_r+'mux']**2+df[str_r+'muy']**2)**0.5
    df=df.drop(columns=[str_r+'mux', str_r+'muy'])
    
    df_ref[df.columns]=df
    
df_ref=df_ref[df_ref['not_conv']=='....']

rot=NDARCrotor(acd_sol_file=solnfile, i_rot=i_rot, twin=twin)

if rot.MODEL_twin=='coaxial' and rot.i_rot==1: rot.low_up='low'

cal=NDARC_Rotor_Calibration(df_ref,kicd_cp='cp', rot=rot)
df_comp=cal.calc_df_comp(col_targets=['kappa','CPs_ind','lideal','cdmean','CPs_pro'])
   
for col_err in [col for col in df_comp.columns if 'err_' in col]:
    print('Max', col_err, round(max(abs(df_comp[col_err])),4))
