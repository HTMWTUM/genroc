 &DEFN action='ident',created='October 2008',
      title = 'Turboshaft Engine Math Model Data (GEN2000)',
 &END
 &DEFN quant='EngineModel',&END
 &VALUE
 ! Turboshaft Engine Model
      title = 'Turboshaft Engine Math Model Data (GEN2000)',
      notes = 'Generic 2000 hp Engine',
      ident = 'GEN2000',
 ! Engine Ratings
      nrate = 4,
      rating = 'MCP  ', 'IRP  ', 'MRP  ', 'CRP  ',
 ! Weight
      Kwt0_eng =       0.000,
      Kwt1_eng =       0.180,
      Kwt2_eng =       0.000,
      Xwt_eng  =      0.0000,
 ! Reference
      P0_ref     =     2000.,     2400.,     2540.,     2660.,
      SP0_ref    =      134.,      145.,      164.,      171.,
      Pmech_ref  =     3000.,     3000.,     3000.,     3000.,
      sfc0C_ref  =     0.44,
      SF0C_ref   =      8.9,
      Nspec_ref  =   19100.,
      Nopt0C_ref =   19100.,
 ! Scaling, MF_ref=15.0
      MF_limit    =     30.,
      SP0C_limit  =    143.,
      sfc0C_limit =    0.40,
      KNspec      =  73790.,
 ! Optimum Power Turbine Speed (linear)
      MODEL_OptN = 1,
      KNoptA =     1.,
      KNoptB =     0.,
      XNeta  =     2.,
 ! Single Set of Input Parameters
      INPUT_param = 1,
 ! Power Available
      INPUT_lin = 1,
    ! specific power
      Nspa =  1,  1,  1,  1,
      Kspa0(1,1) =       3.80,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Kspa0(1,2) =       3.20,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Kspa0(1,3) =       3.00,   0.00,   0.00,   0.00,   0.00,  ! MRP  
      Kspa0(1,4) =       2.80,   0.00,   0.00,   0.00,   0.00,  ! CRP  
      Kspa1(1,1) =      -2.80,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Kspa1(1,2) =      -2.20,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Kspa1(1,3) =      -2.00,   0.00,   0.00,   0.00,   0.00,  ! MRP  
      Kspa1(1,4) =      -1.80,   0.00,   0.00,   0.00,   0.00,  ! CRP  
      Xspa0(1,1) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Xspa0(1,2) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Xspa0(1,3) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! MRP  
      Xspa0(1,4) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! CRP  
      Xspa1(1,1) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Xspa1(1,2) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Xspa1(1,3) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! MRP  
      Xspa1(1,4) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! CRP  
    ! mass flow
      Nmfa =  2,  2,  2,  2,
      Kmfa0(1,1) =       0.30,   0.70,   0.00,   0.00,   0.00,  ! MCP  
      Kmfa0(1,2) =       0.29,   0.45,   0.00,   0.00,   0.00,  ! IRP  
      Kmfa0(1,3) =       0.28,   0.35,   0.00,   0.00,   0.00,  ! MRP  
      Kmfa0(1,4) =       0.27,   0.30,   0.00,   0.00,   0.00,  ! CRP  
      Kmfa1(1,1) =      -0.30,  -0.70,   0.00,   0.00,   0.00,  ! MCP  
      Kmfa1(1,2) =      -0.29,  -0.45,   0.00,   0.00,   0.00,  ! IRP  
      Kmfa1(1,3) =      -0.28,  -0.35,   0.00,   0.00,   0.00,  ! MRP  
      Kmfa1(1,4) =      -0.27,  -0.30,   0.00,   0.00,   0.00,  ! CRP  
      Xmfa0(1,1) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! MCP  
      Xmfa0(1,2) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! IRP  
      Xmfa0(1,3) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! MRP  
      Xmfa0(1,4) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! CRP  
      Xmfa1(1,1) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! MCP  
      Xmfa1(1,2) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! IRP  
      Xmfa1(1,3) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! MRP  
      Xmfa1(1,4) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! CRP  
 ! Performance at Power Required
    ! fuel flow         mass flow         gross jet thrust  net jet thrust
      Kffq0 =   0.20,   Kmfq0 =   0.60,   Kfgq0 =   0.20,   Kfgr0 =   0.80,
      Kffq1 =   0.80,   Kmfq1 =   0.78,   Kfgq1 =   0.80,   Kfgr1 =   0.60,
      Kffq2 =   0.00,   Kmfq2 =  -0.48,   Kfgq2 =   0.00,   Kfgr2 =   0.00,
      Kffq3 =   0.00,   Kmfq3 =   0.10,   Kfgq3 =   0.00,   Kfgr3 =   0.00,
      Xffq  =   1.30,   Xmfq  =   3.50,   Xfgq  =   2.00,
 &END
