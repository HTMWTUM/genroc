 ! Tandem Helicopter
 ! March 2009
 &DEFN action='ident',created='March 2009',title='Tandem Helicopter',&END
 !##############################################################################
 ! default tandem helicopter
 &DEFN action='configuration',&END
 &VALUE config='tandem',rotate=1,-1,overlap_tandem=2*0.35,&END
 !==============================================================================
 &DEFN quant='Cases',&END
 &VALUE
     title='Tandem Helicopter',
     FILE_design='tandem.design',FILE_perf='tandem.perf',
     FILE_geometry='tandem.geom',FILE_sketch='tandem.dxf',
     FILE_aircraft='tandem.acd',FILE_solution='tandem.soln',
     FILE_aero='tandem.aero',FILE_engine='tandem.eng',
     FILE_error='tandem.err',
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Size',&END
 &VALUE
     title='Tandem Helicopter',
     SIZE_perf='none',SET_rotor='radius+Vtip+sigma','radius+Vtip+sigma',
     FIX_DGW=1,FIX_WE=0,
     SET_tank='input',SET_SDGW='input',SET_WMTO='input',SET_limit_ds='input',
 &END
 &DEFN quant='OffDesign',&END
 &VALUE title='mission analysis',&END
 &DEFN quant='Performance',&END
 &VALUE title='performance analysis',&END
 !------------------------------------------------------------------------------
 &DEFN quant='Solution',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Cost',&END
 &VALUE
     FuelPrice=2.00,
     Npass=30,
 &END
 &DEFN quant='Emissions',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Aircraft',&END
 &VALUE
     title='Tandem Helicopter Aircraft',
     DGW=30000.,SDGW=30000.,WMTO=45000.,nz_ult=4.5,
     altitude=4000.,SET_atmos='temp',temp=95.,
     FIX_drag=0,FIX_DL=0,  ! FIX_drag=1 DoQ, 2 CD, 3 kDrag; FIX_DL=1 DoQV, 2 kDL
        DoQ=50.,CD=0.012,kDrag=4.0,
        DoQV=275.,kDL=.055,
     INPUT_geom=2,KIND_scale=1,kScale=1,  ! scaled geometry (x/R,y/R,z/R)
        KIND_Ref=3,  ! fuselage reference
     kx=0.2,ky=0.5,kz=0.5,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Systems',&END
 &VALUE
     title='Tandem Helicopter Systems',
   ! weight
     Wcrew=600.,Wtrap=100.,
     SET_Wvib=1,Wvib=0.,fWvib=0.025,
     SET_Wcont=2,Wcont=0.,
   ! systems and equipment
     Wfc_cc=125.,Wfc_afcs=100.,
     MODEL_RWfc=1, ! 1 global, 2 for each rotor
     MODEL_WRWfc=2,xRWfc_red=3.,fRWfc_nb=0.75,KIND_WRWfc=1,fRWhyd=0.3,
     Wauxpower=150.,Winstrument=250.,Wpneumatic=0.,Welectrical=650.,
     WMEQ=500.,
     Wfurnish=800.,Wenviron=150.,Wload=400.,
     MODEL_DI=1,kDeIce_elec=0.025,0.025,kDeIce_rotor=0.02,0.02,kDeIce_air=0.020,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Fuselage',&END
 &VALUE
     title='Tandem Helicopter Fuselage',
     SET_length=4,SET_nose=1,SET_aft=1,Length_nose=6.,Length_aft=6.,
        fRef_fus=0.85,Width_fus=9.,
     SET_Swet=4,fSwet=0.8,fSproj=1.0,
        Height_fus=8.,
   ! aerodynamics
     DoQ_cont=0.,
     AoA_zl=0.,AoA_max=35.,SS_max=35.,
     SET_lift=2,dCLda=0.1,
     SET_moment=2,CM0=0.,dCMda=0.05,
     SET_side=2,dCYdb=-0.4,
     SET_yaw=2,CN0=0.,dCNdb=-0.03,
     SET_drag=2,SET_Dfit=2,SET_Drb=2,SET_Vdrag=2,SET_Sdrag=2,
     CD=0.0070,CD_fit=0.0020,CDV=0.60,CD_rb=0.,0.,CDS=0.,
     MODEL_drag=2,MODEL_trans=2,AoA_Dmin=0.,Kdrag=8.,Xdrag=1.25,
   ! weight
     MODEL_body=1,KIND_ramp=1,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='LandingGear',&END
 &VALUE
     title='Tandem Helicopter Landing Gear',
   ! drag
     DoQ=5.,
   ! weight
     MODEL_LG=2,
     nLG=4,fWLG_crash=0.0,fWLG_ret=0.,
 &END
 !==============================================================================
 &DEFN quant='Rotor 1',&END
 &VALUE
!~ 	 KIND_control=3,
     title='Tandem Helicopter Front Rotor',
     diskload=6.,CWs=0.075, ! design conditions
     Vtip_ref=700.,
        INPUT_Vtip=2,fRPM_cruise=1.,fRPM_man=1.,fRPM_oei=1.,fRPM_xmsn=1.,
     Plimit_rs=4000.,fPlimit_rs=2.,
     radius=30.,sigma=.085,rotate=1,nblade=3,
        overlap_tandem=0.35,
     SET_chord=2,taper=1.,SET_twist=1,twistL=-10.,
     KIND_hub=1,flapfreq=1.02,gamma=8.,precone=0.,
     dclda=5.7,tiploss=.97,xroot=.15,thick=.10,
     incid_hub=-8.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! profile power
     MODEL_basic=2,
     CTs_Dmin=.07,d0_hel=.0080,d0_prop=.0080,d1_hel=0.,d1_prop=0.,d2_hel=0.,d2_prop=0.,
     CTs_sep=.07,dsep=7.,Xsep=3.,
     MODEL_stall=1,CTs_stall(1)=0., ! default
     fstall=1.,dstall1=2.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,Mdd0=.73,Mddcl=0.,dm1=.005,dm2=1.,Xm=3.,
   ! induced power
     Ki_hover=1.15,Ki_climb=1.12,Ki_prop=1.12,Ki_edge=2.0,Ki_min=1.1,
        CTs_Hind=0.06,kh1=1.5,kh2=0.,CTs_Pind=0.06,kp1=1.5,kp2=0.,
        mu_edge=0.35,ke1=0.,ke2=0.,ke3=1.,Xe=4.,
   ! twin rotors
     Kh_twin=1.,Kf_twin=0.85,Cind_twin=1.,
   ! interference
     MODEL_int=2,Vint_low=5.,Vint_high=20.,
   ! drag
     SET_Spylon=2,kSwet_pylon=0.3,
     SET_Dhub=2,SET_Dpylon=2,SET_Vhub=2,SET_Vpylon=2,
     CD_hub=0.0025,CDV_hub=0.0025,CD_pylon=0.05,CDV_pylon=0.05,
   ! interference
     Kint_fus=.75,
 &END
 !------------------------------------------------------------------------------
 ! rear rotor: title,KIND_xmsn=0,INPUT_gear=1,gear=1.,incid_hub=-4,rotate=-1
 &DEFN quant='Rotor 2',action='copy',source=1,&END
 &DEFN quant='Rotor 2',&END
 &VALUE
     title='Tandem Helicopter Rear Rotor',
     KIND_xmsn=0,INPUT_gear=1,
     gear=1.,rotate=-1,incid_hub=-4.,
   ! configuration dependent
     otherRotor=1,T_coll(3,1)=1.,T_latcyc(2,1)=1.,
   ! drag
     SET_Spylon=2,kSwet_pylon=1.2,
 &END
 !==============================================================================
 &DEFN quant='FuelTank',&END
 &VALUE
     title='Tandem Helicopter Fuel Tank',
     Wfuel_cap=6000.,
   ! aux tanks
     Waux_cap=1000.,DoQ_auxtank=0.,fWauxtank=0.11,
   ! weight
     ntank_int=6,nplumb=6,K0_plumb=120.,K1_plumb=3.,
     KIND_crash=2,Ktoler=2.,
 &END
 !==============================================================================
 &DEFN quant='Propulsion',&END
 &VALUE
     title='Tandem Helicopter Propulsion',
   ! losses
     fPloss_xmsn=.02,Pacc_0=200.,
   ! geometry
     SET_length=2,fLength_ds=0.9,
   ! torque limit
     Plimit_ds=7500.,fPlimit_ds=0.9,
   ! weight
     ngearbox=5,ndriveshaft=6,fShaft=.10,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='EngineGroup',&END
 &VALUE
     title='Tandem Helicopter Engine Group',
     nEngine=2,nEngine_main=2,
     IDENT_engine='GEN4000',
     Peng=4500.,rating_to='MRP',
   ! torque limit
     Plimit_es=7500.,fPlimit_es=0.9,
   ! installation
     eta_d=0.95,
     Kffd=1.,fPloss_inlet=0.,fPloss_ps=0.,fPloss_exh=0.,
     fMF_auxair=0.,eta_auxair=0.,
     fPloss_exh_IRon=0.,fMF_auxair_IRon=0.,eta_auxair_IRon=0.,
   ! geometry
     SET_Swet=2,kSwet=0.6,
   ! drag
     SET_drag=2,SET_Vdrag=2,
     CD=0.025,CDV=0.025,
   ! weight
     fWpylon=0.,fWair=.55,
     Kwt0_exh=0.,Kwt1_exh=.005,
     MODEL_lub=2,
 &END
 !==============================================================================
 &DEFN quant='Geometry',&END
 &VALUE
   ! fuselage reference
     loc_fuselage%FIX_geom='xyz',
   ! scaled geometry (INPUT_geom=2); x +aft, y +right, z +up
     loc_cg%XoL          =0.01,loc_cg%YoL          =0.00,loc_cg%ZoL          = 0.00,  ! .0583R fwd to .0333R aft
     loc_fuselage%XoL    =0.00,loc_fuselage%YoL    =0.00,loc_fuselage%ZoL    = 0.00,
     loc_gear%XoL        =0.00,loc_gear%YoL        =0.00,loc_gear%ZoL        =-0.30,
     loc_rotor(1)%XoL    =0.00,loc_rotor(1)%YoL    =0.00,loc_rotor(1)%ZoL    = 0.20,  ! rotor hubs at FS=+/-x
     loc_pylon(1)%XoL    =0.00,loc_pylon(1)%YoL    =0.00,loc_pylon(1)%ZoL    = 0.20,  ! SET_geom=tandem: SL relative hub
     loc_rotor(2)%XoL    =0.00,loc_rotor(2)%YoL    =0.00,loc_rotor(2)%ZoL    = 0.30,
     loc_pylon(2)%XoL    =0.00,loc_pylon(2)%YoL    =0.00,loc_pylon(2)%ZoL    = 0.30,  ! SET_geom=tandem: SL relative hub
     loc_auxtank(1,1)%XoL=0.00,loc_auxtank(1,1)%YoL=0.00,loc_auxtank(1,1)%ZoL= 0.00,
     loc_engine(1)%XoL   =0.65,loc_engine(1)%YoL   =0.00,loc_engine(1)%ZoL   = 0.10,
 &END
 !==============================================================================
 &DEFN quant='TechFactors',&END
 &VALUE
   ! cost
     TECH_cost_af=1.,TECH_cost_maint=1.,
   ! flight control
     TECH_RWfc_b=1.,TECH_RWfc_mb=1.,TECH_RWfc_nb=1.,TECH_RWhyd=1.,
   ! anti-icing
     TECH_DIelect=1.,TECH_DIsys=1.,
   ! fuselage
     TECH_body=1.,TECH_mar=1.,TECH_press=1.,TECH_crash=1.,TECH_ftfold=1.,TECH_fwfold=1.,
     TECH_LG=1.,TECH_LGret=1.,TECH_LGcrash=1.,
   ! rotor profile power
     TECH_drag=1.,1.,
   ! rotor
     TECH_blade=1.,1.,TECH_hub=1.,1.,TECH_spin=1.,1.,TECH_rfold=1.,1.,TECH_tr=1.,1.,TECH_aux=1.,1.,
   ! fuel tank
     TECH_tank=1.,TECH_plumb=1.,
   ! drive system
     TECH_gb=1.,TECH_rs=1.,TECH_ds=1.,TECH_rb=1.,
   ! engine group
     TECH_eng=1.,TECH_cowl=1.,TECH_pylon=1.,TECH_supt=1.,TECH_air=1.,TECH_exh=1.,TECH_acc=1.,
 &END
 !##############################################################################
 &DEFN action='endoffile',&END
