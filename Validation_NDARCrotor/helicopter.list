 ! Single Main Rotor and Tail Rotor Helicopter
 ! March 2009
 &DEFN action='ident',created='March 2009',title='Helicopter',&END
 !##############################################################################
 ! default helicopter
 &DEFN action='configuration',&END
 &VALUE config='helicopter',rotate=1,&END
 !==============================================================================
 &DEFN quant='Cases',&END
 &VALUE
     title='Helicopter',
     FILE_design='helicopter.design',FILE_perf='helicopter.perf',
     FILE_geometry='helicopter.geom',FILE_sketch='helicopter.dxf',
     FILE_aircraft='helicopter.acd',FILE_solution='helicopter.soln',
     FILE_aero='helicopter.aero',FILE_engine='helicopter.eng',
     FILE_error='helicopter.err',
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Size',&END
 &VALUE
     title='Helicopter',
     SIZE_perf='none',SET_rotor='radius+Vtip+sigma','radius+Vtip+sigma',
     FIX_DGW=1,FIX_WE=0,
     SET_tank='input',SET_SDGW='input',SET_WMTO='input',SET_limit_ds='input',
 &END
 &DEFN quant='OffDesign',&END
 &VALUE title='mission analysis',&END
 &DEFN quant='Performance',&END
 &VALUE title='performance analysis',&END
 !------------------------------------------------------------------------------
 &DEFN quant='Solution',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Cost',&END
 &VALUE
     year_inf=2010,
     FuelPrice=5.00,
     Npass=10,
 &END
 &DEFN quant='Emissions',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Aircraft',&END
 &VALUE
     title='Helicopter Aircraft',
     DGW=16000.,SDGW=17000.,WMTO=21000.,nz_ult=4.0,
     altitude=4000.,SET_atmos='temp',temp=95.,
     FIX_drag=0,FIX_DL=0,  ! FIX_drag=1 DoQ, 2 CD, 3 kDrag; FIX_DL=1 DoQV, 2 kDL
        DoQ=25.,CD=0.012,kDrag=4.0,
        DoQV=70.,kDL=0.035,
     INPUT_geom=2,KIND_scale=1,kScale=1,  ! scaled geometry (x/R,y/R,z/R)
        KIND_Ref=1,kRef=1,  ! mr hub reference
      kx=0.1,ky=0.3,kz=0.3,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Systems',&END
 &VALUE
     title='Helicopter Systems',
     SET_Wpayload=2,Upass=200.,
     SET_Wcrew=2,Ncrew=2,Ucrew=220.,Wcrew=160.,
     nWoful=2,Woful_name='baggage','survival kit',Woful=60.,40.,
     Wtrap=75.,
     SET_Wvib=1,Wvib=0.,fWvib=0.025,
     SET_Wcont=2,Wcont=0.,
     Wfc_cc=100.,Wfc_afcs=100.,
     fRWfc_nb=1.25,fRWhyd=0.4,
     MODEL_FWfc=1,MODEL_WFWfc=2,fFWfc_nb=0.15,
     Wauxpower=200.,Winstrument=200.,Wpneumatic=0.,Welectrical=400.0,
     WMEQ=400.,
     Wfurnish=600.,Wenviron=100.,
     Ncrew_seat=2,Npass_seat=10,Ucrew_seat_inc=50.,Upass_seat_inc=40.,
     MODEL_DI=1,kDeIce_elec=0.25,0.25,kDeIce_rotor=0.25,0.08,kDeIce_air=0.006,
     SET_fold=0,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Fuselage',&END
 &VALUE
     title='Helicopter Fuselage',
     SET_length=4,SET_nose=1,SET_aft=2,Length_nose=12.,fLength_aft=-0.2,
        fRef_fus=0.4,Width_fus=8.,
     SET_Swet=3,fSwet=0.7,fSproj=1.0,
        Height_fus=6.,
        Circum_boom=18.,Width_boom=2.7,
   ! aerodynamics
     DoQ_cont=0.,
     AoA_zl=0.,AoA_max=35.,SS_max=35.,
     SET_lift=2,dCLda=0.1,
     SET_moment=2,CM0=0.,dCMda=0.05,
     SET_side=2,dCYdb=-0.15,
     SET_yaw=2,CN0=0.,dCNdb=-0.03,
     SET_drag=2,SET_Dfit=2,SET_Drb=2,SET_Vdrag=2,SET_Sdrag=2,
     CD=0.0065,CD_fit=0.0065,CD_rb=0.,0.,CDV=0.40,CDS=0.04,
     MODEL_drag=2,AoA_Dmin=0.,Kdrag=15.,AoA_tran=25.,
   ! weight
     fWbody_crash=0.06,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='LandingGear',&END
 &VALUE
     title='Helicopter Landing Gear',
   ! drag
     DoQ=0.,
   ! weight
     nLG=3,fWLG_crash=0.15,fWLG_ret=0.,
 &END
 !==============================================================================
 &DEFN quant='Geometry',&END
 &VALUE
   ! mr hub reference
     loc_rotor(1)%FIX_geom='xyz',
   ! fixed geometry (INPUT_geom=1); SL +aft, BL +right, WL +up
     loc_rotor(1)%SL=0.,loc_rotor(1)%BL=0.,loc_rotor(1)%WL=0.,
   ! scaled geometry (INPUT_geom=2); x +aft, y +right, z +up
     loc_cg%XoL          =0.05,loc_cg%YoL          =0.00,loc_cg%ZoL          =-0.20,
     loc_fuselage%XoL    =0.00,loc_fuselage%YoL    =0.00,loc_fuselage%ZoL    =-0.25,
     loc_gear%XoL        =0.00,loc_gear%YoL        =0.00,loc_gear%ZoL        =-0.45,
     loc_rotor(1)%XoL    =0.00,loc_rotor(1)%YoL    =0.00,loc_rotor(1)%ZoL    = 0.00,
     loc_pylon(1)%XoL    =0.00,loc_pylon(1)%YoL    =0.00,loc_pylon(1)%ZoL    =-0.11,
     loc_rotor(2)%XoL    =1.20,loc_rotor(2)%YoL    =0.04,loc_rotor(2)%ZoL    = 0.04,
     loc_pylon(2)%XoL    =0.00,loc_pylon(2)%YoL    =0.04,loc_pylon(2)%ZoL    = 0.04, ! SET_geom=tailrotor: pylon XoL relative hub
     loc_tail(1)%XoL     =1.10,loc_tail(1)%YoL     =0.00,loc_tail(1)%ZoL     =-0.25,
     loc_tail(2)%XoL     =1.10,loc_tail(2)%YoL     =0.00,loc_tail(2)%ZoL     =-0.15,
     loc_auxtank(1,1)%XoL=0.00,loc_auxtank(1,1)%YoL=0.00,loc_auxtank(1,1)%ZoL=-0.25,
     loc_engine(1)%XoL   =0.00,loc_engine(1)%YoL   =0.00,loc_engine(1)%ZoL   =-0.10,
 &END
 !==============================================================================
 &DEFN quant='Rotor 1',&END
 &VALUE
     title='Helicopter Main Rotor',
     diskload=8.,CWs=.09, ! design conditions
     Vtip_ref=700.,
        INPUT_Vtip=2,fRPM_cruise=1.,fRPM_man=1.,fRPM_oei=1.,fRPM_xmsn=1.,
     Plimit_rs=2880.,fPlimit_rs=2.,
     radius=25.,sigma=0.0962,rotate=1,nblade=4,
     SET_chord=3,SET_twist=1,twistL=-10.,
        nprop=3,rprop=0.,.9,1.,fchord=1.,1.,.6,
     KIND_hub=1,flapfreq=1.035,gamma=8.,precone=0.,
     dclda=5.7,tiploss=.97,xroot=0.15,thick=0.09,
     incid_hub=-3.,cant_hub=0.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! induced power
     Ki_hover=1.125,Ki_climb=1.125,Ki_prop=1.125,Ki_edge=2.0,Ki_min=1.1,
     CTs_Hind=.10,kh1=1.25,kh2=0.,
     mu_edge=0.35,ke1=.8,ke2=0.,ke3=1.,Xe=4.5,
   ! profile power
     MODEL_basic=2,
     CTs_Dmin=.05,d0_hel=.0080,d0_prop=.0080,d1_hel=0.,d1_prop=0.,d2_hel=.5,d2_prop=.5,
     CTs_sep=.07,dsep=4.,Xsep=3.,
     MODEL_stall=1,CTs_stall(1)=0., ! default
     fstall=1.,dstall1=2.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,Mdd0=.73,Mddcl=0.,dm1=.005,dm2=1.,Xm=3.,
   ! drag
     SET_Spylon=2,kSwet_pylon=0.9,
     SET_Dhub=2,SET_Vhub=2,SET_Dpylon=2,SET_Vpylon=2,
     CD_hub=0.0025,CDV_hub=0.0025,CD_pylon=0.04,CDV_pylon=0.04,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Rotor 2',&END
 &VALUE
     title='Helicopter Tail Rotor',
     diskload=15.,CWs=.10, ! design conditions
     INPUT_gear=1,Vtip_ref=700.,
     Plimit_rs=640.,fPlimit_rs=2.,
     radius=6.,sigma=0.16,rotate=1,nblade=4,clearance_tr=0.25,
     SET_chord=2,taper=1.,SET_twist=1,twistL=0.,
     KIND_hub=1,flapfreq=1.2,gamma=4.,precone=0.,delta3=45.,
     dclda=5.7,tiploss=.97,xroot=0.4,thick=.09,
     incid_hub=0.,cant_hub=20.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! induced power
     Ki_hover=1.65,Ki_climb=1.65,Ki_prop=1.65,Ki_edge=2.0,Ki_min=1.65,
     CTs_Hind=.05,kh1=0.,kh2=80.,
     mu_edge=0.35,ke1=.8,ke2=0.,ke3=1.,Xe=4.5,
   ! profile power
     MODEL_basic=2,
     CTs_Dmin=.04,d0_hel=.0090,d0_prop=.0090,d1_hel=0.,d1_prop=0.,d2_hel=.9,d2_prop=.9,
     CTs_sep=.06,dsep=20.,Xsep=3.,
     MODEL_stall=1,CTs_stall(1)=0., ! default
     fstall=1.,dstall1=5.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,Mdd0=.68,Mddcl=0.,dm1=.005,dm2=1.,Xm=3.,
   ! no interference (vertical tail at low speed)
     MODEL_int=0,
   ! drag
     SET_Spylon=1,Swet_pylon=0.,
     SET_Dhub=2,SET_Vhub=2,SET_Dpylon=2,SET_Vpylon=2,
     CD_hub=0.030,CDV_hub=0.030,CD_pylon=0.,CDV_pylon=0.,
 &END
 !==============================================================================
 &DEFN quant='Tail 1',&END
 &VALUE
     title='Helicopter Horizontal Tail',
     SET_tail='vol+aspect',
     TailVol=0.020,AspectRatio=5.,
     taper=1.,sweep=0.,dihedral=0.,thick=0.12,fchord_cont=0.,
     cant=0.,incid=0.,
   ! aerodynamics
     AoA_zl=1.,CLmax=0.8,SET_lift=3,dCLda=2.3,Tind=1.,Eind=.9,
     SET_drag=2,SET_Vdrag=2,
     CD=0.015,CDV=0.015,
     AoA_Dmin=0.,MODEL_drag=0,
   ! weight
     Vdive=250.,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Tail 2',&END
 &VALUE
     title='Helicopter Vertical Tail',
     SET_tail='vol+aspect',
     TailVol=0.015,AspectRatio=2.,
     taper=1.,sweep=45.,dihedral=0.,thick=0.20,fchord_cont=0.,
     cant=0.,incid=0.,
   ! aerodynamics
     AoA_zl=0.,CLmax=1.,SET_lift=3,dCLda=1.5,Tind=1.,Eind=.9,
     SET_drag=2,SET_Vdrag=2,
     CD=0.020,CDV=0.020,
     AoA_Dmin=0.,MODEL_drag=0,
   ! weight
     Vdive=210.,
     place_AntiQ=2, ! place=tail
 &END
 !==============================================================================
 &DEFN quant='FuelTank',&END
 &VALUE
     title='Helicopter Fuel Tank',
     Wfuel_cap=2500.,
   ! aux tanks
     Waux_cap=5000.,DoQ_auxtank=0.,fWauxtank=0.11,
   ! weight
     ntank_int=2,nplumb=2,Ktoler=1.8,
     K0_plumb=120.,K1_plumb=3.,
 &END
 !==============================================================================
 &DEFN quant='Propulsion',&END
 &VALUE
     title='Helicopter Propulsion',
   ! losses
     MODEL_Xloss=1,
     fPloss_xmsn=0.02,Ploss_windage=0.0,Pacc_0=60.,Pacc_d=0.,Pacc_n=0.,fPacc_ECU=0.,fPacc_IRfan=0.,
   ! geometry
     SET_length=2,fLength_ds=0.9,
   ! torque limit
     Plimit_ds=2880.,fPlimit_ds=0.9,
   ! weight
     ngearbox=5,ndriveshaft=4,fShaft=0.10,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='EngineGroup',&END
 &VALUE
     title='Helicopter Engine Group',
     nEngine=2,nEngine_main=2,
     IDENT_engine='GEN2000',
     Peng=1600.,rating_to='MRP',
     SET_Swet=2,kSwet=0.8,
   ! torque limit
     Plimit_es=2880.,fPlimit_es=0.9,
   ! installation
     eta_d=0.99,
     Kffd=1.05,fPloss_inlet=.007,fPloss_ps=0.,
     fPloss_exh=.02,fMF_auxair=.01,eta_auxair=.75,
     fPloss_exh_IRon=.02,fMF_auxair_IRon=.01,eta_auxair_IRon=.75,
   ! drag
     SET_drag=2,SET_Vdrag=2,
     CD=0.0100,CDV=0.0100,
   ! weight
     fWpylon=0.,fWair=0.45,
     Kwt0_exh=0.,Kwt1_exh=0.06,
     MODEL_lub=1,
 &END
 !==============================================================================
 &DEFN quant='TechFactors',&END
 &VALUE
   ! cost
     TECH_cost_af=1.,TECH_cost_maint=1.,
   ! flight control
     TECH_RWfc_b=1.,TECH_RWfc_mb=1.,TECH_RWfc_nb=1.,TECH_RWhyd=1.,
     TECH_FWfc_nb=1.,TECH_FWfc_mb=1.,TECH_FWhyd=1.,
   ! anti-icing
     TECH_DIelect=1.,TECH_DIsys=1.,
   ! fuselage
     TECH_body=1.,TECH_mar=1.,TECH_press=1.,TECH_crash=1.,TECH_ftfold=1.,TECH_fwfold=1.,
     TECH_LG=1.,TECH_LGret=1.,TECH_LGcrash=1.,
   ! rotor profile power
     TECH_drag=1.,1.,
   ! rotor
     TECH_blade=1.,1.,TECH_hub=1.,1.,TECH_spin=1.,1.,TECH_rfold=1.,1.,TECH_tr=1.,1.,TECH_aux=1.,1.,
   ! tail
     TECH_tail=1.,1.,
   ! fuel tank
     TECH_tank=1.,TECH_plumb=1.,
   ! drive system
     TECH_gb=1.,TECH_rs=1.,TECH_ds=1.,TECH_rb=1.,
   ! engine group
     TECH_eng=1.,TECH_cowl=1.,TECH_pylon=1.,TECH_supt=1.,TECH_air=1.,TECH_exh=1.,TECH_acc=1.,
 &END
 !##############################################################################
 &DEFN action='endoffile',&END
