.. _Installation:


Installation
++++++++++++


Installation
To install GeNRoC follow these steps::

	git clone https://gitlab.lrz.de/HTMWTUM/genroc.git
	cd genroc
	python setup.py install	

The following sections are taken from the RCOtools documentation:

.. _OpenMDAO_Installation:


OpenMDAO Installation
+++++++++++++++++++++

OpenMDAO can be installed using the following command::

	pip install openmdao

or through `Anaconda Python <https://www.anaconda.com>`_ with::
	
	conda install openmdao


.. _OpenMDAO_Parallel:


OpenMDAO Parallel Processing Support
++++++++++++++++++++++++++++++++++++

OpenMDAO uses MPI and PETSc for parallel processing. 
Installing an OpenMDAO compatible MPI and PETSc can be quite difficult, however 
`Anaconda Python <https://www.anaconda.com>`_ now allows installation of these utilities 
using their ``conda`` command. Before installation OpenMDAO should be installed and
"conda-forge" should be added to the conda "channels" list. To add "conda-forge",
enter the following commands in a terminal window::

	conda config --add channels conda-forge
	conda config --set channel_priority strict

Once access to "conda-forge" is setup, MPI and PETSc can be installed using the 
following commands::

	conda install openmpi mpi4py
	conda install petsc petsc4p


**Note: OpenMPI Not Available for Windows**  
OpenMPI does not currently seem to be available for Windows systems.
Windows users might consider using the Ubuntu subsystem.

.. _Generate_Documentation:


Generate Documentation
++++++++++++++++++++++

GeNRoC documentation can be generated using the python package 
`Sphinx <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_
and the files in the ``docs``
directory of the rcotools distribution by using the following commands::

	cd path_to_docs
	make html BUILDDIR=directory_path

if ``BUILDDIR`` argument is not provided, the HTML documents will be generated in
the ``docs/build/html`` directory.  Different document formats can be
built, just execute ``make`` without an argument to see which format targets are
available.


**LaTex PDF generation:**
If you have TeX and LaTeX available, run "``make latexpdf``," which runs
the LaTeX builder and invokes the pdfTeX toolchain for you. The PDF 
document will be generated in the ``docs/build/latex`` directory.
