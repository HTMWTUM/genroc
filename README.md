# GeNRoC

GeNRoC is a calibration tool for the induced and profile power model of
NDARC. It employs the genetic algorithm SimpleGADriver from
the open-source optimization framework OpenMDAO. All parameters of the
induced and profile submodels, including those for twin rotors, can be calibrated
using reference data, e.g. from CAMRAD. The methodology is derived from the 
NDARC rotor calibration manual. The necessary equations from the NDARC source
code are present in the python package so that NDARC is not required for execution.
For fast execution the calibration can be run in parallel on multiple CPUs.

**Currently supported NDARC version: v1.16**

## Installation
To install GeNRoC follow these steps:

```
git clone https://gitlab.lrz.de/HTMWTUM/genroc.git
cd genroc
python setup.py install	
```

The following sections are taken from the RCOtools documentation:

## OpenMDAO Installation

OpenMDAO can be install using the following command:
```
pip install openmdao
```
or through [Anaconda Python](https://www.anaconda.com) with:
```
conda install openmdao
```

## OpenMDAO Parallel Processing Support


OpenMDAO uses MPI and PETSc for parallel processing. 
Installing an OpenMDAO compatible MPI and PETSc can be quite difficult, however 
[Anaconda Python](https://www.anaconda.com)  now allows installation of these utilities 
using their ``conda`` command. Before installation OpenMDAO should be installed and
"conda-forge" should be added to the conda “channels” list. To add "conda-forge",
enter the following commands in a terminal window::
```
conda config --add channels conda-forge
conda config --set channel_priority strict
```
Once access to "conda-forge" is setup, MPI and PETSc can be installed using the 
following commands:
```
conda install openmpi mpi4py
conda install petsc petsc4p
```

**Note: OpenMPI Not Available for Windows**  
OpenMPI does not currently seem to be available for Windows systems.
Windows users might consider using the Ubuntu subsystem.

## Generate Documentation

**GeNRoC** documentation can be generated using the python package 
[Sphinx](https://www.sphinx-doc.org/en/master/usage/quickstart.html)
and the files in the ``docs``
directory of the rcotools distribution by using the following commands:
```
cd path_to_docs
make html BUILDDIR=directory_path
```
if ``BUILDDIR`` argument is not provided, the HTML documents will be generated in
the ``docs/build/html`` directory.  Different document formats can be
built, just execute ``make`` without an argument to see which format targets are
available.


**LaTex PDF generation:**
If you have TeX and LaTeX available, run "``make latexpdf``," which runs
the LaTeX builder and invokes the pdfTeX toolchain for you. The PDF 
document will be generated in the ``docs/build/latex`` directory.

