#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import OrderedDict
import yaml
import math
import numpy as np
from scipy import interpolate

from rcotools.ndarc.ndarc_core import NdarcSolution

dict_pars_default=dict(
    MODEL_ind=2,
    MODEL_edge=0,

    Ki_hover=1.12,
    Ki_climb=1.08,
    Ki_prop=2,
    Ki_edge=2,

    CTs_Hind=0.08,
    kh1=0,
    kh2=0,
    Xh2=2,
    CTs_Pind=0.08,
    kp1=0,
    kp2=0,
    Xp2=2,
    CTs_Tind=0.08,
    kt1=0,
    kt2=0,
    Xt2=2,

    kpa=0,
    Xpa=2,
    
    kpx=0.,
    Xpx=1.,

    ko1=0,
    ko2=8,
    
    Maxial=1.176,
    Xaxial=0.65,
    mu_axtran=0.,

    mu_prop=1,
    ka1=0,
    ka2=0,
    ka3=0,
    Xa=4.5,

    mu_edge=0.35,
    ke1=0.8,
    ke2=0,
    ke3=1,
    Xe=4.5,
    kea=0,
    Ki_min=1,
    Ki_max=10,
        
        
    ### profile drag ###
    TECH_drag=1,
    Re_ref=0,
    X_Re=0.2,
    MODEL_basic=2,

    ncd=24,

    CTs_Dmin=0.07,
    d0_hel=0.0090,
    d0_prop=0.0090,
    d1_hel=0,
    d1_prop=0,
    d2_hel=0.5,
    d2_prop=0.5,
    dprop=0,
    Xprop=2,
    CTs_sep=0.07,
    dsep=4,
    Xsep=3,
    df1=0,
    df2=0,
    Xf=2,
    dz1=0,
    dz2=0,
    Xz=2,
    MODEL_stall=1,

    nstall=10,
    fstall=1,
    dstall1=2,
    dstall2=40,
    Xstall1=2,
    Xstall2=3,

    do1=0,
    do2=8,
    dsa=0,
    MODEL_comp=1,

    fSim=1,
    thick_tip=0.08,

    dm1=0.056,
    dm2=0.416,
    Xm=2,

    Mdd0=0.88,
    Mddcl=0.16,

    lst_mu_stall= [0.000, 0.050, 0.100, 0.150, 0.20, 0.250, 0.300, 0.350, 0.400, 0.5000, 0.6000, 0.7000, 0.8000, 0.9000, 1.0000, 5],
    lst_CTs_stall=[0.170, 0.160, 0.150, 0.140, 0.13, 0.120, 0.110, 0.100, 0.100, 0.1000, 0.1000, 0.1000, 0.1000, 0.1000, 0.1000, 0.1],
    lst_CTs_cd=[0.000, 0.010, 0.020, 0.030, 0.040, 0.05, 0.060, 0.070, 0.080, 0.090, 0.1000, 0.1100, 0.1200, 0.1300, 0.1400, 0.1500, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23],
    lst_cd=[0.01145, 0.01080, 0.01025, 0.00980, 0.00945, 0.00920, 0.00905, 0.00900, 0.00905, 0.00923, 0.00956, 0.01006, 0.01075, 0.01166, 0.01282, 0.01425, 0.01597, 0.01800, 0.02037, 0.02311, 0.02624, 0.02978, 0.03375, 0.03818],
    
    ### Custom CTs_stall model ###
    MODEL_stall_custom=0,       # 0:none, 1:Harris, 2:linear, 3:square, 4:configs
    # Harris
    Clmax_harris=1.2,
    # linear
    CTs_stall_hov=0.17,
    CTs_stall_04=0.1,
    # dCTs_stall_mu=0.2,
    # CTs_stall_min=0.1,
                
    ### Twin Rotors ###
    MODEL_twin='none',
    Kh_twin=1.0,
    Kp_twin=1.0,
    Kf_twin=0.85,
    Cind_twin=1.0,
    Caxial_twin=1.0,
    A_coaxial=1.05,
    
    ### Derived from twin geometry ### 
    xh=0.,
    xp=0.,
    xf1=0.,
    xf2=0.,
    xh_multi=1.0, # multicopter
    xf_multi=1.0,
    overlap_twin=0.,
    m_twin=0.,
    Hsep_twin=0.,
    )

class NDARCrotor:
    """ A class that represents the NDARC rotor submodels 
    
        Parameters
        ----------
        sigma : float, optional
            solidity. The default is None.
        twistL : float, optional
            linear twist. The default is None.
        Mtip : float, optional
            Tip Mach Number. The default is None.
        Re_75 : float, optional
            Re(r=0.75). The default is 0..
        twin : bool, optional
            (de-)activates twin model. The default is None.
        low_up : string, optional
            for coaxial model. The default is ''.
        i_rot : int, optional
            defines which of twin rotors to investigate. The default is 1.
        acd_sol_file : string, optional
            filepath to .acd or .soln file. The default is ''.
        dict_rot : dictionary, optional
            dictionary to initialize rotor object. The default is {}.
        yaml_file : string, optional
            filepath to .yaml file with dict_rot. The default is ''.

        Returns
        -------
        None.
    
    """

    def __init__(self, sigma=0., twistL=0., Mtip=0., twin=False, 
                 low_up='', i_rot=1, acd_sol_file='', dict_rot={}, yaml_file='', stall_config='hel'):
        # rotor parameters ###
        self.sigma=sigma
        
        self.twistL=twistL
        self.Mtip=Mtip       
        
        self.i_rot=i_rot
        self.twin=twin
        self.low_up=low_up
        self.stall_config=stall_config        
        
        self.vars_ind=['MODEL_ind','MODEL_edge','Ki_min','Ki_max','Ki_hover','Ki_climb','Ki_prop','Ki_edge','CTs_Hind','kh1','kh2','Xh2','CTs_Pind','kp1','kp2','Xp2','CTs_Tind','kt1','kt2','Xt2','kpa','Xpa','kpx','Xpx','ko1','ko2','Maxial','Xaxial','mu_axtran','mu_prop','ka1','ka2','ka3','Xa','mu_edge','ke1','ke2','ke3','Xe','kea','Ki_min','Ki_max']
        self.vars_pro=['TECH_drag','Re_ref','X_Re','MODEL_basic','ncd','CTs_Dmin','d0_hel','d0_prop','d1_hel','d1_prop','d2_hel','d2_prop','dprop','Xprop','CTs_sep','dsep','Xsep','df1','df2','Xf','dz1','dz2','Xz','MODEL_stall','nstall','fstall','dstall1','dstall2','Xstall1','Xstall2','do1','do2','dsa','MODEL_comp','fSim','thick_tip','dm1','dm2','Xm','Mdd0','Mddcl','lst_mu_stall','lst_CTs_stall','lst_CTs_cd','lst_cd']
        self.vars_CTs_stall=['MODEL_stall_custom', 'CTs_stall_hov', 'CTs_stall_04', 'Clmax_harris']
        self.vars_twin=['MODEL_twin', 'Kh_twin', 'Kp_twin', 'Kf_twin', 'Cind_twin', 'Caxial_twin', 'A_coaxial']#, 'xh_multi', 'xf_multi']
        self.vars_twin_geo=['overlap_twin', 'm_twin', 'Hsep_twin', 'xh', 'xp', 'xf1', 'xf2']
        
        for var in dict_pars_default:
            setattr(self, var, dict_pars_default[var])
            
        self.dict_acd={}
        if acd_sol_file:
            self.read_acd(acd_sol_file)
        if dict_rot:
            self.read_dict(dict_rot=dict_rot)
        if yaml_file:
            self.read_dict(yaml_file=yaml_file)
            
        if (not sigma or not twistL) and not acd_sol_file and not dict_rot and not yaml_file:
            raise ValueError('If no .acd or .soln file is defined, sigma, twistL and Mtip have to be input')
        
        # if not Mtip:
        #     print('No value for Mtip set. Make sure that Mtip is included in reference data')
        
        # if Re_75==0.: print('No RE_75 input. Reynolds scaling will be ignored')
                 
            
            
    def read_acd(self,file):
        """
        Reads model parameters from NDARC .soln or .acd file
        Based on RCOtools NdarcSolution
        
        Reads in sigma, Mtip and twistL
        if set_twist=2 in .acd, the twistL is calculated from twist(r)
        Mtip is calculated from csound and vtip. 
        
        Sets all model parameters that are defined in self.vars_ind and self.vars_pro
        and if twin=True in self.vars_twin
        
        Also reads geometry values for twin geometry defined in self.vars_geom
          

        Parameters
        ----------
        file : string
            filepath.

        Returns
        -------
        None.

        """
                
        acd=NdarcSolution(file)
        
        dict_acd={}
    
        dict_acd['sigma']=acd['Rotor'][self.i_rot]['sigma']
        
        set_twist=acd['Rotor'][self.i_rot]['SET_twist']
                
        if set_twist==1:
            dict_acd['twistL']=acd['Rotor'][self.i_rot]['twistL']
            # print('Using linear twist')
        elif set_twist==2:
            nprop=acd['Rotor'][self.i_rot]['nprop']
            rprop=list(acd['Rotor'][self.i_rot]['rprop'])[:nprop]
            lst_twist=list(acd['Rotor'][self.i_rot]['twist'])[:nprop]
            
            int_twist=interpolate.interp1d(rprop,lst_twist)
            dict_acd['twistL']=4.*(int_twist(0.8)-int_twist(0.55))
        
        vtip=acd['Rotor'][self.i_rot]['vtip_ref'][1]
        csound=acd['Aircraft']['csound']
        dict_acd['Mtip']=vtip/csound
        # print('Using Reference Tip Speed and Speed of Sound')
    
        for var in self.vars_ind:
            dict_acd[var]=acd['Rotor'][self.i_rot]['ProtorIND'][var]
        
        if self.twin:
            for var in self.vars_twin:
                dict_acd[var]=acd['Rotor'][self.i_rot]['ProtorIND'][var]
            
            for var in self.vars_twin_geo:
                if 'x' in var: dict_acd[var]=acd['Rotor'][self.i_rot]['ProtorIND'][var]
                else: dict_acd[var]=acd['Rotor'][self.i_rot][var]
            
        for var in self.vars_pro:
            if not 'lst_' in var:
                dict_acd[var]=acd['Rotor'][self.i_rot]['ProtorPRO'][var]
            else:
                if 'stall' in var:
                    lst_len=self.nstall
                elif 'cd' in var:
                    lst_len=self.ncd
                dict_acd[var]=list(acd['Rotor'][self.i_rot]['ProtorPRO'][var.replace('lst_','')])[0:lst_len]
                
        if self.MODEL_stall_custom:
            print("Custom model for CTs_stall will be disabled")
            dict_acd['MODEL_stall_custom']=0
            
        self.read_dict(dict_rot=dict_acd)
        setattr(self, 'dict_acd', dict_acd)
            
                
    def read_dict(self,dict_rot={}, yaml_file=''):
        """
        Reads dictionary with parameters and overwrites corresponding values ib rotor object

        Parameters
        ----------
        dict_rot : dict
            dictionary with all attributes of NDARCrotor.
        yaml_file : string
            filepath to .yaml file with dict with all attributes of NDARCrotor.

        Returns
        -------
        None.

        """        
        if yaml_file and not dict_rot:
            with open(yaml_file) as file:
                dict_rot = yaml.full_load(file)
            
        for var in dict_rot:
            setattr(self, var, dict_rot[var])
        
        if any(item in self.vars_CTs_stall for item in dict_rot.keys()):
            if self.MODEL_stall_custom == 1:
                self.model_stall_harris()
            elif self.MODEL_stall_custom==2:
                self.model_stall_linear()
            elif self.MODEL_stall_custom==3:
                self.model_stall_square()
            elif self.MODEL_stall_custom==4:
                self.model_stall_configs()
            
            
    def write_namelist(self, ignore_unchanged=True):
        """
        Returns a string that can be copy & pasted directly into a NDARC .list file
        Only outputs variables that have been changed in comparison to the 
        default values

        Returns
        -------
        string for namelist file

        """
        from rcotools.ndarc.ndarc_core import getNdarcUpdate
        
        dict_ndarc=OrderedDict()        
                
        for var in self.vars_ind:
            val = getattr(self, var)
            if val == dict_pars_default[var]: continue
            if self.dict_acd:
                if var in self.dict_acd and val == self.dict_acd[var] and ignore_unchanged: continue
            if isinstance(val, float): val=round(val,6)
            dict_ndarc['Rotor('+str(self.i_rot)+')%'+var] = val
            
        for var in self.vars_pro:
            if 'lst_' in var: 
                var_tmp=var.replace('lst_','')
            else:
                var_tmp=var
            
            val = getattr(self, var)
            if val == dict_pars_default[var] and ignore_unchanged: continue
            
            # if self.dict_acd:
            #     if var in self.dict_acd and val == self.dict_acd[var]: continue
            # This would exclude parameters that are unchanged to the initial acd file
            # Could lead to problems when replacing complete rotor model 
            # with the output
            
            # rounding values for nicer formatting
            if isinstance(val, float): val=round(val,6)
            if isinstance(val, list): val=[round(v,6) for v in val]
            dict_ndarc['Rotor('+str(self.i_rot)+')%'+var_tmp] = val
          
        if self.twin:
            for var in self.vars_twin:
                val = getattr(self, var)
                if val == dict_pars_default[var] and ignore_unchanged: continue
                if self.dict_acd:
                    if var in self.dict_acd and val == self.dict_acd[var] and ignore_unchanged: continue
                if isinstance(val, float): val=round(val,6)
                dict_ndarc['Rotor('+str(self.i_rot)+')%'+var] = val
                        
        return getNdarcUpdate(dict_ndarc, asString=True)
    
   
    def write_dict(self, yaml_file=''):
        """
        Returns dictionary with all model parameters and optionally writes it 
        to .yaml file

        Returns
        -------
        dict_rot : dict

        """
        
        dict_rot={}
        dict_rot['sigma']=self.sigma
        dict_rot['twistL']=self.twistL
        dict_rot['Mtip']=self.Mtip
        
        for var in ['twin','i_rot']+self.vars_ind+self.vars_pro+self.vars_CTs_stall+self.vars_twin+self.vars_twin_geo:
            dict_rot[var] = getattr(self, var)
            
        if yaml_file:
            with open(yaml_file, "w") as file:
                yaml_file = yaml.dump(dict_rot, file)
                
        return dict_rot
        
    def model_stall_harris(self):
        """
        Populates lst_mu_stall and lst_CTs_stall with a variation of CTs_stall 
        over mu_stall
        Model taken from equation 12.5 in "Rotorcraft Aeromechanics" by Johnson
        Reduces the list CTs_stall to only one independant variable "Clmax_harris"
        If MODEL_stall_custom==1:
            mu_stall=[0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
            CTs_stall[i]=Clmax/6*(1-mu_stall^2+9/4*mu_stall^4)/(1+8/3*mu_stall+3/2*mu_stall^2)

        Returns
        -------
        None.

        """
        self.nstall=12
        self.lst_mu_stall=[0.0,0.05,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.7,1.0]
            
        self.lst_CTs_stall=[self.Clmax_harris/6*(1-mu**2+9/4*mu**4)/(1+8/3*mu+3/2*mu**2) for mu in self.lst_mu_stall]
        
        self.lst_CTs_stall=[round(x, 4) for x in self.lst_CTs_stall]
        
        
    def model_stall_configs(self):
        """
        Populates lst_mu_stall and lst_CTs_stall with values from reference NDARC models.
        
        For calibration the reference values can be multiplied with self.fstall

        Returns
        -------
        None.

        """
        
        dict_stall = dict(
            hel = ([0.,.05,.10,.15,.20,.25,.30,.35,.40,.80], [.11,.10,.09,.085,.08,.0775,.075,.0725,.07,.07]),
            coax = ([0.,.05,.10,.15,.20,.25,.30,.35,.40,.50,.60,.70,.80,.90,1.00], [.145,.135,.12,.105,.09,.08,.07,.065,.06,.055,.05,.045,.04,.04,.04]),
            tand = ([0.,.05,.10,.15,.20,.25,.30,.35,.40,.80], [.145,.12,.095,.09,.085,.08,.075,.07,.065,.065]),
            tilt = ([0.,.05,.10,.15,.20], [.13,.10,.09,.08,.08]),
            modsus = ([.0,.0189,.0545,.0949,.1319,.1709,.2090,.2469,.2862,.3229,.3638,.3975,.4388,.4747,.5131,.5520,.5854],[.1600,.1597,.1588,.1577,.1561,.1538,.1510,.1473,.1426,.1374,.1308,.1247,.1166,.1085,.1008,.0922,.0853])
            )
        
        check=False
        for key, config in dict_stall.items():
            if key in self.stall_config:
                check=True
                self.nstall = len(config[0])
                self.lst_mu_stall = config[0]
                lst_CTs_stall = config[1]
        
        if not check: raise ValueError('Stall config not implemented')
        
        self.lst_CTs_stall=[round(float(x), 4) for x in lst_CTs_stall]
        
    
    def model_stall_linear(self):
        """
        Populates lst_mu_stall and lst_CTs_stall with a linear variation of 
        CTs_stall over mu_stall
        If MODEL_stall_custom==2:
            mu_stall=[0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 5.]
            dCTs_stall_mu=(CTs_stall_04-CTs_stall_hov)/0.4
            CTs_stall[i]=CTs_stall_hov+dCTs_stall_mu*mu_stall[i]

        Returns
        -------
        None.

        """
        self.nstall=12
        self.lst_mu_stall=[0.0,0.05,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.7,1.0]
            
        dCTs_stall_mu=(self.CTs_stall_04-self.CTs_stall_hov)/0.4
        self.lst_CTs_stall=[]
        self.lst_CTs_stall=[min(self.CTs_stall_hov,max(self.CTs_stall_04,self.CTs_stall_hov+dCTs_stall_mu*self.lst_mu_stall[i])) for i in range(self.nstall)]
        
        self.lst_CTs_stall=[round(x, 4) for x in self.lst_CTs_stall]   

    
    def model_stall_square(self):
        """
        Populates lst_mu_stall and lst_CTs_stall with a square variation of 
        CTs_stall over mu_stall
        If MODEL_stall_custom==3

        Returns
        -------
        None.

        """
        self.nstall=12
        self.lst_mu_stall=[0.0,0.05,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.7,1.0]
            
        b=self.CTs_stall_04
        a=(self.CTs_stall_hov-b)/0.4**2
        self.lst_CTs_stall=[min(self.CTs_stall_hov,max(self.CTs_stall_04,a*(mu-0.4)*b**2)) if mu<=0.4 else self.CTs_stall_04 for mu in self.lst_mu_stall]

        
        self.lst_CTs_stall=[round(x, 4) for x in self.lst_CTs_stall]           
                
        
    def calc_induced(self, CTs, mu, muz, offset=0):
        """
        NDARC induced model
        Directly translated to python from NDARC Fortran source code

        Parameters
        ----------
        CTs : float
            Blade Loading.
        mu : float
            advance ratio (sqrt(mux^2+muy^2)).
        muz : float
            axial advance ratio.
        offset : float
            lift offset.

        Returns
        -------
        kappa : float
            inflow factor.

        """
        mu=abs(mu)
        if mu==0. and muz==0.: 
            Kconstant=self.Ki_hover
        elif abs(mu)<0.1*abs(muz):
            Kconstant=self.Ki_prop
        else:
            Kconstant=self.Ki_edge
            
        
        if muz==0.:
            transition=0.
        elif CTs==0.:
            transition=1.
        else:
            transition=2/math.pi*math.atan((abs(muz/((CTs*self.sigma*0.5)**0.5*self.Maxial)))**self.Xaxial)
        
        
        Kh=self.Ki_hover+self.kh1*(CTs-self.CTs_Hind)+self.kh2*abs(CTs-self.CTs_Hind)**self.Xh2+(self.Ki_climb-self.Ki_hover)*transition
     
        # axial
        fprop=self.ka2*self.mu_prop**2+self.ka3*(abs(self.mu_prop))**self.Xa
        if fprop!=0.:fprop=1./fprop
        
        if self.mu_prop==0. or muz==0 or abs(muz)<self.mu_axtran:
            Kaxial=Kh
        else:
            Deltap=CTs-self.CTs_Pind
            Kp=self.Ki_prop+self.kp1*Deltap
            if Deltap!=0.: Kp=Kp+self.kp2*abs(Deltap)**self.Xp2
            if mu!=0.: Kp=Kp+self.kpa*(muz**2)*(abs(mu))**self.Xpa
            if self.kpx!=0.:
                if CTs!=0.: Kp=Kp+self.kpx*(abs(CTs)/muz**2)**self.Xpx
                if self.CTs_Pind!=0: Kp=Kp-self.kpx*((abs(self.CTs_Pind)/self.mu_prop**2)**self.Xpx)
            scale_a=(Kp-(Kh+self.ka1*self.mu_prop))*fprop
            Kaxial=Kh+self.ka1*abs(muz)+scale_a*(self.ka2*abs(muz)**2+self.ka3*(abs(muz)**self.Xa))
   
        # edgewise
        fedge=self.ke2*self.mu_edge**2+self.ke3*(abs(self.mu_edge))**self.Xe
        if fedge!=0.:fedge=1./fedge
        
        if self.mu_edge==0. or mu==0.:
            Ki=Kaxial
        else:
            foffset=1.-self.ko1*(1.-math.exp(-self.ko2*offset))
            falpha=1.-self.kea*muz
            fmin=1./abs(self.Ki_edge)
            foffset=max(fmin,foffset)
            falpha=max(fmin,falpha)
            
            Deltat=CTs-self.CTs_Tind
            Ke=self.Ki_edge+self.kt1*Deltat
            if Deltat!=0.: Ke=Ke+self.kt2*abs(Deltat)**self.Xt2
            Ke=Ke*foffset*falpha
            if self.MODEL_edge<=0: 
                scale_e=(Ke-(Kaxial+self.ke1*self.mu_edge))*fedge
            else:
                scale_e=(Ke-self.ke1*self.mu_edge)*fedge
                
            Ki=Kaxial+self.ke1*mu+scale_e*(self.ke2*mu**2+self.ke3*mu**self.Xe)
        
        if self.MODEL_ind==1:
            kappa=Kconstant
        else:
            kappa=max(self.Ki_min,min(self.Ki_max,Ki))
        
        return kappa
    
    
    def calc_profile(self, CTs, mu, muz, offset=0., Mtip=0., Re_75=0.):
        """
        NDARC profile drag model
        Directly translated to python from NDARC Fortran source code
        Only addition is the implementation of stall models with possibility
        to add more

        Parameters
        ----------
        CTs : float
            Blade Loading.
        mu : float
            advance ratio (sqrt(mux^2+muy^2)).
        muz : float
            axial advance ratio.
        offset : float
            lift offset.
        Mtip : float
            Tip Mach Number.The default is 0. If Mtip=0 the atrribute self.Mtip is used

        Returns
        -------
        cdmean : float
            mean drag coefficient.
        FP : float
            drag inflow factor.

        """
        
        if not Mtip: Mtip=self.Mtip
                
        Mat=Mtip*((1+mu)**2+muz**2)**0.5
        
        mu=abs(mu)
        mu_xyz_sq=mu**2+muz**2
        mu_xyz=mu_xyz_sq**0.5
        
        if self.MODEL_stall==0:
            cdstall=0
        else:
        
            # Add additional stall model functions here
            if self.MODEL_stall_custom == 1:
                self.model_stall_harris()
            elif self.MODEL_stall_custom==2:
                self.model_stall_linear()
            elif self.MODEL_stall_custom==3:
                self.model_stall_square()
            elif self.MODEL_stall_custom==4:
                self.model_stall_configs()
            # else:
                # self.lst_mu_stall=dict_pars_default['lst_mu_stall']
                # self.lst_CTs_stall=dict_pars_default['lst_CTs_stall']
                
            self.lst_mu_stall=self.lst_mu_stall[:self.nstall]   
            self.lst_CTs_stall=self.lst_CTs_stall[:self.nstall]    
                       
            # Stall interpolation directly from source code
            CTs_stall=interpolate.interp1d(self.lst_mu_stall,self.lst_CTs_stall)(min(mu_xyz,max(self.lst_mu_stall)))
            foffset=max(.001,1.-self.do1*(1.-math.exp(-self.do2*offset)))
            falpha=max(.001,1.-self.dsa*muz)
            Delta_CTs_stall=max(0,abs(CTs)-self.fstall*CTs_stall/(falpha*foffset))
                
            cdstall=self.dstall1*(Delta_CTs_stall**self.Xstall1)+self.dstall2*(Delta_CTs_stall**self.Xstall2)
        
        ### Tabular stall model
        if self.MODEL_basic == 1:    
            if len(self.lst_CTs_cd) != len(self.lst_cd):
                raise ValueError('CTs_cd must have same length as cd')
            else:
                self.ncd=len(self.lst_CTs_cd)
                
            lst_C0_cd=[]
            lst_C1_cd=[]
            for i, (i_mu, i_cts) in enumerate(zip(self.lst_CTs_cd,self.lst_cd)):
                if i==len(self.lst_CTs_cd)-1:
                    c0=i_cts
                    c1=0.
                else:
                    c0=(self.lst_cd[i]*self.lst_CTs_cd[i+1]-self.lst_cd[i+1]*self.lst_CTs_cd[i])/(self.lst_CTs_cd[i+1]-self.lst_CTs_cd[i])
                    c1=(self.lst_cd[i+1]-self.lst_cd[i])/(self.lst_CTs_cd[i+1]-self.lst_CTs_cd[i])
                    
                lst_C0_cd.append(c0)
                lst_C1_cd.append(c1)
                
            C0_cd_int=interpolate.interp1d(self.lst_CTs_cd,lst_C0_cd)(CTs)
            C1_cd_int=interpolate.interp1d(self.lst_CTs_cd,lst_C1_cd)(CTs)
        
        
        clat=5.7*(1.6*(1-2.97*mu+2.21*mu**2)*(6*CTs/5.7)+0.25*self.twistL/180*math.pi)
        
        Mdd=self.Mdd0-self.Mddcl*abs(clat)
        
        Ksim=max(-1,min(-0.2,(Mat**2-1)/((Mat**2*self.thick_tip*2.4)**(2/3))))
        
        
        if mu_xyz_sq==0:
            FP=1
        else:
            FP=self.calc_FP(mu,muz)       
        
        cdsep=self.dsep*(max(0,abs(CTs)-self.CTs_sep))**self.Xsep
        
        _,lambdah=self.ideal_inflow(CTs, mu, muz)
        x_lh=lambdah
        if x_lh != 0.:
            x_lh=abs(muz)/x_lh
            x_lh=2./math.pi*math.atan(x_lh)
        
        delta_Dmin=abs(CTs-self.CTs_Dmin)
        cdh=self.d0_hel+self.d1_hel*delta_Dmin+self.d2_hel*delta_Dmin**2+cdsep
        cdp=self.d0_prop+self.d1_prop*delta_Dmin+self.d2_prop*delta_Dmin**2+cdsep
        if mu != 0.:
            cdp=cdp+self.dprop*muz**2*abs(mu)**self.Xprop
        
        
        cdf=self.df1*mu+self.df2*(abs(mu)**self.Xf)
        cdz=self.dz1*mu+self.dz2*(abs(mu)**self.Xz)
        
        if self.MODEL_basic==1:
            cdbasic=C0_cd_int+C1_cd_int*CTs
        else:
            cdbasic=cdh+(cdp-cdh)*x_lh+cdf+cdz
                        
        
        if self.MODEL_comp==0:
            cdcomp=0
        elif self.MODEL_comp==1:
            cdcomp=self.dm1*(max(0,Mat-Mdd))+self.dm2*(max(0,Mat-Mdd))**self.Xm
        elif self.MODEL_comp==2:
            cdcomp=2.3548*self.fSim*(Ksim+1)**2*((1+mu)*self.thick_tip)**2.5
        else:
            raise ValueError('MODEL_comp=3 is deprecated')  
        
        cdmean=self.TECH_drag*(cdbasic+cdstall+cdcomp)
        
        if self.Re_ref != 0. and Re_75!=0.:
            cdmean=cdmean*(self.Re_ref/Re_75)**self.X_Re
            
        return cdmean
    
    
    def calc_FP(self, mu, muz):
        """
        Calculates the edgewise profile power correction factor FP

        Parameters
        ----------
        mu : float
            advance ratio (sqrt(mux^2+muy^2)).
        muz : float
            axial advance ratio (sqrt(mux^2+muy^2)).

        Returns
        -------
        FP : float
            edgewise profile power correction factor.

        """
        mu=abs(mu)
        mu_xyz_sq=mu**2+muz**2
        mu_xyz=mu_xyz_sq**0.5
        mu_inv=1/(1+mu_xyz_sq)
        if mu_xyz_sq==0:
            FP=1
        else:
            FP=(1+mu_xyz_sq)**0.5*(1+2.5*mu_xyz_sq+0.375*mu**2*(4.+7.*mu_xyz_sq+4.*mu_xyz_sq**2)/(1+mu_xyz_sq)**2-0.5625*mu**4/(1+mu_xyz_sq))+(1.5*muz**4+1.5*muz**2*mu**2+0.5625*mu**4)*np.log(((1+mu_xyz_sq)**0.5+1)/mu_xyz)
        
        return FP
    
    
    def ideal_inflow(self, CTs, mu, muz):
        """
        Ideal Inflow calculation
        Directly translated to python from NDARC Fortran source code

        Parameters
        ----------
        CTs : float
            Blade Loading.
        mu : float
            advance ratio (sqrt(mux^2+muy^2)).
        muz : float
            axial advance ratio.

        Returns
        -------
        lideal : float
            Lambda ideal.
        lambdah : float
            Lambda hover.

        """
        
        mu=abs(mu)
        CT=CTs*self.sigma
        
        if CT == 0.:
            lideal=0.
            lambdah=0.
            return lideal, lambdah
        
        else:
            st=CT/abs(CT)
            lh=abs(0.5*CT)**0.5
            lambdah=lh
                        
            ### axial ###
            if mu**2 < 0.0001*lh**2:
                if st*muz >= -lh:
                    dl=abs((.5*muz)**2+lh**2)
                    if dl != 0.: dl=(abs(dl))**0.5
                    l=0.5*muz+st*dl
                elif st*muz < -2.*lh:
                    dl=abs((0.5*muz)**2-lh**2)
                    if dl != 0.: dl=(abs(dl))**0.5
                    l=0.5*muz+st*dl
            ### vortex ring ###
            elif (1.5*mu**2.+(2.*st*muz+3.*lh)**2) < lh**2:
                l=muz*((.3726780*muz**2+.5980197*mu**2)/lh**2-.9907120)
            ### forward flight ###
            else:
                l=(st*lh+muz)**2+mu**2
                if l != 0: l=st*lh**2/l**0.5+muz
                for i in range(100):
                    root=abs(l**2+mu**2)
                    if root != 0: root=1./root**0.5
                    lhat=st*lh**2*root
                    den=1.+l*lhat*root**2
                    if den != 0.: den=1./den
                    dl=(l-muz-lhat)*den*0.5
                    l=l-dl
                    if abs(dl) <= abs(0.00001*l): 
                        break
            
            lideal=l-muz
            
            return lideal, lambdah
            
        
    def ideal_inflow_twin(self, CTs, mu, muz, CTs2):
        '''
        Ideal Inflow calculations for twin rotors
        Directly translated to python from NDARC Fortran source code
        Limitation: CTs2 has to in reference data. 
        
        Parameters
        ----------
        CTs : float
            Blade Loading.
        mu : float
            advance ratio (sqrt(mux^2+muy^2)).
        muz : float
            axial advance ratio.
        CTs2 : float
            Blade Loading of other rotor.

        Raises
        ------
        ValueError
            DESCRIPTION.

        Returns
        -------
        None.

        '''        
        
        xh=self.xh
        xp=self.xp
        xf1=self.xf1
        xf2=self.xf2
        
        # xh=1./(2-self.m_twin)
        # xp=xh
        
        if self.MODEL_twin =='none':
            raise ValueError('Twin Rotor Model is \'none\'')
        if self.MODEL_twin =='config':
            raise ValueError('Twin Rotor Model is \'config\'. This is not clear')
        if self.MODEL_twin =='multirotor':
            raise ValueError('Twin Rotor Model \'multirotor\' is not implemented yet')
        # if self.MODEL_twin =='tiltrotor':
        #     raise ValueError('Twin Rotor Model \'tiltrotor\' is not implemented yet')
        # if self.MODEL_twin =='side-by-side':
        #     raise ValueError('Twin Rotor Model \'side-by-side\' is not implemented yet')
        # if self.MODEL_twin =='coaxial':
        #     xf1=1.
        #     xf2=1.
        # if self.MODEL_twin =='tandem':
        #     xf1=self.m_twin
        #     xf2=2.-self.m_twin
            
        
        CT=CTs*self.sigma
        CT2=CTs2*self.sigma
        
        wa=muz**2/(muz**2+self.Caxial_twin*abs(0.5*(CT+CT2)))
        w=mu**2/(mu**2+self.Cind_twin*abs(0.5*(CT+CT2)))
        
        if self.MODEL_twin =='coaxial':
            xh=1.
            if 'low' in self.low_up:
                tau=1.
                if CT2 != 0:  tau=abs(CT/CT2)
                if tau ==0: tau=0.6666667
                xh=((abs(1.+4.*((1+tau)**2)*self.A_coaxial*tau))**0.5-1.)**2/(4.*tau**3)
            x1=xf1*w+xh*(1.-w)
            x2=xf2*w
            Ktwin=self.Kf_twin*w+(1.-w)
            
        if self.MODEL_twin =='tandem' or self.MODEL_twin =='side-by-side' or self.MODEL_twin =='tiltrotor':
            x1=xf1*w+xh*(1-w)
            x2=xf2*w+xh*(1-w)
            Ka=self.Kp_twin*wa+self.Kh_twin*(1-wa) # Twin Model for next NDARC version
            Ktwin=self.Kf_twin*w+Ka*(1-w)           # ""
            # Ktwin=self.Kf_twin*w+self.Kh_twin*(1-w)
            
        CTes=(x1*CT+x2*CT2)/self.sigma
        
        lideal, lambdah = self.ideal_inflow(CTes, mu, muz)
        lideal=lideal*Ktwin
            
        return lideal, lambdah
    
    def calc_CPs_ind(self, CTs, mu, muz, offset=0, CTs2=None):
        """
        Calculates CPs_ind from kappa and ideal inflow
        
        Returns
        -------
        CPs_ind : float

        """
        CT=CTs*self.sigma
        if not self.twin:
            lideal, _= self.ideal_inflow(CTs, mu, muz)
        else:
            lideal, _= self.ideal_inflow_twin(CTs, mu, muz, CTs2)
        
        CPideal=CT*lideal
        
        kappa=self.calc_induced(CTs, mu, muz, offset)
        CPs_ind=kappa*CPideal/self.sigma
        
        return CPs_ind 
    
    def kappa_from_CPs_ind(self, CPs_ind, CTs, mu, muz, offset=0, CTs2=0.):
        """
        Calculates kappa from CPs_ind and ideal inflow
        
        Returns
        -------
        kappa : float

        """
        
        CT=CTs*self.sigma
        if not self.twin:
            lideal, _= self.ideal_inflow(CTs, mu, muz)
        else:
            lideal, _= self.ideal_inflow_twin(CTs, mu, muz, CTs2)
        
        CPideal=CT*lideal
        
        kappa=CPs_ind*self.sigma/CPideal
        
        return kappa
    
    
    def calc_CPs_pro(self, CTs, mu, muz, offset=0., Mtip=0., Re_75=0.):
        """
        Calculates CPs_pro from cdmean and FP factor
        
        Returns
        -------
        CPs_pro : float

        """
        ### profile power ###
        cdmean = self.calc_profile(CTs, mu, muz, offset, Mtip, Re_75)
        FP = self.calc_FP(mu, muz)
        
        CPs_pro=1/8*cdmean*FP
        
        return CPs_pro
    
    def cdmean_from_CPs_pro(self, CPs_pro, mu, muz):
        """
        Calculates cdmean from CPs_pro and FP factor
        
        Returns
        -------
        cdmean : float

        """
        ### profile power ###
        FP = self.calc_FP(mu, muz)
        
        cdmean=8*CPs_pro/FP
        
        return cdmean
    
    
    def calc_CPs(self, CTs, mu, muz, offset=0, Mtip=0, Re_75=0., CTs2=None):
        """
        Calculates CPs from CPs_ind and CPs_pro
        
        Returns
        -------
        CPs : float

        """
            
        ### induced power ### offset)
        CPs_ind=self.calc_CPs_ind(CTs, mu, muz, offset, CTs2)
      
        ### profile power ###
        CPs_pro=self.calc_CPs_pro(CTs, mu, muz, offset, Mtip, Re_75)
        
        ### total power ###
        CPs=CPs_ind+CPs_pro
        
        return CPs, CPs_ind, CPs_pro
    
    def calc_all(self, CTs, mu, muz, offset=0, Mtip=0, CTs2=None):
        """
        Outputs all available output parameters as a ditionary
        
        Returns
        -------
        dictionary 
        """
        
        kappa=self.calc_induced(CTs, mu, muz, offset)
        cdmean = self.calc_profile(CTs, mu, muz, offset, Mtip)
        CPs, CPs_ind, CPs_pro=self.calc_CPs(CTs, mu, muz, offset, Mtip, CTs2)
        
        return {'kappa':kappa, 'cdmean':cdmean, 'CPs':CPs, 'CPs_ind':CPs_ind, 'CPs_pro':CPs_pro}
    
    def write_excel(self):
        """
        Returns a string that can be printed with print() and then 
        copy and pasted into the Excel rotor calibration document by Johnson

        """
        
        args_induced=dict(MODEL_ind=self.MODEL_ind,Ki_hover=self.Ki_hover,
                          Ki_climb=self.Ki_climb,Ki_prop=self.Ki_prop,
                          Ki_edge=self.Ki_edge,CTs_Hind=self.CTs_Hind,
                          kh1=self.kh1,kh2=self.kh2,Xh2=self.Xh2,
                          CTs_Pind=self.CTs_Pind,kp1=self.kp1,kp2=self.kp2,
                          Xp2=self.Xp2,CTs_Tind=self.CTs_Tind,kt1=self.kt1,
                          kt2=self.kt2,Xt2=self.Xt2,kpa=self.kpa,Xpa=self.Xpa,
                          ko1=self.ko1,ko2=self.ko2,Maxial=self.Maxial,
                          Xaxial=self.Xaxial,mu_prop=self.mu_prop,ka1=self.ka1,
                          ka2=self.ka2,ka3=self.ka3,Xa=self.Xa,mu_edge=self.mu_edge,
                          ke1=self.ke1,ke2=self.ke2,ke3=self.ke3,Xe=self.Xe,
                          kea=self.kea,Ki_min=self.Ki_min,Ki_max=self.Ki_max)
        
        
        # \b(\w+) reference: \1
        str_induced='''MODEL_ind,{MODEL_ind}
        
Ki_hover,{Ki_hover}
Ki_climb,{Ki_climb}
Ki_prop,{Ki_prop}
Ki_edge,{Ki_edge}

CTs_Hind,{CTs_Hind}
kh1,{kh1}
kh2,{kh2}
Xh2,{Xh2}
CTs_Pind,{CTs_Pind}
kp1,{kp1}
kp2,{kp2}
Xp2,{Xp2}
CTs_Tind,{CTs_Tind}
kt1,{kt1}
kt2,{kt2}
Xt2,{Xt2}

kpa,{kpa}
Xpa,{Xpa}

ko1,{ko1}
ko2,{ko2}
Maxial,{Maxial}
Xaxial,{Xaxial}

mu_prop,{mu_prop}
ka1,{ka1}
ka2,{ka2}
ka3,{ka3}
Xa,{Xa}

mu_edge,{mu_edge}
ke1,{ke1}
ke2,{ke2}
ke3,{ke3}   
Xe,{Xe}
kea,{kea}
Ki_min,{Ki_min}
Ki_max,{Ki_max}
        '''.format(**args_induced)
        
        args_profile=dict(TECH_drag=self.TECH_drag,Re_ref=self.Re_ref,
                          X_Re=self.X_Re,MODEL_basic=self.MODEL_basic,
                          ncd=self.ncd,CTs_Dmin=self.CTs_Dmin,d0_hel=self.d0_hel,
                          d0_prop=self.d0_prop,d1_hel=self.d1_hel,
                          d1_prop=self.d1_prop,d2_hel=self.d2_hel,
                          d2_prop=self.d2_prop,dprop=self.dprop,Xprop=self.Xprop,
                          CTs_sep=self.CTs_sep,dsep=self.dsep,Xsep=self.Xsep,
                          df1=self.df1,df2=self.df2,Xf=self.Xf,dz1=self.dz1,
                          dz2=self.dz2,Xz=self.Xz,MODEL_stall=self.MODEL_stall,
                          nstall=self.nstall,fstall=self.fstall,dstall1=self.dstall1,
                          dstall2=self.dstall2,Xstall1=self.Xstall1,
                          Xstall2=self.Xstall2,do1=self.do1,do2=self.do2,
                          dsa=self.dsa,MODEL_comp=self.MODEL_comp,fSim=self.fSim,
                          thick_tip=self.thick_tip,dm1=self.dm1,dm2=self.dm2,
                          Xm=self.Xm,Mdd0=self.Mdd0,Mddcl=self.Mddcl,          
                          )
        
        str_profile='''TECH_drag,{TECH_drag}
Re_ref,{Re_ref}
X_Re,{X_Re}
MODEL_basic,{MODEL_basic}

ncd,{ncd}

CTs_Dmin,{CTs_Dmin}
d0_hel,{d0_hel}
d0_prop,{d0_prop}
d1_hel,{d1_hel}
d1_prop,{d1_prop}
d2_hel,{d2_hel}
d2_prop,{d2_prop}
dprop,{dprop}
Xprop,{Xprop}
CTs_sep,{CTs_sep}
dsep,{dsep}
Xsep,{Xsep}
df1,{df1}
df2,{df2}
Xf,{Xf}
dz1,{dz1}
dz2,{dz2}
Xz,{Xz}
MODEL_stall,{MODEL_stall}

nstall,{nstall}
fstall,{fstall}
dstall1,{dstall1}
dstall2,{dstall2}
Xstall1,{Xstall1}
Xstall2,{Xstall2}

do1,{do1}
do2,{do2}
dsa,{dsa}
MODEL_comp,{MODEL_comp}

fSim,{fSim}
thick_tip,{thick_tip}

dm1,{dm1}
dm2,{dm2}
Xm,{Xm}

Mdd0,{Mdd0}
Mddcl,{Mddcl}
        '''.format(**args_profile)
        
        
        str_stall='mu_stall,CTs_stall\n'
        for i in range(len(self.lst_mu_stall)):
            str_stall+=str(self.lst_mu_stall[i])+','+str(self.lst_CTs_stall[i])+'\n'
        
        return '####INDUCED###\n\n' + str_induced + '\n\n####PROFILE###\n\n' +  str_profile + '\n\n####STALL###\n\n' +  str_stall
        
    
    
    def vars_default_deprecated(self):
        """
        this is only here so that my IDE recognizes all variables for autocomplete :D
    
        Raises
        ------
        ValueError
            DESCRIPTION.
    
        Returns
        -------
        None.
    
        """
        ### induced ###
        self.MODEL_ind=2
        self.MODEL_edge=0
    
        self.Ki_hover=1.12
        self.Ki_climb=1.08
        self.Ki_prop=2
        self.Ki_edge=2
    
        self.CTs_Hind=0.08
        self.kh1=0
        self.kh2=0
        self.Xh2=2
        self.CTs_Pind=0.08
        self.kp1=0
        self.kp2=0
        self.Xp2=2
        self.CTs_Tind=0.08
        self.kt1=0
        self.kt2=0
        self.Xt2=2
    
        self.kpa=0
        self.Xpa=2
        self.kpx=0.
        self.Xpx=1.
    
        self.ko1=0
        self.ko2=8
        self.Maxial=1.176
        self.Xaxial=0.65
        self.mu_axtran=0.
    
        self.mu_prop=1
        self.ka1=0
        self.ka2=0
        self.ka3=0
        self.Xa=4.5
    
        self.mu_edge=0.35
        self.ke1=0.8
        self.ke2=0
        self.ke3=1
        self.Xe=4.5
        self.kea=0
        self.Ki_min=1
        self.Ki_max=10
        
        
        ### profile drag ###
        self.TECH_drag=1
        self.Re_ref=0
        self.X_Re=0.2
        self.MODEL_basic=2
    
        self.ncd=24
    
        self.CTs_Dmin=0.07
        self.d0_hel=0.0090
        self.d0_prop=0.0090
        self.d1_hel=0
        self.d1_prop=0
        self.d2_hel=0.5
        self.d2_prop=0.5
        self.dprop=0
        self.Xprop=2
        self.CTs_sep=0.07
        self.dsep=4
        self.Xsep=3
        self.df1=0
        self.df2=0
        self.Xf=2
        self.dz1=0
        self.dz2=0
        self.Xz=2
        self.MODEL_stall=1
    
        self.nstall=10
        self.fstall=1
        self.dstall1=2
        self.dstall2=40
        self.Xstall1=2
        self.Xstall2=3
    
        self.do1=0
        self.do2=8
        self.dsa=0
        self.MODEL_comp=1
    
        self.fSim=1
        self.thick_tip=0.08
    
        self.dm1=0.056
        self.dm2=0.416
        self.Xm=2
    
        self.Mdd0=0.88
        self.Mddcl=0.16
    
        self.lst_mu_stall=[0.000, 0.050, 0.100, 0.150, 0.20, 0.250, 0.300, 0.350, 0.400, 0.5000, 0.6000, 0.7000, 0.8000, 0.9000, 1.0000, 5]
        self.lst_CTs_stall=[0.170, 0.160, 0.150, 0.140, 0.13, 0.120, 0.110, 0.100, 0.100, 0.1000, 0.1000, 0.1000, 0.1000, 0.1000, 0.1000, 0.1]
    
        self.lst_CTs_cd=[0.000, 0.010, 0.020, 0.030, 0.040, 0.05, 0.060, 0.070, 0.080, 0.090, 0.1000, 0.1100, 0.1200, 0.1300, 0.1400, 0.1500, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23]
        self.lst_cd=[0.01145, 0.01080, 0.01025, 0.00980, 0.00945, 0.00920, 0.00905, 0.00900, 0.00905, 0.00923, 0.00956, 0.01006, 0.01075, 0.01166, 0.01282, 0.01425, 0.01597, 0.01800, 0.02037, 0.02311, 0.02624, 0.02978, 0.03375, 0.03818]
        ### Custom CTs_stall model ###
        self.MODEL_stall_custom=0       # 0:none, 1:Harris, 2:linear
        # Harris
        self.Clmax_harris=1.2
        # linear
        self.CTs_stall_hov=0.17
        self.CTs_stall_04=0.1
        
        # self.dCTs_stall_mu=0.2
        # self.CTs_stall_min=0.1
             
        ### Twin Rotors ###
        self.MODEL_twin='none'
        self.Kh_twin=1.0
        self.Kp_twin=1.0
        self.Kf_twin=0.85
        self.Cind_twin=1.0
        self.Caxial_twin=1.0
        self.A_coaxial=1.05
        self.xh=0.
        self.xp=0.
        self.xf1=0.
        self.xf2=0.
        # self.xh_multi=1.0 # multicopter
        # self.xf_multi=1.0 # ""
        
        self.overlap_twin=0.
        self.m_twin=0.
        self.Hsep_twin=0.
        self.Vsep_twin=0.

    
# if __name__ == "__main__":
    # solnfile='~/work/tom-ndarc/models/BK117_D3/BK117D3_opt/BK117D3.soln'
    # rot = NDARCrotor(sigma=0.1,twistL=-10, Mtip=0.65)
    # vardict=rot.calc_all(0.0382, 0.2284,0.0318)
    # rot.CTs_Tind=5
