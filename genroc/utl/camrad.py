#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 11:00:12 2022

@author: vzappek
"""
import os
import re
import math
import copy
import pandas as pd
import numpy as np
from collections import OrderedDict

from rcotools.camrad.camrad_mdao import CamradWrapper
import rcotools.camrad.camrad_core as cc
from openmdao.api import IndepVarComp, Problem, Group
from openmdao.api import DOEDriver, ListGenerator
from openmdao.api import SqliteRecorder
from genroc.utl.utl_mdao import def_input, sample_full_ivc


    
def sweeps_cal_johnson(TOW_ref, Mtip_ref=0., V_Vtip_ref=(), V_RPM_ref=(), V_ref=0., speed_sound=343., sample_res=1.):
    """
    Script that creates case definitions for CAMRAD sweeps in accordance with 
    NDARC rotor calibration manual by Johnson.
    Sweeps for hover, edgewise flight and (optionally) compressibilty are created.
    TOW_ref has to be given and should correspond roughly to CTs=0.08
    For advance ratio sweeps and tip Mach number sweeps Mtip_ref is sufficient
    as input.  
    V_ref=Mtip_ref*mu_ref*speed_sound  with mu_ref=0.3
    
    The sweeps look as follows:
        hover:
        tow=0.5*TOW_ref....2*TOW_ref; n=21
        
        edgewise:
        tow=1*TOW_ref....1.75*TOW_ref; n=6
        speed=0.5*V_ref....1.5*V_ref; n=11
        
        compresibility:
        speed=0.5*V_ref....1.5*V_ref; n=11
        tip_speed=0.85*tip_ref....1.15*tip_ref; n=3
        

    Parameters
    ----------
    TOW_ref : float
        Reference weight. Should correspond to roughly CTs=0.08.
        The default is 0..
    Mtip_ref : tuple, optional
        Reference tip mach number. The Reference speed will be calculated as:
            V_ref=Mtip_ref*mu_ref*speed_sound (mu_ref=0.3)
        The default is 0.
    V_Vtip_ref : tuple, optional
        Tuple fo reference speed and tip speed. 
        The default is ().
    V_RPM_ref : tuple, optional
        tuple of reference speed and RPM. 
        The default is ().
    V_ref : float, optional
        Reference speed if none of above is given. Compressibility 
        will be ignored.
        The default is 0.
    speed_sound : float, optional
        Speed of sound. The default is 343.

    Returns
    -------
    dict with cases for hover, edgewise and compressibility sweep.

    """
    mu_ref=0.3
    str_tip='MTIP'
    if Mtip_ref:
        V_ref=Mtip_ref*mu_ref*speed_sound
        tip_ref=Mtip_ref
    elif V_Vtip_ref:
        V_ref=V_Vtip_ref[0]
        tip_ref=V_Vtip_ref[1]
        str_tip='VTIP'
    elif V_RPM_ref:
        V_ref=V_RPM_ref[0]
        tip_ref=V_RPM_ref[1]
        str_tip='RPM'
    elif V_ref !=0.:
        str_tip=''
        print('No input for reference tip speed. No compressibility sweep will be defined')
    else:
        raise ValueError('Not enough inputs for reference speed definition')
        
    hover={}
    hover['in_tow']  = {'accesstext': 'CASE 1%SHELL%AIRFRAME:STRUCTURE%WEIGHT', 'values':list(np.linspace(TOW_ref*0.5,TOW_ref*2.,int(math.ceil(21)))), 'metadict':{'val':TOW_ref}}
    hover['in_speed']  = {'accesstext': 'CASE 1%SHELL%TRIM%SPEED', 'values':[0.], 'metadict':{'val':0.}}
    hover['in_'+str_tip.lower()]  = {'accesstext': 'CASE 1%SHELL%TRIM%'+str_tip, 'values':[tip_ref], 'metadict':{'val':tip_ref}}
 
    edge={}
    edge['in_tow']  = {'accesstext': 'CASE 1%SHELL%AIRFRAME:STRUCTURE%WEIGHT', 'values':list(np.linspace(TOW_ref,TOW_ref*1.75,int(math.ceil(6)))), 'metadict':{'val':V_ref}}
    edge['in_speed']  = {'accesstext': 'CASE 1%SHELL%TRIM%SPEED', 'values':list(np.linspace(V_ref*0.3,V_ref*1.8,int(math.ceil(16)))), 'metadict':{'val':0.}}
    edge['in_'+str_tip.lower()]  = {'accesstext': 'CASE 1%SHELL%TRIM%'+str_tip, 'values':[tip_ref], 'metadict':{'val':tip_ref}}
    
    compress={}
    compress['in_tow']  = {'accesstext': 'CASE 1%SHELL%AIRFRAME:STRUCTURE%WEIGHT', 'values':[TOW_ref], 'metadict':{'val':TOW_ref}} 
    compress['in_speed']  = {'accesstext': 'CASE 1%SHELL%TRIM%SPEED', 'values':list(np.linspace(V_ref*0.5,V_ref*1.5,int(math.ceil(11)))), 'metadict':{'val':0.}}
    compress['in_'+str_tip.lower()]  = {'accesstext': 'CASE 1%SHELL%TRIM%'+str_tip, 'values':list(np.linspace(tip_ref*0.85,tip_ref*1.15,int(math.ceil(3)))), 'metadict':{'val':tip_ref}}
    
    return OrderedDict(hover=hover,edge=edge, compress=compress)


def camrad_sweep(jobname, jobfile, cases, dir_output='Output', outputs=[], setenv = {}, samples=[], replacenan=True):
    """
    Executes CAMRAD sweeps using OpenMDAO Wrapper

    Parameters
    ----------
    jobname : str
        jobname.
    jobfile : str
        path to CAMRAD jobfile.
    cases : list
        list of casedictionaries for the run.
        Used to define inputs
    dir_output : str, optional
        output directory. The default is 'Output'.
    outputs : list, optional
        list of output tuples. The default is [].
    setenv : dict, optional
        CAMRAD modifications. The default is {}.
    samples : list, optional
        list of samples (tuples). The default is [].
    replacenan : bool, optional
        replace 1e9 values with NaN. The default is True.

    Returns
    -------
    None.

    """
    
    
    ################### Preprocessing #########################################
    #Inputs
    inputs=def_input(cases)
        
    # ################### Set active/output directory #########################
    
    # # Set active/output directory
    prevcwd = os.getcwd()
    os.chdir(os.path.abspath(dir_output))
    # ############################ NDARC ######################################    
    cjob = CamradWrapper(jobfile, inputs, outputs, jobname=jobname,
            setenv=setenv, saveallruns=True, renameoutputs=True,
            parallel=False, record=True,)
        
    prob = Problem()
    model = prob.model=Group()
    
    #Independent Variable
    ivc = IndepVarComp()
        
    for i in inputs: 
        if 'discrete' in i[2] and i[2]['discrete']:
            ivc.add_discrete_output(i[0],val=i[2]['val'])
        else: 
            ivc.add_output(i[0],val=i[2]['val'])
    
    # if samples are explicitly defined the function input is used
    if not samples: samples=sample_full_ivc(cases)
    
    # add components
    model.add_subsystem('ivc', ivc)
    model.add_subsystem('camrad', cjob)
    
    # add connections
    for i in inputs:  model.connect('ivc.'+i[0], 'camrad.'+i[0])
    
    for i in inputs: model.add_design_var('ivc.'+i[0]) #boundaries are not necessary when using generated lists ,lower=lo_up[i[0]][0], upper=lo_up[i[0]][1])
    
    #Drivers
    prob.driver = DOEDriver(ListGenerator(samples))
    prob.driver.record_doe = True
    prob.driver.options['run_parallel'] =  True
    prob.driver.options['procs_per_model'] =  1
    prob.driver.add_recorder(SqliteRecorder('test.sql'))
      
    prob.setup(derivatives=False)
    prob.run_driver()
    
    prob.cleanup()    # this closes all recorders
    
    df = pd.read_csv(jobname+'.csv')
        
    if replacenan: df = df.replace(1e9, np.nan)
    
#    df.loc[df['Converged']=='1','Converged']=True
#    df.loc[df['Converged']=='0','Converged']=False
#    df.loc[df['Converged']=='None','Converged']=False
#    
#    df.loc[df['Converged']==1,'Converged']=True
#    df.loc[df['Converged']==0,'Converged']=False
    
    
    # Restore cwd
    os.chdir(prevcwd)
    
    return df


def format_df_calibration(df_res, dict_cases, i_case=1, i_r=1, def_i_plot=True):
    """
    Formats the results dataframe from CAMRAD the MDAO CAMRAD Wrapper
    so that the calibration can use it.
    Also extracts the desired case and rotor ID

    Parameters
    ----------
    df_res : TYPE
        DESCRIPTION.
    dict_cases : TYPE
        DESCRIPTION.
    i_case : TYPE, optional
        DESCRIPTION. The default is 1.
    i_r : TYPE, optional
        DESCRIPTION. The default is 1.
    def_i_plot : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.

    """
    
    df=copy.deepc(df_res)
    
    str_case='c'+str(i_case)+'_'
    str_r='r'+str(i_r)+'_'
    columns=df.columns
    if any([bool(str_case in col) for col in df.columns]):
        columns=[col for col in columns if str_case in col or not ((col[0]=='c') & (col[2]=='_'))]
    if any([bool(str_r in col) for col in df.columns]):
        columns=[col for col in columns if str_r in col or not ((col[0]=='r') & (col[2]=='_'))]
    
    df=df[columns]
    df=df.rename(columns={col:col.replace(str_r,'').replace(str_case,'') for col in columns})
    
    df['i_plot']=[0]*len(df)
    df['case']=['none']*len(df)
    df['induced']=['none']*len(df)
    df['profile']=['none']*len(df)
    df['weighting']=[1.]*len(df)
    
    df['']
    
    df['kappa']=df['kappa_ind']*df['kappa_int']
    df['CPs_ind_cam']=copy.copy(df['CPs_ind'])
    df['P_ind_cam']=copy.copy(df['P_ind'])
    df['CPs_ind']=df['CPs_ind_cam']+df['CPs_int']
    df['P_ind']=df['P_ind_cam']+df['P_int']
    
    for case, casedict in dict_cases.items():
        samples=sample_full_ivc([casedict])
        for sample in samples:
            df_tmp=copy.deepcopy(df)
            for in_tuple in sample:
                df_tmp=df_tmp.loc[df[in_tuple[0].replace('ivc.','')]==in_tuple[1]]
            if len(df_tmp)>1:
                print('Warning More than one row matches sample input')
            if len(df_tmp)>0: df.loc[(df['RunID']==list(df_tmp.RunID)[0]), ['case', 'induced', 'profile']] = [case, case, case]
            
    if def_i_plot: df=def_i_plot(df)
    
    df_cal=copy.deepcopy(df)
    
    columns=['weighting','i_plot','induced','profile','mu','muz','Mtip','CTs','CPs','kappa','cdmean''CPs_ind','CPs_pro']
    df_cal=df[columns]
    
    return df, df_cal
            
         
def def_i_plot(df):
    """
    Rudimentary definition of i_plot based on standard case definition

    Parameters
    ----------
    df : TYPE
        DESCRIPTION.

    Returns
    -------
    df : TYPE
        DESCRIPTION.

    """
    
    df.loc[df.case=='hover', 'i_plot']=1
    
    for i,tow in enumerate(set(df.loc[df.case=='edge']['tow'])):
        df.loc[(df.case=='edge')&(df.tow==tow),'i_plot']=11+i
        
    for i,tow in enumerate(set(df.loc[df.case=='compress']['Mtip'])):
        df.loc[(df.case=='compress')&(df.tow==tow),'i_plot']=101+i    
        
    return df


outputs_cam_default=[
        ('speed','CASE nnnn%SUMMARY%OPERATING CONDITION%FLIGHT SPEED%VELOCITY (M/SEC)'),
        ('mass','CASE nnnn%SUMMARY%CONFIGURATION%AIRFRAME%GROSS WEIGHT (KG)'),
        ('rpm','CASE nnnn%SUMMARY%OPERATING CONDITION%ROTOR SPEED%ROTATIONAL SPEED (RPM)'),
        ('Vtip','CASE nnnn%SUMMARY%OPERATING CONDITION%ROTOR SPEED%TIP SPEED (M/SEC)'),
        ('T','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR FORCES AND MOMENTS%SHAFT AXES%THRUST%T'),
        ('CTs','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR FORCES AND MOMENTS%SHAFT AXES%THRUST%CT/S'),
        ('mux','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%OPERATING CONDITION%HUB PLANE%MUX'),
        ('muy','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%OPERATING CONDITION%HUB PLANE%MUY'),
        ('muz','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%OPERATING CONDITION%HUB PLANE%MUZ'),
        ('Mtip','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%OPERATING CONDITION%HOVER TIP MACH NUMBER'),
        ('coll','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%OPERATING CONDITION%COLLECTIVE CONTROL'),
        ('kappa_ind','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%PERFORMANCE%INDUCED%KAPPA=P/PM'),
        ('kappa_int','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%PERFORMANCE%INTERFERENCE%KAPPA=P/PM'),
        ('cdmean','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%PERFORMANCE%CDO=(8*CPO/S)/F(MU)'),
        ('CPs','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%TOTAL%CP/S'),
        ('P','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%TOTAL%P(WATT)'),
        ('CPs_pro','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%PROFILE%CPO/S'),
        ('P_pro','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%PROFILE%PO(WATT)'),
        ('CPs_ind','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%INDUCED%CPIND/S'),
        ('P_ind','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%INDUCED%PIND(WATT)'),
        ('CPs_int','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%INTERFERENCE%CPINT/S'),
        ('P_int','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%INTERFERENCE%PINT(WATT)'),
        ('CPs_par_climb','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%CLIMB + PARASITE%CPC/S+CPP/S'),
        ('p_par_climb','CASE nnnn%TRIM SOLUTION:ROTOR rrrr PERFORMANCE%ROTOR POWER%CLIMB + PARASITE%PC+PP(WATT)'),
]

def output_camrad(outputs_raw=[], lst_cases=[1], lst_r=[1]):
    """
    

    Parameters
    ----------
    outputs_raw : list, optional
        different outputs than standard.
        Raw outputs muss replace rotor number with rrrr and case number with nnnn
        if these are not to be constant.
        The default is [].
    lst_cases : list, optional
        list of cases from which to get outputs. The default is [1].
    lst_r : list, optional
        list of rotors from which to get output. The default is [1].

    Returns
    -------
    outputs_final : TYPE
        DESCRIPTION.

    """
    if not outputs_raw:
        outputs_raw=outputs_cam_default
    
    outputs_final=[]
    for tup_out in outputs_raw:
        if 'nnnn' not in tup_out[1] and 'rrrr' not in tup_out[1]:
            outputs_final.append(tup_out)
    
    outputs_tmp=[]
    for tup_out in outputs_raw:
        if 'rrrr' not in tup_out[1]:
            outputs_tmp.append((tup_out[0], tup_out[1]))
        else:
            for i_r in lst_r:
                str_r=''
                if '' in tup_out[0] and len(lst_r)>1:
                    str_r='r'+str(i_r)+'_'
                if 'nnnn' in tup_out[1]:
                    outputs_tmp.append((str_r+tup_out[0], tup_out[1].replace('rrrr', str(i_r))))
                else:
                    outputs_final.append((str_r+tup_out[0], tup_out[1].replace('rrrr', str(i_r))))
             
    for tup_out in outputs_tmp:
        str_case=''
        for i_case in lst_cases:
            if len(lst_cases)>1:
                str_case='c'+str(i_case)+'_'
            outputs_final.append((str_case+tup_out[0], tup_out[1].replace('nnnn', str(i_case))))
            
    return outputs_final

    
def parse_cam_out(path_out, lst_r=[1]):
    # Parse the CAMRAD II solution solution file, 'sample.out'
    soln = cc.CamradSolution(path_out)
    ncases =len([key for key in soln.keys() if 'CASE' in key])
    lst_cases=list(np.linspace(1,ncases,ncases).astype(int))
    outputs = output_camrad(lst_cases=lst_cases, lst_r=lst_r)
    lst_keys_nocases=[tup[0] for tup in outputs if 'nnnn' not in tup[1]]
    lst_keys_cases=[tup[0] for tup in outputs if 'nnnn' in tup[1]]
    
    # for i, key in enumerate(lst_keys_nocases):
    #     lst_keys_multiple=[key2 for key2 in lst_keys_nocases if key in key2]
    #     if len(lst_keys_multiple)>1:
    #         print(lst_keys_multiple)
    #         lst_keys_nocases[i]=sorted(lst_keys_multiple)
    
    # for i, key in enumerate(lst_keys_cases):
    #     lst_keys_multiple=[key2 for key2 in lst_keys_nocases if key in key2]
    #     if len(lst_keys_multiple)>1:
    #         lst_keys_nocases[i]=sorted(lst_keys_multiple)
            
    df=pd.DataFrame()
    
    for key in lst_keys_nocases:
        accesstext =[tup[1] for tup in outputs if tup[0]==key][0]
        df[key]=[soln.getByAccesstext(accesstext)]*ncases
        
    for key in lst_keys_cases:
        tmp=[]
        str_case=""
        for i_case in lst_cases:
            if len(lst_cases)>1: str_case='c'+str(i_case)+'_'
            accesstext =[tup[1] for tup in outputs if tup[0]==str_case+key][0]
            tmp.append(soln.getByAccesstext(accesstext))
        df[key]=tmp
        
    return df
   
if __name__ == "__main__":
    directory="directory/with/camrad/.out/files"
    lst_r=[1,2]
    df=pd.DataFrame()
    for subdir, dirs, files in os.walk(directory):
        for file in files:
            if "00ms" in file and ".out" in file:
                path_out=os.path.join(subdir, file)
                df_tmp=parse_cam_out(path_out, lst_r)
                df=pd.concat([df, df_tmp], ignore_index=True)
    path_csv=os.path.join(directory, "results_hover")      
    df.to_csv(path_csv, index=False)