#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 11:01:37 2022

@author: vzappek
"""
import itertools


def def_input(cases):
    """
    From list of cases defines list of tuples for mdao input

    Parameters
    ----------


    Returns    
    -------
    
    
    Example    
    -------
     
    """    

    if not isinstance(cases, list):cases=[cases]
        
    inputs = []    
    
    for case in cases:
        for key, val in case.items():
            if 'accesstext' in val: inputs.append((key,val['accesstext'], val['metadict']))
    
    return inputs      
     

def sample_full_ivc(cases, parallel_cases=True):
    """
    Creates samples by combining every element of each input with every element of the other inputs
    The sampling of multiple cases is done in parallel 
    If one case results in a larger number of samples than the other cases the parameters of the other cases will be set to default value
    This reduces the number of cases 
    Requires parameters of cases to be created with syntax of init_par 

    Parameters
    ----------
    cases : list of OrderedDicts for each case

    Returns    
    -------
    samples
    
    Example    
    -------
    Cases A and B with parameters A1, A2 and B1 and B2 each representing a tuple of value and name with default values
    a11 = (mtowp1, 4000)
    A1 = [a11,a12,a13]
    A2 = [a21,a22]
    
    B1 = [b11,b12,b13,b14]
    B2 = [b21]
    
    samples = [[a11,a21,b11,b21],
               [a12,a21,b12,b21],
               [a13,a21,b13,b21],
               [a11,a22,b14,b21],
               [a12,a22,b1def,b2def],
               [a13,a22,b1def,b2def]
     
    """  
    
    if isinstance(cases, dict):cases=[cases]
    samples=[]
    caselistsamples=[]
    defaults = ivc_defaults(cases)
    
    #make list of samples for each case
    for case in cases:
        res = []
        for key, val in case.items():
            tmp = []
            for v in val['values']:
                tmp.append(('ivc.'+key,v))
            res.append(tmp)
        caselistsamples.append(list(list(j) for j in itertools.product(*res)))
    
    if parallel_cases:
        #max length of samples of cases
        maxlen = len(max(caselistsamples, key=len))
        #append cases that are shorter than max length with default values
        for i, case in enumerate(caselistsamples):
            if len(case) < maxlen:
                line = list(defaults[i].items())
                caselistsamples[i] += [line for i in range(maxlen - len(caselistsamples[i]))] 
        #make  one list of samples with length maxlen           
        for i in range(maxlen):
            tmp = []
            for j, case in enumerate(caselistsamples):
                tmp += caselistsamples[j][i]
            samples.append(tmp)
    else:
        for caselist in caselistsamples:
            samples+=caselist
        
    
    return samples
            
def ivc_defaults(cases):
    
    defaults = []
    for i, case in enumerate(cases):
        tmp = {}
        for key, val in case.items():
            tmp['ivc.'+key]=val['metadict']['val']
        defaults.append(tmp)
            
    return defaults

    