#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import copy
from rcotools.ndarc.ndarc_parse_perf import parse_perf_file

from functools import reduce  # forward compatibility for Python 3
import operator

def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)
def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value


def parse_perf(file_perf, lst_vars=[], i_rot=1, lst_cond=[1], str_r='', dict_vars={}):
    """
    Function that gets defined variables from perf file.
    Can get results for a specific rotor and multiple flight conditions

    Parameters
    ----------
    file_perf : string
        Filepath of .perf file.
    lst_vars : list, optional
        list of variables to get. The default is []. This will get all 
        predefined variables
    i_rot : int, optional
        Identifier of rotor. The default is 1.
    lst_cond : list, optional
        list of ints identifying flight conditions. The default is [1].
    str_r : string, optional
        Prefix for rotor column names in dataframe. The default is ''.
    dict_vars : dictionary, optional
        Replaces predefined dictionary. The default is {}.

    Returns
    -------
    df : dataframe
        result data from performance file.

    """
    if not dict_vars:
        dict_vars=dict(
            CTs=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'controls, collective', 'value, CT/s'],
            Mtip=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'tip speed', 'Mtip'],
            mux=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'speed, V/Vtip', 'mu x'],
            muy=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'speed, V/Vtip', 'mu y'],
            muz=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'speed, V/Vtip', 'mu z'],
            offset=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'lift offset, Mx/TR'],
            kappa=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'power, CP', 'Ki=Pind/Pideal'],
            cdmean=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'power, CP', 'cd mean'],
            CPs=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'power, CP', 'CP/s'],
            CPs_ind=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'power, CP', 'CP/s induced'],
            CPs_pro=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'power, CP', 'CP/s profile'],
            lambda_h=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'inflow, lambda_h', 'value'],
            lideal=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'inflow, lambda_h', 'lambda_ideal'],
            lambda_in=['FLIGHT STATE, SWEEP', 'tmp_str_rotor', 'inflow, lambda_h', 'lambda'],
            not_conv=['FLIGHT STATE, SWEEP', 'Not conv _maxEffort,Trim,Rotor_']
            )
        
    if dict_vars and not lst_vars:
        lst_vars=list(dict_vars.keys())
    
    datadict = parse_perf_file(file_perf)
    df=pd.DataFrame()
    
    str_rotor=[key for key in datadict['FLIGHT STATE, SWEEP'] if 'Rotor '+str(i_rot) in key][0]
    
    for key, lst_keys in dict_vars.items():
        if key in lst_vars or not lst_vars:
            if lst_vars: lst_vars.remove(key)
            lst_keys_tmp=[key.replace('tmp_str_rotor', str_rotor) for key in lst_keys]
            tmp=getFromDict(datadict, lst_keys_tmp)
            lst_tmp=[]
            for i in lst_cond: lst_tmp+=list(tmp['Condition '+str(i)])
            if 'tmp_str_rotor' in dict_vars[key]:
                df[str_r+key]=lst_tmp
            else: 
                df[key]=lst_tmp                
    
    df=df.replace('NaN', None)
    
    return df