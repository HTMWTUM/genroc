#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from copy import deepcopy
from statistics import mean

from openmdao.api import ExplicitComponent

 
class RotorTarget(ExplicitComponent):
    """
    A function that returns a target value to OpenMDAO (in the self.compute function).
    Takes in a set of reference flight case sweeps and a set of NDARC rotor parameters.
    Calculates the same flight cases with the Python NDARC Rotor model.
    Returns the Root Mean Square difference as target value
    
    Columns of df have to be: weighting (ootional), CTs, mu, muz, (Mtip (optional)), (offset (optional), kappa, cdmean, CPs, CPs_ind, CPs_pro 
    
                                                                                      
    Parameters
    ----------
    rot : NDARCrotor

    optpars : dictionary
        Dictionary with OpenMDAO definition for all parameters.
    par_target : string
        Target parameter (e.g. 'kappa').
    df_ref : pandas.DataFrame
        Subset of NDARC_Rotor_Calibration.df_ref with flight cases 
        for current calibration step
    **kwargs : dictionary
        optional keyword arguments.
    """

    def __init__(self, rot, optpars, par_target, df_ref, **kwargs):
        super(RotorTarget, self).__init__()
        
        df=deepcopy(df_ref)
        df=df.rename(columns={c:'ref_'+c for c in df_ref.columns if not any(par == c for par in ['CTs', 'mu', 'muz', 'Mtip', 'offset', 'weighting'])})
        
        self.rot=rot
        self.optpars=optpars
        self.par_target=par_target
        self.df=df  
        # If desired error metric can be changed through kwargs 
        # New method has to be implemented beforehand
        self.error_alg='NMRSE'
        if 'error_alg' in kwargs: self.error_alg=kwargs['error_alg']
                
        
    def setup(self):
        for par, pardict in self.optpars.items():
            self.add_input(par, val = pardict['val'])
        self.add_output('target', val=0.0)
        

    def compute(self, inputs, outputs):
        """
        Sets outputs['target'] to the error value calculated with all 
        flight cases in this calibration step
        
        It overrides
        :func:`openmdao.core.explicitcomponent.ExplicitComponent.compute`.

        :Params:

            inputs (openmdao Vector):
                unscaled, dimensional input variables read via inputs[key]

            outputs (openmdao Vector):
                unscaled, dimensional output variables read via outputs[key]
        """
        
        dict_updates = {}
        for par, pardict in self.optpars.items():
            dict_updates[par]=float(inputs[par])
            
        outputs['target'] = self.calc_target(dict_updates)
        
    def calc_target(self, dict_updates):
        """
        Calculates the target(error) value from all flight cases in df_ref

        Parameters
        ----------
        dict_updates : dictionary
            Dictionary that includes new values of parameters that are being 
            optimized in this iteration

        Returns
        -------
        target : float
            error value.

        """
        
        if not 'offset' in self.df:
            self.df['offset']=[0]*len(self.df.CTs)
            
        if not 'CTs2' in self.df:
            self.df['CTs2']=[0]*len(self.df.CTs)
        
        if not 'Mtip' in self.df:
            if self.rot.Mtip:
                self.df['Mtip']=[self.rot.Mtip]*len(self.df.CTs)
            else:
                raise ValueError('Mtip not available')
                    
                
        for var in dict_updates:
            setattr(self.rot, var, dict_updates[var])
        
        
        if self.par_target == 'kappa':
            self.df['ndarc_kappa']=[self.rot.calc_induced(CTs, mu, muz, offset) for (CTs, mu, muz, offset) in zip(self.df.CTs, self.df.mu, self.df.muz, self.df.offset)]
                    
        if self.par_target == 'cdmean':
            self.df['ndarc_cdmean']=[self.rot.calc_profile(CTs, mu, muz, offset, Mtip) for (CTs, mu, muz, offset, Mtip) in zip(self.df.CTs, self.df.mu, self.df.muz, self.df.offset, self.df.Mtip)]
            
        if self.par_target == 'CPs_ind':
            self.df['ndarc_CPs_ind']=[self.rot.calc_CPs_ind(CTs, mu, muz, offset, CTs2=CTs2) for (CTs, mu, muz, offset, CTs2) in zip(self.df.CTs, self.df.mu, self.df.muz, self.df.offset, self.df.CTs2)]
            
        if self.par_target == 'CPs_pro':
            self.df['ndarc_CPs_pro']=[self.rot.calc_CPs_pro(CTs, mu, muz, offset, Mtip) for (CTs, mu, muz, offset, Mtip) in zip(self.df.CTs, self.df.mu, self.df.muz, self.df.offset, self.df.Mtip)]
            
        if self.par_target == 'CPs':
            self.df['ndarc_CPs']=[self.rot.calc_CPs(CTs, mu, muz, offset, Mtip, CTs2=CTs2)[0] for (CTs, mu, muz, offset, Mtip, CTs2) in zip(self.df.CTs, self.df.mu, self.df.muz, self.df.offset, self.df.Mtip, self.df.CTs2)]
        
        if not 'weighting' in self.df.columns: self.df['weighting']=[1.]*len(self.df)
        
        if self.error_alg=='NRMSE': 
            target=self.NRMSE()
        elif self.error_alg=='NRMSE': 
            target=self.NMSE()
        elif self.error_alg=='NE': 
            target=self.NE()
        else: 
            target=self.NRMSE()
            
        target=self.NE()
                
        return target
    
    def NMSE(self):
        """
        Normalized Mean Square Error

        Returns
        -------
        target : float
            error value.

        """
        delta_square = np.square(self.df['ndarc_'+self.par_target] - self.df['ref_'+self.par_target])*self.df.weighting/(sum(self.df.weighting)/len(self.df.weighting))
                            
        target =  (delta_square.sum()/len(delta_square))/mean(self.df['ref_'+self.par_target])  
        
        return target
    
    def NRMSE(self):
        """
        Normalized Root Mean Square Error

        Returns
        -------
        target : float
            error value.

        """
        delta_square = np.square(self.df['ndarc_'+self.par_target] - self.df['ref_'+self.par_target])*self.df.weighting/(sum(self.df.weighting)/len(self.df.weighting))
                            
        target =  np.sqrt(delta_square.sum()/len(delta_square))/mean(self.df['ref_'+self.par_target])  
        
        return target
    
    def NE(self):
        """
        Normalized Absolute Error

        Returns
        -------
        target : float
            error value.

        """
        target = sum(abs(self.df['ndarc_'+self.par_target] - self.df['ref_'+self.par_target])*self.df.weighting/(sum(self.df.weighting)/len(self.df.weighting)))/sum(self.df['ref_'+self.par_target])
        # print(target)
        return target
