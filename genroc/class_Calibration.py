#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
from statistics import mean

from genroc.class_Target import RotorTarget
from genroc.class_NDARCrotor import dict_pars_default

import matplotlib.pyplot as plt

from openmdao.api import IndepVarComp, Problem
from openmdao.api import SimpleGADriver, DifferentialEvolutionDriver

try:
    from mpi4py import MPI
    mpiloaded=True
except:
    print("mpi not loaded")
    mpiloaded=False
    
def print_mpi_0(*str_msgs):
    """
    Function that only prints statements on one process.
    replaces '_np_' with number of processors

    Parameters
    ----------
    *str_msgs : str
        string to print.

    Returns
    -------
    None.

    """
    if mpiloaded:
        comm = MPI.COMM_WORLD
        if comm.rank==0: print(*(str(str_msg).replace('_np_',str(comm.size)) for str_msg in str_msgs))
    else:
        print(*str_msgs)

import collections.abc

def update_dict_nested(d, u):
    """
    Helper function that updates all vales in a nested dictionary d
    from an update nested dictionary u
    used to update the mdao parameters from input if necessary

    """
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_dict_nested(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class NDARC_Rotor_Calibration():
    """
    Calibration Class object.
    Encapsulates functions used for executing the calibration
    Can also just be used for pre- or postprocessing of calibration
    
    Attributes
    ----------
    df_ref : pandas.DataFrame
        reference dataframe from .csv file
        
    rot : NDARCrotor
        Instance of NDARCrotor class 
            
    dict_cal_order: dict
        A dictionary that defines the assignment of parameters to calibrate
        to the calibration steps. For induced and profile model calibration 
        steps are defined. For each step a list of rotor parameters is given.
        These parameters will be calibrated by the optimization.
        The calibration steps have to correspond to identifiers in the 
        reference .csv file in the columns 'induced' and 'profile'.
        
        Example:
        dict_cal_order = {'induced': {'hover': ['Ki_hover', 'CTs_Hind'],
                                      'edge': ['Ki_edge', 'mu_edge']},
                          'profile': {'all': ['CTs_Dmin', 'd0_hel']}}

    dict_pars_MDAO : dict
        Dictionary that includes the default value and the constraints for
        every possible optimization parameter.
        
    Notes
    ----------
     
    """
    
    
    def __init__(self, df_ref, rot, **kwargs):        
        self.df_ref=df_ref
        if 'weighting' in self.df_ref:
            self.df_ref=self.df_ref[self.df_ref['weighting']>0.01]
            
        self.rot=rot
        
        # dictionary that defines order of calibration steps
        if 'dict_cal_order' in kwargs:
            self.dict_cal_order=kwargs['dict_cal_order']
        # else:
        #     self.dict_cal_order=self.dict_cal_order_default()
            
        # dictionary with defaults and constraints
        self.dict_pars_MDAO=self.dict_pars_MDAO_default()
        if 'dict_pars_MDAO' in kwargs: 
            # self.dict_pars_MDAO.update(kwargs['dict_pars_MDAO'])
            update_dict_nested(self.dict_pars_MDAO, kwargs['dict_pars_MDAO'])
            
        self.axial_from_edge=False
        if 'axial_from_edge' in kwargs: self.axial_from_edge=kwargs['axial_from_edge']
            
        
        
        # set defaults of calibration variables to values in rotor instance
        for ind_pro in self.dict_pars_MDAO:
            for par in self.dict_pars_MDAO[ind_pro]:
                self.dict_pars_MDAO[ind_pro][par]['val']=getattr(self.rot, par)
        
    
    def calibrate(self, ind_pro=None, max_gen=500, bits=9, pop_size=0, driver='DE'):
        """
        Runs the calibration process. 
        

        Parameters
        ----------
        ind_pro : str, optional
            Use if only one of the models is to be calibrated.
            set to 'induced' or 'profile'. If None both models are calibrated
        max_gen : int, optional
            Maximum generations. The default is 500.
        bits : int, optional
            "resolution" of variables. The default is 9.
        pop_size : int, optional
            Population size. The default is 0.
            
            For documentation on max_gen, bits and pop_size see manual for
            SimpleGAdriver from OpenMDAO

        Returns
        -------
        None.

        """
        # If desired only one of the models can be calibrated. Default is that 
        # both will be calibrated
        if not ind_pro: 
            lst_ind_pro=['induced', 'profile']
        else: 
            lst_ind_pro=[ind_pro]
      
        if not 'offset' in self.df_ref:
            self.df_ref['offset']=[0]*len(self.df_ref.CTs)
            
        dict_pars_MDAO=copy.deepcopy(self.dict_pars_MDAO)
            
        for model in lst_ind_pro:  
            # for induced model always optimize with kappa. If not in 
            # reference it is calculated from CPs_ind and ideal inflow
            if model=='induced':
                par_target='kappa'
                if not 'kappa' in self.df_ref.columns: 
                    if not 'CPs_ind' in self.df_ref.columns: raise ValueError('Neither kappa nor CPs_ind in columns')
                    self.df_ref['kappa']=[self.rot.kappa_from_CPs_ind(CPs_ind, CTs, mu, muz, offset) for (CPs_ind, CTs, mu, muz, offset) in zip(self.df_ref.CPs_ind, self.df_ref.CTs, self.df_ref.mu, self.df_ref.muz, self.df_ref.offset)]
            
            # for profile model always optimize with cdmean. If not in 
            # reference it is calculated CPs_pro
            elif model=='profile':
                par_target='cdmean'
                if not 'Mtip' in self.df_ref:
                    self.df_ref['Mtip']=[self.rot.Mtip]*len(self.df_ref.CTs) 
                    
                if not 'Re_75' in self.df_ref:
                    self.df_ref['Re_75']=[0.]*len(self.df_ref.CTs) 
                    
                if not 'cdmean' in self.df_ref.columns: 
                   if not 'CPs_pro' in self.df_ref.columns: raise ValueError('Neither cdmean nor CPs_pro in columns')
                   self.df_ref['cdmean']=[self.rot.cdmean_from_CPs_pro(CPs_pro, mu, muz) for (CPs_pro, mu, muz) in zip(self.df_ref.CPs_pro, self.df_ref.mu, self.df_ref.muz)]
            
            # Calibration using just CPs=CPs_ind+CPs_pro is also possible 
            # but not recommended
            elif model=='combined':
                par_target='CPs'
                if not 'Mtip' in self.df_ref:
                    self.df_ref['Mtip']=[self.rot.Mtip]*len(self.df_ref.CTs) 
                
                dict_pars_MDAO={}
                dict_pars_MDAO['combined']={**self.dict_pars_MDAO['induced'], **self.dict_pars_MDAO['profile']}
                    
                if not 'CPs' in self.df_ref.columns: raise ValueError('CPs not in columns')            
            
            # Loop through calibration steps
            for key in self.dict_cal_order[model]:
                key_orig=copy.copy(key)
                if "_2" in key: key=key.replace("_2","")
                
                opt_pars={k:dict_pars_MDAO[model][k] for k in self.dict_cal_order[model][key_orig]}
                # If this is a repeat run marked by '_2', set default values to reults of preovious run
                if '_2' in key_orig:
                    for par in opt_pars:
                        opt_pars[par]['val']=getattr(self.rot, par)
                
                
                if key=='all':
                    df=copy.deepcopy(self.df_ref[(self.df_ref[model]!='none') & (self.df_ref[model].notnull())])
                else:
                    df=copy.deepcopy(self.df_ref[self.df_ref[model].str.contains(key)])
                if not df.empty:
                    print_mpi_0('\nCalibration Step starting:', model, key_orig)
                    print_mpi_0('Target Parameter:', par_target)
                    print_mpi_0('Running on '+'_np_'+' processes\n')

                    self.mdao_setup(optpars=opt_pars, par_target=par_target, df_ref=df, max_gen=max_gen, bits=bits, pop_size=pop_size, key=key, driver=driver)
                                         
            self.df_comp=self.calc_df_comp()
            
              
    def mdao_setup(self, optpars, par_target, df_ref, max_gen=100, bits=8, pop_size=0, key='', driver='DE'):
        """
        Sets up the OpenMDAO problem and executes optimization

        Parameters
        ----------
        optpars : list
            list of variable to optimize.
        par_target : string
            target parameter.
        df_ref : dataframe
            reference data. is different from self.df_ref
        max_gen : int, optional
            max. generations. The default is 100.
        bits : int, optional
            precision. The default is 8.
        pop_size : int, optional
            population size. The default is 0.

        Returns
        -------
        df_opt : dataframe
            dataframe with target (error) value.

        """
        target = RotorTarget(self.rot, optpars=optpars, par_target=par_target, df_ref=df_ref, error_alg='NRMSE')
        
        # Set up MDAO Problem
        prob = Problem()
        # Add design variables
        indeps = prob.model.add_subsystem('indeps', IndepVarComp(), promotes=['*'])
        
        for par, pardict in optpars.items():
            indeps.add_output(par, val = pardict['val'])
        
        # Add components
        prob.model.add_subsystem('ndarc_target', target, promotes=['*'])    # promotes_inputs=['ki_hov'], promotes_outputs=['target'])
        
        if driver=='GA':
            # See SimpleGADriver documentation for all options
            prob.driver = SimpleGADriver()
            bit_dict={par:optpars[par]['bits'] if 'bits' in optpars[par] else bits for par in optpars}
            prob.driver.options['bits'] = bit_dict   
            pop_size_new = pop_size*4*sum([bit_dict[par] for par in optpars])
        
        if driver=='DE':
            # DifferentialEvolutionDriver is supposedly 3 times faster then SimpleGAdriver
            prob.driver = DifferentialEvolutionDriver()
            pop_size_new = pop_size*20*len(optpars)
            # prob.driver.options['F'] = 0.9     #0.9
            # prob.driver.options['Pc'] = 0.9     #0.9
            
        prob.driver.options['max_gen'] = max_gen
        prob.driver.options['pop_size'] = pop_size_new  
        prob.driver.options['run_parallel'] = True
        
        # For debugging
        # prob.driver.options['debug_print'] = ['desvars','objs']
       
        
        # Set up problem
        for par, pardict in optpars.items():
            prob.model.add_design_var(par, **pardict['constraints'])
        
        prob.model.add_objective('target')
        
        # If desired set up OpenMDAO recorder
        # recorder = SqliteRecorder('/scratch/'+'calibrate' + '.sql')
        # prob.driver.add_recorder(recorder)
        
        prob.setup()#mode='rev')
        
        # Run the system
        prob.run_driver()
        
        case=''
        if par_target in ('kappa','CPs_ind'):
            case='induced'
            
        if par_target in ('cdmean','CPs_pro'):
            case='profile'
            
            
        print_mpi_0(case, key,'Results:')
        for par, pardict in optpars.items():
            print_mpi_0(par + ' = {}'.format(round(*prob[par],5)))#/pardict['constraints']['ref']
            
        print_mpi_0('Normalized Mean Target Error [%] =',  round(*prob.get_val('target')*100,2), '\n')
        
        
        for par, pardict in optpars.items():
            setattr(self.rot, par, float(*prob[par]))
                
        prob.cleanup()
 
        
        
    def calc_df_comp(self, col_targets=[], df_ref_alt=None):
        """
        Returns dataframe with calculated values for reference and NDARC
        
        Available parameters to calculate are:
        kappa, cdmean, CPs_ind, CPs_pro, CPs, lideal, lambda_h
        
        Parameters
        ----------
        col_targets : list, optional
            list of strings that defines which variables to calculate

        Returns
        -------
        df_comp : pandas.DataFrame
            dataframe with results and reference values.

        """
        if df_ref_alt: 
            df_comp=copy.deepcopy(df_ref_alt)
        else:
            df_comp=copy.deepcopy(self.df_ref)
        
        if 'induced' in df_comp.columns and 'profile' in df_comp.columns:
            df_comp=df_comp[((df_comp['induced'] != 'none') & ((df_comp['induced'].notnull()))) | ((df_comp['profile'] != 'none') & ((df_comp['induced'].notnull()))) ]
        
        if self.rot.twin and not 'CTs2' in df_comp.columns: 
            df_comp=self.df_twin_to_single()
        
        if not 'offset' in df_comp:
            df_comp['offset']=[0]*len(df_comp.CTs)
        
        if not 'Mtip' in df_comp:
            df_comp['Mtip']=[self.rot.Mtip]*len(df_comp.CTs)  
            
        if not 'Re_75' in df_comp:
            df_comp['Re_75']=[0.]*len(df_comp.CTs) 
            
        if not 'CTs2' in df_comp:
            df_comp['CTs2']=[None]*len(df_comp.CTs)
               
        if not col_targets:    
            col_targets=[]
            if 'kappa' in df_comp: 
                col_targets.append('kappa')
            elif 'CPs_ind' in df_comp: 
                col_targets.append('CPs_ind')
            if 'cdmean' in df_comp: 
                col_targets.append('cdmean')
            elif 'CPs_pro' in df_comp: 
                col_targets.append('CPs_pro')
            if 'CPs' in df_comp: col_targets.append('CPs')
        
        if not 'CPs' in col_targets:
            if not 'kappa' in df_comp.columns: 
                if not 'CPs_ind' in df_comp: raise ValueError('Neither kappa nor CPs_ind in columns')
                df_comp['kappa']=[self.rot.kappa_from_CPs_ind(CPs_ind, CTs, mu, muz, offset, CTs2) for (CPs_ind, CTs, mu, muz, offset, CTs2) in zip(df_comp.CPs_ind, df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset, df_comp.CTs2)]
                
            if not 'cdmean' in df_comp.columns: 
                if not 'CPs_pro' in df_comp: raise ValueError('Neither cdmean nor CPs_pro in columns')
                df_comp['cdmean']=[self.rot.cdmean_from_CPs_pro(CPs_pro, mu, muz) for (CPs_pro, mu, muz) in zip(df_comp.CPs_pro, df_comp.mu, df_comp.muz)]
            
        col_targets_rename_ref={col:'ref_'+col for col in col_targets}
        cols=['CTs', 'mu', 'muz', 'offset', 'Mtip', 'Re_75', 'CTs2']+col_targets
        if 'i_plot' in df_comp: cols=['i_plot']+cols
        if 'weighting' in df_comp: cols=['weighting']+cols

        df_comp=df_comp[cols]
        df_comp=df_comp.rename(columns=col_targets_rename_ref)

        for par_target in col_targets:
            if par_target == 'kappa':
                df_comp['ndarc_kappa']=[self.rot.calc_induced(CTs, mu, muz, offset) for (CTs, mu, muz, offset) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset)]
                
            if par_target == 'cdmean':
                df_comp['ndarc_cdmean']=[self.rot.calc_profile(CTs, mu, muz, offset, Mtip, Re_75) for (CTs, mu, muz, offset, Mtip, Re_75) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset, df_comp.Mtip, df_comp.Re_75)]
                
            if par_target == 'CPs_ind':
                df_comp['ndarc_CPs_ind']=[self.rot.calc_CPs_ind(CTs, mu, muz, offset, CTs2) for (CTs, mu, muz, offset, CTs2) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset, df_comp.CTs2)]
                
            if par_target == 'CPs_pro':
                df_comp['ndarc_CPs_pro']=[self.rot.calc_CPs_pro(CTs, mu, muz, offset, Mtip, Re_75) for (CTs, mu, muz, offset, Mtip, Re_75) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset, df_comp.Mtip, df_comp.Re_75)]
                
            if par_target == 'CPs':
                df_comp['ndarc_CPs']=[self.rot.calc_CPs(CTs, mu, muz, offset, Mtip, Re_75, CTs2)[0] for (CTs, mu, muz, offset, Mtip, Re_75, CTs2) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.offset, df_comp.Mtip, df_comp.Re_75, df_comp.CTs2)]
                
            if par_target == 'lideal':
                if not self.rot.twin:
                    df_comp['ndarc_lideal']=[self.rot.ideal_inflow(CTs, mu, muz)[0] for (CTs, mu, muz) in zip(df_comp.CTs, df_comp.mu, df_comp.muz)]
                else:
                    df_comp['ndarc_lideal']=[self.rot.ideal_inflow_twin(CTs, mu, muz, CTs2)[0] for (CTs, mu, muz, CTs2) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.CTs2)]
            
            if par_target == 'lambda_h':
                if not self.rot.twin:
                    df_comp['ndarc_lambda_h']=[self.rot.ideal_inflow(CTs, mu, muz)[1] for (CTs, mu, muz) in zip(df_comp.CTs, df_comp.mu, df_comp.muz)]
                else:
                    df_comp['ndarc_lambda_h']=[self.rot.ideal_inflow_twin(CTs, mu, muz, CTs2)[1] for (CTs, mu, muz, CTs2) in zip(df_comp.CTs, df_comp.mu, df_comp.muz, df_comp.CTs2)]
                
                  
        for par_target in col_targets:
            df_comp['err_'+par_target]=(df_comp['ndarc_'+par_target]-df_comp['ref_'+par_target])/df_comp['ref_'+par_target]*100
            # df_comp['err_'+par_target]=(df_comp['ndarc_'+par_target]-df_comp['ref_'+par_target])/mean(df_comp[df_comp['ref_'+par_target].notnull()]['ref_'+par_target])*100
            
 
        if 'CTs2' in df_comp and not 'CTs2' in self.df_ref: df_comp.drop(columns=['CTs2']) 
        if 'offset' in df_comp and not 'offset' in self.df_ref: df_comp.drop(columns=['offset']) 
        if 'Re_75' in df_comp and not 'Re_75' in self.df_ref: df_comp.drop(columns=['Re_75']) 
            
        return df_comp
    
    
    def set_vals_pre_calc(self, pars_pre=[]):
        """
        Sets values for parameters that can be set before optimization.

        Parameters
        ----------
        pars_pre : list, optional
            list of strings that defines which parameters to set. The default is [].

        Returns
        -------
        None.

        """
        
        df=self.fill_df(self.df_ref)
        
        # For this to work df_ref has to include kappa and cdmean
        if 'kappa' not in df.columns and 'CPs_ind' in df.columns:
            df.loc[:,'kappa']=[self.rot.kappa_from_CPs_ind(CPs_ind, CTs, mu, muz, CTs2=CTs2) for (CPs_ind, CTs, mu, muz, CTs2) in zip(df.CPs_ind, df.CTs, df.mu, df.muz, df.CTs2)]
        
        if 'cdmean' not in df.columns and 'CPs_pro' in df.columns:
            df.loc[:,'cdmean']=[self.rot.cdmean_from_CPs_pro(CPs_pro, mu, muz) for (CPs_pro, mu, muz) in zip(df.CPs_pro, df.mu, df.muz)]
                
        # Add fitting parameters here
        dict_pars_pre=dict(induced=['Ki_min','Ki_max','Ki_hover'], profile=['d0_hel','CTs_Dmin'])
        
        print_mpi_0('Preset parameters:')
        
        for model, pars in dict_pars_pre.items():
            if model in self.dict_cal_order:
                for par in pars:
                    if par in pars_pre or not pars_pre: 
                        if any(par in val for val in self.dict_cal_order[model].values()):
                            for i, key in enumerate(self.dict_cal_order[model]):
                                if par in self.dict_cal_order[model][key]: 
                                    # Add functions for new parameters here:
                                    if par=='Ki_min':
                                        val=min(df[df['kappa'].notnull()]['kappa'])
                                    if par=='Ki_max':
                                        val=max(df[df['kappa'].notnull()]['kappa'])
                                    if par=='Ki_hover':
                                        val=min(df[(df['mu']==0.0) & (df['kappa'].notnull())]['kappa'])
                                    if par=='d0_hel':
                                        val=min(df[(df['cdmean'].notnull())&(df['mu']==0.)]['cdmean'])
                                    if par=='CTs_Dmin':
                                        Dmin=min(df[(df['cdmean'].notnull())&(df['mu']==0.)]['cdmean'])
                                        val=self.rot.CTs_Dmin=list(df[df['cdmean']==Dmin]['CTs'])[0]
                                    # This is the same for all parameters (only change if precision is not sufficient)
                                    val=round(val,5)
                                    setattr(self.rot, par, val)
                                    print_mpi_0(par+':', val)
                                    self.dict_cal_order[model][key].remove(par)
                        else:
                            print_mpi_0(par, 'nowhere in dict_cal_order')
                        
                        
    def set_axial(self):
        """
        Sets axial parameters according to calibration manual for a normal
        H/C calibration

        Returns
        -------
        None.

        """
        from_hover=[('CTs_Pind','CTs_Hind'),('kp1','kh1'),('kp2','kh2'),('Xp2','Xh2'),('d0_prop','d0_hel'),('d1_prop','d1_hel'),('d2_prop','d2_hel')]
        makezero=['mu_prop','ka1','ka2','ka3']
        
        for tup in from_hover:
            setattr(self.rot,tup[0],getattr(self.rot,tup[1]))
            
        for par in makezero:
            setattr(self.rot,par,0.)
            
    
    def df_twin_to_single(self, df=None):
        """
        Converts a reference dataframe with values for `r1_` and `r2_` to one for
        only one rotor without prefixes and CTs2 = CTs of other rotor based on i_rot.
        Only valid for two rotors.

        Returns
        -------
        df_single : dataframe
            dataframe without `r1_` and `r2_` but with CTs2.

        """
        if not df: df=self.df_ref
        
        i_rot_other=2
        if self.rot.i_rot==2: i_rot_other=1
        df_single=copy.deepcopy(df)
        columns=[col for col in df_single.columns if 'r'+str(self.rot.i_rot) in col]
        col_rename={col:col.replace('r'+str(self.rot.i_rot)+'_','') for col in columns}
        col_rename['r'+str(i_rot_other)+'_CTs']='CTs2'
        columns+=['r'+str(i_rot_other)+'_CTs']
        df_single=df_single[columns]
        df_single=df_single.rename(columns=col_rename)
        
        return df_single
        
    
    def plot_cases(self, y_var, i_list=[], ylimit=False, ploterror=True, all_cases=False, fileprefix=None):
        """
        Plot calibration results.
        Only for basic plots in hover and edgewise flight
        Plots the y-variable (kappa, cdmean, CPs_ind, CPs_pro, CPs)
        against CTs or mu depending on whether mu=0 for all cases in sweep.

        Parameters
        ----------
        y_var : string
            parameter to plot.
        i_list : list, optional
            list with i_plots. The default is [].
        ylimit : bool, optional
            True: set ylimit (1 for kappa, 0 for others). The default is False.
        ploterror : bool, optional
            True: plot normalized error in red. The default is True.

        Returns
        -------
        None.

        """
        df_ref_alt=None
        if all_cases:
            df_ref_alt=copy.deepcopy(self.df_ref)
            df_ref_alt['induced']=['plot']*len(df_ref_alt)
        df_comp=self.calc_df_comp(col_targets=[y_var], df_ref_alt=df_ref_alt)
        
        df_comp=df_comp[df_comp['ref_'+y_var].notnull()]
        
        y_err=df_comp['ndarc_'+y_var]-df_comp['ref_'+y_var]
        y_err=y_err/df_comp['ref_'+y_var]*100    
        
        df_comp['y_err']=abs(y_err)
        
        if not 'i_plot' in df_comp:
            df_comp['i_plot']=[0]*len(df_comp)
            print_mpi_0('No case distinction')
        
        if not i_list: i_list=list(sorted(set(df_comp.i_plot)))
        
        for i in i_list:
            df=df_comp[df_comp['i_plot']==i]
            x_var='mu'
            if (df['mu'] == 0).all() and (df['muz'] == 0).all(): x_var='CTs'
            if (df['mu'] == 0).all() and not (df['muz'] == 0).all(): x_var='muz'
            
            lines=[]
            fig, ax1 = plt.subplots()
            x = df[x_var]
            for prefix in ['ref_','ndarc_']:
                y = df[prefix+y_var]
                lines.append(*ax1.plot(x, y, label=prefix.replace('_','')))
                
            ax1.set_xlabel(x_var)
            ax1.set_ylabel(y_var)
            
            y_lim=0.
            if y_var=='kappa': y_lim=1.
            if ylimit: ax1.set_ylim(bottom=y_lim)
            
            if ploterror:
                y2 = df.y_err
                ax2 = ax1.twinx()  
    
                color = 'red'
                ax2.set_ylabel('Abs. Norm. Error [%]', color='red')  
                lines.append(*ax2.plot(x, y2, color=color, label='Error', linestyle='--'))
                ax2.tick_params(axis='y', labelcolor=color)
                if ylimit: ax2.set_ylim(bottom=0.)
                
                fig.tight_layout()  
                        
            labels = [l.get_label() for l in lines]
            ax1.legend(lines, labels, loc=0)
            
            if x_var=='mu' or x_var=='muz': 
                filetitle='forw_cts'+str(round(sum(df['CTs'])/len(df['CTs']),3)).replace('.','')
                if not fileprefix: plt.title('CTs ~= '+str(round(sum(df['CTs'])/len(df['CTs']),3)))
            elif x_var=='CTs': 
                if not fileprefix: plt.title('Hover')
                filetitle='hover'
            
            if fileprefix:
                filename=fileprefix+'_'+y_var+'_'+filetitle+'.pdf'
                plt.savefig(filename)
                
                
    def fill_df(self, df_in):
        
        df=copy.deepcopy(df_in)
        if self.rot.twin and not 'CTs2' in df.columns: 
            df=self.df_twin_to_single()
        
        if not 'offset' in df:
            df['offset']=[0]*len(df.CTs)
        
        if not 'Mtip' in df:
            df['Mtip']=[self.rot.Mtip]*len(df.CTs)  
            
        if not 'Re_75' in df:
            df['Re_75']=[0.]*len(df.CTs) 
            
        if not 'CTs2' in df:
            df['CTs2']=[None]*len(df.CTs)
            
        return df
        
    def dict_pars_MDAO_default(self):
        """
        Defines the dictionary that includes all NDARC rotor parameters
        for the induced and prfile power model
        For each parameter a dictionary with OpenMDAO inputs is given
        {'val': DEFAULTVALUE, 'constraints': 
         {'lower':LOWER BOUNDARY, 'upper':UPPER BOUNDRAY, 'scaler':SCALING (optional)}}
        
        Returns
        -------
        dict_pars_MDAO : dict

        """
        # Parameters to be optimized
        ### induced ###
        dict_pars_MDAO = {}
        dict_pars_MDAO['induced']={}
        dict_pars_MDAO['induced']['Ki_min'] = {'val': 1.0,'constraints': {'lower':1.0, 'upper':1.5}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['Ki_max'] = {'val': 10.0,'constraints': {'lower':3., 'upper':15.}}#, 'scaler':1.12}}
        
        dict_pars_MDAO['induced']['Ki_hover'] = {'val': 1.12,'constraints': {'lower':1.0, 'upper':1.5}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['CTs_Hind'] = {'val': 0.08, 'constraints': {'lower':0., 'upper':0.3}}#, 'scaler':0.08}}
        dict_pars_MDAO['induced']['kh1'] = {'val': 0., 'constraints': {'lower':-2., 'upper':2.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['kh2'] = {'val': .0, 'constraints': {'lower':-100., 'upper':400.}}#, 'scaler':0.01}}
        dict_pars_MDAO['induced']['Xh2'] = {'val': 2., 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['induced']['Ki_climb'] = {'val': 1.08,'constraints': {'lower':1.0, 'upper':2.}}#, 'scaler':1.12}}
        
        dict_pars_MDAO['induced']['Ki_edge'] = {'val': 2., 'constraints': {'lower':1.2, 'upper':10.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['mu_edge'] = {'val': 0.35, 'constraints': {'lower':0.15, 'upper':0.7}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ke1'] = {'val': 0.8, 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ke2'] = {'val': 0., 'constraints': {'lower':0., 'upper':100.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ke3'] = {'val': 1., 'constraints': {'lower':-100., 'upper':100.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['Xe'] = {'val': 4.5, 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':1.12}}
        
        dict_pars_MDAO['induced']['CTs_Tind'] = {'val': 0.08, 'constraints': {'lower':0.0, 'upper':0.3}}#, 'scaler':0.08}}
        dict_pars_MDAO['induced']['kt1'] = {'val': 0., 'constraints': {'lower':-100., 'upper':100.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['kt2'] = {'val': 0., 'constraints': {'lower':0., 'upper':500.}}#, 'scaler':0.01}}
        dict_pars_MDAO['induced']['Xt2'] = {'val': 3., 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['induced']['Ki_prop'] = {'val': 2., 'constraints': {'lower':1.2, 'upper':4.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['mu_prop'] = {'val': 1.0, 'constraints': {'lower':1.2, 'upper':4.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ka1'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ka2'] = {'val': 0., 'constraints': {'lower':0., 'upper':100.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['ka3'] = {'val': 0., 'constraints': {'lower':-100., 'upper':500.}}#, 'scaler':1.12}}
        dict_pars_MDAO['induced']['Xa'] = {'val': 4.5, 'constraints': {'lower':2., 'upper':8.}}#, 'scaler':1.12}}
        
        # dict_pars_MDAO['induced']['mu_axtran'] = {'val': 0., 'constraints': {'lower':0., 'upper':0.5}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['induced']['CTs_Pind'] = {'val': 0.08, 'constraints': {'lower':0.0, 'upper':0.3}}#, 'scaler':0.08}}
        dict_pars_MDAO['induced']['kp1'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['kp2'] = {'val': 0., 'constraints': {'lower':0., 'upper':10.}}#, 'scaler':0.01}}
        dict_pars_MDAO['induced']['Xp2'] = {'val': 2., 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['induced']['kea'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['induced']['kpa'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['Xpa'] = {'val': 2., 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        # dict_pars_MDAO['induced']['kpx'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        # dict_pars_MDAO['induced']['Xpx'] = {'val': 1., 'constraints': {'lower':1., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['induced']['ko1'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['ko2'] = {'val': 8., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
                
        # These are not intended to be changed. Only here for completeness
        dict_pars_MDAO['induced']['Maxial'] = {'val': 1.176, 'constraints': {'lower':0.5, 'upper':2.}}#, 'scaler':0.1}}
        dict_pars_MDAO['induced']['Xaxial'] = {'val': 0.65, 'constraints': {'lower':0.3, 'upper':2.}}#, 'scaler':0.1}}
        
        
        ### profile ###
        dict_pars_MDAO['profile']={}
        dict_pars_MDAO['profile']['CTs_Dmin'] = {'val': 0.08, 'constraints': {'lower':0., 'upper':0.15}}#, 'scaler':0.08}}
        dict_pars_MDAO['profile']['d0_hel'] = {'val': 0.009, 'constraints': {'lower':0.0, 'upper':0.05}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['d1_hel'] = {'val': 0.0, 'constraints': {'lower':0., 'upper':2.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['d2_hel'] = {'val': 0.5, 'constraints': {'lower':0., 'upper':5.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['CTs_sep'] = {'val': 0.07, 'constraints': {'lower':0.03, 'upper':0.15}}#, 'scaler':0.08}}
        dict_pars_MDAO['profile']['dsep'] = {'val': 4., 'constraints': {'lower':0., 'upper':50.}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['Xsep'] = {'val': 3.0, 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['d0_prop'] = {'val': 0.009, 'constraints': {'lower':0., 'upper':0.1}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['d1_prop'] = {'val': 0.0, 'constraints': {'lower':-2., 'upper':2.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['d2_prop'] = {'val': 0.5, 'constraints': {'lower':0., 'upper':5.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['dz1'] = {'val': 0.0, 'constraints': {'lower':0., 'upper':0.1}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['dz2'] = {'val': 0.0, 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['Xz'] = {'val': 2.0, 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['df1'] = {'val': 0.0, 'constraints': {'lower':0., 'upper':0.1}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['df2'] = {'val': 0.0, 'constraints': {'lower':0., 'upper':10.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['Xf'] = {'val': 2.0, 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['dm1'] = {'val': 0.056, 'constraints': {'lower':0., 'upper':0.1}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['dm2'] = {'val': 0.416, 'constraints': {'lower':0., 'upper':5.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['Xm'] = {'val': 2.0, 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['Mdd0'] = {'val': 0.88, 'constraints': {'lower':0.6, 'upper':0.9}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['Mddcl'] = {'val': 0.16, 'constraints': {'lower':0., 'upper':1.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['fstall'] = {'val': 1., 'constraints': {'lower':0.7, 'upper':1.3}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['dstall1'] = {'val': 2., 'constraints': {'lower':0., 'upper':20.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['dstall2'] = {'val': 40., 'constraints': {'lower':0., 'upper':500.}}#, 'scaler':0.01}}
        dict_pars_MDAO['profile']['Xstall1'] = {'val': 2., 'constraints': {'lower':1., 'upper':6.}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['Xstall2'] = {'val': 3., 'constraints': {'lower':2., 'upper':6.}}#, 'scaler':0.01}}
        
        dict_pars_MDAO['profile']['Clmax_harris'] = {'val': 1.2, 'constraints': {'lower':0.3, 'upper':2.0}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['profile']['CTs_stall_hov'] = {'val': 0.17, 'constraints': {'lower':0.07, 'upper':0.2}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['CTs_stall_04'] = {'val': 0.1, 'constraints': {'lower':0.06, 'upper':0.15}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['profile']['do1'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['do2'] = {'val': 8., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['profile']['dsa'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['profile']['Re_ref'] = {'val': 0., 'constraints': {'lower':0., 'upper':10.}}#, 'scaler':0.1}}
        
        dict_pars_MDAO['profile']['dprop'] = {'val': 0., 'constraints': {'lower':-10., 'upper':10.}}#, 'scaler':0.1}}
        dict_pars_MDAO['profile']['Xprop'] = {'val': 2., 'constraints': {'lower':1., 'upper':6.}}#, 'scaler':0.01}}
        
        ### twin rotors ###
        dict_pars_MDAO['twin']={}
        dict_pars_MDAO['twin']['Kh_twin'] = {'val': 1., 'constraints': {'lower':0.75, 'upper':2.}}#, 'scaler':0.01}}
        # dict_pars_MDAO['twin']['Kp_twin'] = {'val': 0.85, 'constraints': {'lower':0.75, 'upper':2.}}#, 'scaler':0.01}}
        dict_pars_MDAO['twin']['Kf_twin'] = {'val': 0.85, 'constraints': {'lower':0.75, 'upper':1.}}#, 'scaler':0.01}}
        dict_pars_MDAO['twin']['Cind_twin'] = {'val': 1., 'constraints': {'lower':1.0, 'upper':4.}}#, 'scaler':0.01}}
        dict_pars_MDAO['twin']['Caxial_twin'] = {'val': 1., 'constraints': {'lower':1.0, 'upper':4.}}#, 'scaler':0.01}}
        dict_pars_MDAO['twin']['A_coaxial'] = {'val': 1.05, 'constraints': {'lower':1.0, 'upper':4.}}#, 'scaler':0.01}}
        
        # Fill all 'val' default values with dict_pars_default from class_NDARCrotor
        # This makes the values here redundant but ensures that as long as
        # dict_pars_default is up to date the defaults here are also correct
        for model, dict_model in dict_pars_MDAO.items():
            for key in dict_pars_MDAO:
                if key in dict_pars_default:
                    dict_pars_MDAO[model][key]['val']=dict_pars_default[key]
        
        return dict_pars_MDAO
    
  
