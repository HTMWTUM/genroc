 ! UH-60A Helicopter
 ! January 2009
 &DEFN action='ident',created='January 2009',title='UH-60A',&END
 !##############################################################################
 ! default helicopter
 &DEFN action='configuration',&END
 &VALUE config='helicopter',rotate=1,&END
 !==============================================================================
 &DEFN quant='Cases',&END
 &VALUE
     title='UH-60A',
     FILE_design='uh60a.design',FILE_perf='uh60a.perf',
     FILE_geometry='uh60a.geom',FILE_sketch='uh60a.dxf',
     FILE_aircraft='uh60a.acd',FILE_solution='uh60a.soln',
     FILE_aero='uh60a.aero',FILE_engine='uh60a.eng',
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Size',&END
 &VALUE
     title='UH-60A',
     SIZE_perf='none',SET_rotor='radius+Vtip+sigma','radius+Vtip+sigma',
     FIX_DGW=1,FIX_WE=0,
     SET_tank='input',SET_SDGW='input',SET_WMTO='input',SET_limit_ds='input',
 &END
 &DEFN quant='OffDesign',&END
 &VALUE title='mission analysis',&END
 &DEFN quant='Performance',&END
 &VALUE title='performance analysis',&END
 !------------------------------------------------------------------------------
 &DEFN quant='Solution',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Cost',&END
 &VALUE
     FuelPrice=2.00,
     Npass=11,
     Nlot=92,Nprod=72,Nprod_eng=1,year_proc=1978,
     f_sec=0.35,KIND_fuse_boom=1,
     Pr_avg=1.6,TBO_eng=1500.,KIND_eng_FADEC=0,
     f_env=0.00725,
 &END
 &DEFN quant='Emissions',&END
 &VALUE &END
 !==============================================================================
 &DEFN quant='Aircraft',&END
 &VALUE
     title='UH-60A Aircraft',
     DGW=16500.,SDGW=16825.,WMTO=22000.,WE=11204.6,nz_ult=5.25,
     altitude=4000.,SET_atmos='temp',temp=95.,
     FIX_drag=0,FIX_DL=0,  ! FIX_drag=1 DoQ, 2 CD, 3 kDrag; FIX_DL=1 DoQV, 2 kDL
        DoQ=35.14,CD=0.01554,kDrag=4.476,  ! UH-60A Airloads (Yeo and Bousman)
        DoQ=25.69,CD=0.01136,kDrag=3.272,  ! baseline 1st year (Yeo and Bousman)
        DoQV=79.2,kDL=0.035,
     INPUT_geom=1,KIND_scale=1,kScale=1,  ! fixed geometry (SL,BL,WL); INPUT_geom=2 for scaled geometry (x/R,y/R,z/R)
        KIND_Ref=1,kRef=1,  ! mr hub reference
      kx=0.12,ky=0.33,kz=0.32,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Systems',&END
 &VALUE
     title='UH-60A Systems',
     Wcrew=725.0,Wtrap=60.0,
     Woful=158.6, ! gun installation
     SET_Wvib=1,Wvib=0.,fWvib=0.02585, ! Wvib=289.6
     SET_Wcont=2,Wcont=-65.85, ! weight statement manufacturing variation -65.7
     Wfc_cc=108.8,Wfc_afcs=78.9,
     fRWfc_nb=1.25,fRWhyd=0.39,
     MODEL_FWfc=1,MODEL_WFWfc=2,fFWfc_nb=0.14,
     Wauxpower=179.9,Winstrument=196.,Wpneumatic=0.,Welectrical=382.0,
     WMEQ=425.8,
     Warmor=136.6,Warmprov=3.7,
     Wfurnish=605.3,Wenviron=54.5,Wload=52.5,
     MODEL_DI=1,kDeIce_elec=0.26,0.26,kDeIce_rotor=0.25,0.08,kDeIce_air=0.006,
     SET_fold=1,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Fuselage',&END
 &VALUE
     title='UH-60A Fuselage',
     SET_length=1,Length_fus=41.33, ! SET_length=4,SET_nose=2,SET_aft=2 for scaled geometry
        SET_nose=1,Length_nose=14.93,fLength_nose=0.55640,
        SET_aft=1,Length_aft=-6.167,fLength_aft=-0.22983,
        fRef_fus=0.3700,Width_fus=7.75,
     SET_Swet=2,  ! input+boom; SET_Swet=3 for scaled geometry
        Swet=346.3,Sproj=115.71,fSwet=0.7035,fSproj=1.0,  ! wetted area = 829.3
        Height_fus=5.75,
        Circum_boom=18.,Width_boom=2.7,
   ! aerodynamics
   ! SET_lift=2,SET_moment=2,SET_side=2,SET_yaw=2,SET_drag=2,SET_Dfit=2,SET_Vdrag=2,SET_Sdrag=2 for scaled aero
   ! reference Swet=829.3, Sproj=303.8 (CDV), length_fus=41.33 (moments)
     DoQ_cont=2.95,  ! UH-60A Airloads, D/q for instrumentation and RDAS (Yeo and Bousman)
     DoQ_cont=0.,    ! baseline 1st year (Yeo and Bousman)
     AoA_zl=0.,AoA_max=35.,SS_max=35.,
     SET_lift=1,dLoQda=95.1,dCLda=0.11467,
     SET_moment=1,MoQ0=-140.6,dMoQda=1604.,CM0=-0.004102,dCMda=0.046798, ! tail off M0/q=-40; rotor+engine M/q=100.6
     SET_side=1,dYoQdb=-137.5,dCYdb=-0.16580,
     SET_yaw=1,NoQ0=0.,dNoQdb=-1146.,CN0=0.,dCNdb=-0.033436,
     SET_drag=1,SET_Dfit=1,SET_Drb=1,SET_Vdrag=1,SET_Sdrag=1,
     DoQ=5.28,DoQ_fit=12.61,CD=0.0063668,CD_fit=0.0152057,  ! D/q_total=35.14, UH-60A Airloads (Yeo and Bousman)
     DoQ=5.28,DoQ_fit=5.31,CD=0.0063668,CD_fit=0.0064030,  ! D/q_total=25.69, baseline 1st year (Yeo and Bousman)
     DoQ_rb=0.,0.,CD_rb=0.,0.,
     DoQV=76.49,CDV=0.40652,  ! D/qV_total=fus+fit+ht=79.2, baseline 1st year (Yeo and Bousman)
     DoQS=35.,CDS=0.042205,
   ! DoQinc=DoQ*Kdrag*alpha^Xdrag=k(1.66*alpha_deg)^2, k=.0095 tail off, k=.0160 tail on (Yeo and Bousman); Kdrag=85.94/5.62=15.3
     MODEL_drag=2,AoA_Dmin=0.,Kdrag=15.3,AoA_tran=25.,
   ! weight
     fWbody_tfold=0.06,fWbody_crash=0.055,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='LandingGear',&END
 &VALUE
     title='UH-60A Landing Gear',
   ! drag
     DoQ=0.,
   ! weight
     nLG=3,fWLG_crash=0.13,fWLG_ret=0.,
 &END
 !==============================================================================
 &DEFN quant='Geometry',&END
 &VALUE
   ! mr hub reference
     loc_rotor(1)%FIX_geom='xyz',
   ! fixed geometry (INPUT_geom=1); SL +aft, BL +right, WL +up
     loc_cg%SL          =30.142,loc_cg%BL          =0.000,loc_cg%WL          =20.683,
     loc_fuselage%SL    =28.792,loc_fuselage%BL    =0.000,loc_fuselage%WL    =19.500,
     loc_gear%SL        =28.792,loc_gear%BL        =0.000,loc_gear%WL        =13.920,
     loc_rotor(1)%SL    =28.433,loc_rotor(1)%BL    =0.000,loc_rotor(1)%WL    =26.250,
     loc_pylon(1)%SL    =28.433,loc_pylon(1)%BL    =0.000,loc_pylon(1)%WL    =23.250,
     loc_rotor(2)%SL    =61.000,loc_rotor(2)%BL    =1.167,loc_rotor(2)%WL    =27.000,
     loc_pylon(2)%SL    = 0.000,loc_pylon(2)%BL    =1.167,loc_pylon(2)%WL    =27.000, ! SET_geom=tailrotor: pylon SL relative hub
     loc_tail(1)%SL     =58.500,loc_tail(1)%BL     =0.000,loc_tail(1)%WL     =20.200,
     loc_tail(2)%SL     =57.833,loc_tail(2)%BL     =0.000,loc_tail(2)%WL     =22.683,
     loc_auxtank(1,1)%SL=28.792,loc_auxtank(1,1)%BL=0.000,loc_auxtank(1,1)%WL=19.500,
     loc_engine(1)%SL   =28.433,loc_engine(1)%BL   =0.000,loc_engine(1)%WL   =23.250,
   ! scaled geometry (INPUT_geom=2); x +aft, y +right, z +up
     loc_cg%XoL          =0.06369,loc_cg%YoL          =0.00000,loc_cg%ZoL          =-0.20747,
     loc_fuselage%XoL    =0.01338,loc_fuselage%YoL    =0.00000,loc_fuselage%ZoL    =-0.25156,
     loc_gear%XoL        =0.01338,loc_gear%YoL        =0.00000,loc_gear%ZoL        =-0.45951,
     loc_rotor(1)%XoL    =0.00000,loc_rotor(1)%YoL    =0.00000,loc_rotor(1)%ZoL    = 0.00000,
     loc_pylon(1)%XoL    =0.00000,loc_pylon(1)%YoL    =0.00000,loc_pylon(1)%ZoL    =-0.11180,
     loc_rotor(2)%XoL    =1.21367,loc_rotor(2)%YoL    =0.04349,loc_rotor(2)%ZoL    = 0.02795,
     loc_pylon(2)%XoL    =0.00000,loc_pylon(2)%YoL    =0.04349,loc_pylon(2)%ZoL    = 0.02795, ! SET_geom=tailrotor: pylon XoL relative hub
     loc_tail(1)%XoL     =1.12052,loc_tail(1)%YoL     =0.00000,loc_tail(1)%ZoL     =-0.22547,
     loc_tail(2)%XoL     =1.09567,loc_tail(2)%YoL     =0.00000,loc_tail(2)%ZoL     =-0.13293,
     loc_auxtank(1,1)%XoL=0.01338,loc_auxtank(1,1)%YoL=0.00000,loc_auxtank(1,1)%ZoL=-0.25156,
     loc_engine(1)%XoL   =0.00000,loc_engine(1)%YoL   =0.00000,loc_engine(1)%ZoL   =-0.11180,
 &END
 !==============================================================================
 &DEFN quant='Rotor 1',&END
 &VALUE
     title='UH-60A Main Rotor',
     diskload=7.30,CWs=.0870, ! design conditions
     Vtip_ref=724.66, ! 257.9 rpm
        INPUT_Vtip=2,fRPM_cruise=1.,fRPM_man=1.,fRPM_oei=1.,fRPM_xmsn=1.,
     Plimit_rs=2828.,fPlimit_rs=2.,
     radius=26.833,sigma=0.08317,rotate=1,nblade=4,
     SET_chord=3,
     SET_twist=2,  ! twistL=-18.,
        nprop=21,
        rprop=  0.0000,  0.2329,  0.4658,  0.4799,  0.4801,  0.4969,  0.7250,  0.7450,  0.7604,  0.8230,  0.8399,
                0.8401,  0.8540,  0.8629,  0.8630,  0.8793,  0.9037,  0.9286,  0.9506,  0.9756,  1.0000,
        twist= 13.1438,  9.0730,  5.0031,  4.7567,  4.7532,  4.4598,  0.4731,  0.1236, -0.1460, -0.8730, -1.0154,
               -1.0171, -1.1344, -1.2092, -1.2090, -1.7150, -2.7960, -3.6260, -3.4000, -2.4590, -1.1280,
      ! with -1 deg for SC1094R8
        twist= 13.1438,  9.0730,  5.0031,  4.7567,  3.7532,  3.4598, -0.5269, -0.8764, -1.1460, -1.8730, -2.0154,
               -1.0171, -1.1344, -1.2092, -1.2090, -1.7150, -2.7960, -3.6260, -3.4000, -2.4590, -1.1280,
        fchord= 0.9870,  0.9870,  0.9870,  0.9915,  0.9915,  0.9968,  0.9968,  1.0611,  1.0611,  1.0611,  1.0558,
                1.0556,  1.0513,  1.0513,  0.9870,  0.9870,  0.9870,  0.9870,  1.0503,  1.0503,  1.0503,
     KIND_hub=1,flapfreq=1.0354,gamma=7.07,precone=0.,
     dclda=5.7,tiploss=.97,xroot=0.1925,thick=0.095,
     incid_hub=-3.,cant_hub=0.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! induced power
     Ki_hover=1.125,Ki_climb=1.125,Ki_edge=2.0,Ki_min=1.1,
     CTs_Hind=.10,kh1=1.25,kh2=0.,
     mu_edge=0.35,ke1=.8,ke2=0.,ke3=1.,Xe=4.5,
   ! profile power
     MODEL_basic=2,
     ncd=24,
        CTs_cd=0.,.01,.02,.03,.04,.05,.06,.07,.08,.09,.10,.11,.12,.13,.14,.15,.16,.17,.18,.19,.20,.21,.22,.23,
        cd=0.00955,0.00910,0.00875,0.00850,0.00835,0.00830,0.00835,0.00850,0.00875,0.00913,0.00966,0.01036,
           0.01125,0.01236,0.01372,0.01535,0.01727,0.01950,0.02207,0.02501,0.02834,0.03208,0.03625,0.04088,
     CTs_Dmin=.05,d0_hel=.0083,d0_prop=.0083,d1_hel=0.,d1_prop=0.,d2_hel=.5,d2_prop=.5,
     CTs_sep=.07,dsep=4.,Xsep=3.,
     MODEL_stall=1,nstall=10,
        mu_stall=0.,.05,.10,.15,.20,.25,.30,.35,.40,.80,
        CTs_stall=.11,.10,.09,.085,.08,.0775,.075,.0725,.07,.07,
     fstall=1.,dstall1=2.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,Mdd0=.73,Mddcl=0.,dm1=.005,dm2=.95,Xm=3.,
   ! drag
   ! SET_Spylon=2,SET_Dhub=2,SET_Vhub=2,SET_Dpylon=2,SET_Vpylon=2 for scaled aero
   ! reference Arotor=2262. (hub), Swet_pylon=115. (pylon)
     SET_Spylon=1,Swet_pylon=115.,kSwet_pylon=0.93295,
     SET_Dhub=1,SET_Vhub=1,SET_Dpylon=1,SET_Vpylon=1,
     DoQ_hub=5.83,CD_hub=0.0025774,kDrag_hub=0.7425,  ! hub D/q=5.83 (ADS10); SET_Dhub=1 DoQ_hub, 2 CD_hub, 3 kDrag_hub
     DoQV_hub=5.83,CDV_hub=0.0025774,
     DoQ_pylon=4.14,CD_pylon=0.036000,DoQV_pylon=4.14,CDV_pylon=0.036000,
   ! weight
     fWfold=0.04,rblade=0.614,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Rotor 2',&END
 &VALUE
     title='UH-60A Tail Rotor',
     diskload=17.36,CWs=.1026, ! design conditions
     INPUT_gear=2,Vtip_ref=685.64,gear=4.616, ! INPUT_gear=1 Vtip, 2 gear
     Plimit_rs=500.,fPlimit_rs=2.,
     radius=5.5,sigma=0.1875,rotate=1,nblade=4,clearance_tr=0.2333,
     SET_chord=2,taper=1.,
     SET_twist=2,
        nprop=12,
        rprop= 0.000, 0.550, 0.575, 0.625, 0.675, 0.725, 0.775, 0.825, 0.865, 0.890, 0.910, 1.000,
        twist= 1.280, 1.280, 1.380, 1.580, 1.260, 0.420,-0.570,-1.700,-3.700,-4.930,-5.050,-5.050,
        fchord=12*1.,
     KIND_hub=1,flapfreq=1.14,gamma=2.01,precone=0.,delta3=35.,SET_Iblade=0,
     dclda=5.7,tiploss=.97,xroot=0.55,thick=.095,
     incid_hub=0.,cant_hub=20.,
   ! performance
     MODEL_Ftpp=2,MODEL_Fpro=2,
   ! induced power (main rotor model for forward flight)
     Ki_hover=1.65,Ki_climb=1.65,Ki_edge=2.0,Ki_min=1.65,
     CTs_Hind=.05,kh1=0.,kh2=80.,
     mu_edge=0.35,ke1=.8,ke2=0.,ke3=1.,Xe=4.5,
   ! profile power
     MODEL_basic=2,
     ncd=24,
        CTs_cd=0.,.01,.02,.03,.04,.05,.06,.07,.08,.09,.10,.11,.12,.13,.14,.15,.16,.17,.18,.19,.20,.21,.22,.23,
        cd=0.01244,0.01181,0.01136,0.01109,0.01100,0.01109,0.01136,0.01183,0.01260,0.01379,0.01552,0.01791,
           0.02108,0.02515,0.03024,0.03647,0.04396,0.05283,0.06320,0.07519,0.08892,0.10451,0.12208,0.14175,
     CTs_Dmin=0.04,d0_hel=.0090,d0_prop=.0090,d1_hel=0.,d1_prop=0.,d2_hel=.9,d2_prop=.9,CTs_sep=.06,dsep=20.,Xsep=3.,
     MODEL_stall=1,nstall=10,
        mu_stall=0.,.05,.10,.15,.20,.25,.30,.35,.40,.80,
        CTs_stall=.09,.07,.064,.062,.062,.061,.06,.058,.056,.056,
     fstall=1.,dstall1=5.,dstall2=40.,Xstall1=2.,Xstall2=3.,
     MODEL_comp=1,Mdd0=.68,Mddcl=0.,dm1=.005,dm2=.9,Xm=3.,
   ! no interference (vertical tail at low speed)
     MODEL_int=0,
   ! drag
   ! SET_Dhub=2,SET_Vhub=2 for scaled aero; reference Arotor=95.0 (hub)
     SET_Spylon=1,Swet_pylon=0.,
     SET_Dhub=1,SET_Vhub=1,SET_Dpylon=1,SET_Vpylon=1,
     DoQ_hub=2.90,CD_hub=0.030516,  ! hub D/q=2.90 (ADS10); SET_Dhub=1 DoQ_hub, 2 CD_hub
     DoQV_hub=2.90,CDV_hub=0.030516,
     DoQ_pylon=0.,DoQV_pylon=0.,
   ! weight
     fWfold=0.,
 &END
 !==============================================================================
 &DEFN quant='Tail 1',&END
 &VALUE
     title='UH-60A Horizontal Tail',
     SET_tail='area+span',  ! SET_tail='vol+aspect' for scaled geometry
     area=45.,span=14.33,TailVol=0.021025,AspectRatio=4.5633,
     taper=1.,sweep=0.,dihedral=0.,thick=0.14,fchord_cont=0.,
     cant=0.,incid=0.,
   ! tail incidence for 50% collective
     nVincid=4,incid=40.,40.,2.,-5.,Vincid=0.,30.,80.,150.,
   ! aerodynamics
   ! SET_drag=2,SET_Vdrag=2 for scaled aero; reference Sht
     AoA_zl=1.,CLmax=0.8,SET_lift=3,dCLda=2.27,Tind=1.,Eind=.9,
     SET_drag=1,SET_Vdrag=1,
     DoQ=.6,CD=0.013333,DoQV=.6,CDV=0.013333,  ! CDV=2.0 not used
     AoA_Dmin=0.,MODEL_drag=0,
   ! weight
     Vdive=210.,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='Tail 2',&END
 &VALUE
     title='UH-60A Vertical Tail',
     SET_tail='area+span',  ! SET_tail='vol+aspect' for scaled geometry
     area=32.3,span=8.17,TailVol=0.014736,AspectRatio=2.0665,
     taper=0.8,sweep=41.,dihedral=0.,thick=0.21,fchord_cont=0.,
     cant=0.,incid=3.,
   ! aerodynamics
   ! SET_drag=2,SET_Vdrag=2 for scaled aero; reference Svt
     AoA_zl=0.,CLmax=1.,SET_lift=3,dCLda=1.44,Tind=1.,Eind=.9,
     SET_drag=1,SET_Vdrag=1,
     DoQ=.6,CD=0.018576,DoQV=.6,CDV=0.018576,  ! CDV=2.0 not used
     AoA_Dmin=0.,MODEL_drag=0,
   ! weight
     Vdive=210.,
     place_AntiQ=2, ! place=tail
 &END
 !==============================================================================
 &DEFN quant='FuelTank',&END
 &VALUE
     title='UH-60A Fuel Tank',
     Wfuel_cap=2338.,  ! 2x179.85=359.7 gal
   ! aux tanks
     Waux_cap=4953.,DoQ_auxtank=0.,fWauxtank=0.11,  ! 762 gal
   ! weight
     ntank_int=2,nplumb=2,Ktoler=1.8,
     K0_plumb=120.,K1_plumb=3.,
 &END
 !==============================================================================
 &DEFN quant='Propulsion',&END
 &VALUE
     title='UH-60A Propulsion',
   ! losses
     MODEL_Xloss=1,
     fPloss_xmsn=0.03659,Ploss_windage=0.00,Pacc_0=65.6,Pacc_d=0.,Pacc_n=0.,fPacc_ECU=0.,fPacc_IRfan=0.,  ! AEFA 87-24 fig E-20
     fPloss_xmsn=0.01637,Ploss_windage=37.9,Pacc_0=31.4,Pacc_d=0.,Pacc_n=0.,fPacc_ECU=0.,fPacc_IRfan=0.,  ! Sinsay UH-60M
     fPloss_xmsn=0.01637,Ploss_windage=0.00,Pacc_0=65.8,Pacc_d=0.,Pacc_n=0.,fPacc_ECU=0.,fPacc_IRfan=0.,  ! Sikorsky
     fPloss_xmsn=0.01610,Ploss_windage=0.00,Pacc_0=66.0,Pacc_d=0.,Pacc_n=0.,fPacc_ECU=0.,fPacc_IRfan=0.,  ! Bousman
   ! geometry
     SET_length=1,Length_ds=32.6,fLength_ds=0.94538,  ! SET_length=2 for scaled geometry
   ! torque limit
     Plimit_ds=2828.,fPlimit_ds=0.8810,
   ! weight
     ngearbox=5,ndriveshaft=4,fShaft=0.13,
 &END
 !------------------------------------------------------------------------------
 &DEFN quant='EngineGroup',&END
 &VALUE
     title='UH-60A Engine Group',
     nEngine=2,nEngine_main=2,
     IDENT_engine='T700-GE-700',
     Peng=1605.,rating_to='IRP',
     SET_Swet=1,Swet=56.2,kSwet=0.80466, ! Snac=112.4; SET_Swet=2 for scaled geometry
   ! torque limit
     Plimit_es=2828.,fPlimit_es=0.8810,
   ! installation
     eta_d=0.996,
     Kffd=1.05,fPloss_inlet=.007,fPloss_ps=0.,
     fPloss_exh=.02,fMF_auxair=.01,eta_auxair=.75,
     fPloss_exh_IRon=.02,fMF_auxair_IRon=.01,eta_auxair_IRon=.75,
   ! drag
   ! SET_drag=2,SET_Vdrag=2 for scaled aero; reference Snac
     SET_drag=1,SET_Vdrag=1,
     DoQ=1.03,CD=0.009164,DoQV=1.03,CDV=0.009164,
   ! weight
     fWpylon=0.,fWair=0.46,
     Kwt0_exh=0.,Kwt1_exh=0.0623,
     MODEL_lub=1,
 &END
 !------------------------------------------------------------------------------
 ! two T700-GE-700 engines, 1605 hp (IRP, uninstalled), Nspec=20000, 0.27 lb/hp
 ! based on GEN2000 model
 !------------------------------------------------------------------------------
 &DEFN quant='EngineModel',&END
 &VALUE
 ! Turboshaft Engine Model
      title = 'Turboshaft Engine Math Model Data (T700-GE-700)',
      notes = 'Approximate Model',
      ident = 'T700-GE-700',
 ! Engine Ratings
      nrate = 2,
      rating = 'MCP  ', 'IRP  ',
 ! Weight
      Kwt0_eng =       0.00,
      Kwt1_eng =       0.27,
      Kwt2_eng =       0.00,
      Xwt_eng  =       0.00,
 ! Reference
      P0_ref     =     1313.00,     1605.00,
      SP0_ref    =      120.00,      140.00,
      Pmech_ref  =     2000.00,     2000.00,
      sfc0C_ref  =        0.47,
      SF0C_ref   =        8.00,
      Nspec_ref  =    20000.00,
      Nopt0C_ref =        0.00,
 ! Scaling, MF_ref =     10.9098
      MF_limit    =       30.00,
      SP0C_limit  =      160.00,
      sfc0C_limit =        0.43,
      KNspec      =     9000.00,
 ! Optimum Power Turbine Speed (linear)
      MODEL_OptN = 1,
      KNoptA =     1.,
      KNoptB =     0.,
      XNeta  =     2.,
 ! Single Set of Input Parameters
      INPUT_param = 1,
 ! Power Available
      INPUT_lin = 1,
    ! specific power
      Nspa =  1,  1,
      Kspa0(1,1) =       3.80,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Kspa0(1,2) =       3.20,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Kspa1(1,1) =      -2.80,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Kspa1(1,2) =      -2.20,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Xspa0(1,1) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Xspa0(1,2) =      -0.22,   0.00,   0.00,   0.00,   0.00,  ! IRP  
      Xspa1(1,1) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! MCP  
      Xspa1(1,2) =       0.00,   0.00,   0.00,   0.00,   0.00,  ! IRP  
    ! mass flow
      Nmfa =  2,  2,
      Kmfa0(1,1) =       0.30,   0.70,   0.00,   0.00,   0.00,  ! MCP  
      Kmfa0(1,2) =       0.29,   0.45,   0.00,   0.00,   0.00,  ! IRP  
      Kmfa1(1,1) =      -0.30,  -0.70,   0.00,   0.00,   0.00,  ! MCP  
      Kmfa1(1,2) =      -0.29,  -0.45,   0.00,   0.00,   0.00,  ! IRP  
      Xmfa0(1,1) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! MCP  
      Xmfa0(1,2) =       1.05,   1.10,   0.00,   0.00,   0.00,  ! IRP  
      Xmfa1(1,1) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! MCP  
      Xmfa1(1,2) =       0.00,  -0.01,   0.00,   0.00,   0.00,  ! IRP  
 ! Performance at Power Required
    ! fuel flow         mass flow         gross jet thrust  net jet thrust
      Kffq0 =   0.20,   Kmfq0 =   0.60,   Kfgq0 =   0.20,   Kfgr0 =   0.80,
      Kffq1 =   0.80,   Kmfq1 =   0.78,   Kfgq1 =   0.80,   Kfgr1 =   0.60,
      Kffq2 =   0.00,   Kmfq2 =  -0.48,   Kfgq2 =   0.00,   Kfgr2 =   0.00,
      Kffq3 =   0.00,   Kmfq3 =   0.10,   Kfgq3 =   0.00,   Kfgr3 =   0.00,
      Xffq  =   1.30,   Xmfq  =   3.50,   Xfgq  =   2.00,
 &END
 !==============================================================================
 &DEFN quant='TechFactors',&END
 &VALUE
   ! cost
     TECH_cost_af=0.87,TECH_cost_maint=1.,
   ! flight control
     TECH_FWfc_nb=1.1548,TECH_FWfc_mb=1.1551,TECH_FWhyd=0.,
     TECH_RWfc_b=1.0550,TECH_RWfc_mb=1.1716,TECH_RWfc_nb=1.1727,TECH_RWhyd=1.1784,
   ! anti-icing
     TECH_DIelect=1.0083,TECH_DIsys=1.0064,
   ! fuselage
     TECH_body=1.0308,TECH_mar=1.,TECH_press=1.,TECH_crash=0.9987,TECH_ftfold=0.9396,TECH_fwfold=1.,
     TECH_LG=0.7354,TECH_LGret=1.,TECH_LGcrash=0.9773,
   ! rotor profile power
     TECH_drag=1.,1.,
   ! rotor
     TECH_blade=1.0162,1.,TECH_hub=0.9827,1.,TECH_spin=1.,1.,TECH_rfold=0.9807,1.,TECH_tr=1.,1.1753,TECH_aux=1.,1.,
   ! tails
     TECH_tail=0.9405,2.4662,
   ! fuel tank
     TECH_tank=0.8296,TECH_plumb=1.0181,
   ! drive system
     TECH_gb=0.8978,TECH_rs=0.9734,TECH_ds=0.8494,TECH_rb=0.,
   ! engine group
     TECH_eng=1.0039,TECH_cowl=0.9127,TECH_pylon=1.,TECH_supt=1.2693,TECH_air=1.2730,TECH_exh=1.,TECH_acc=0.7086,
 &END
 !##############################################################################
 &DEFN action='endoffile',&END
