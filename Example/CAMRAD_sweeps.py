# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import numpy as np
import pandas as pd
import operator
from functools import reduce

import tom_ndarc
from genroc.utl.camrad import output_camrad, camrad_sweep, sample_full_ivc


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)
def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value
   


def main():
    jobname='cal_sweeps'
    jobfile='path/to/job.csh file'
    dir_job = os.path.dirname( __file__ )
    dir_output = os.path.abspath(os.path.join(dir_job, 'Output')) 
    if not os.path.exists(dir_output):
        os.makedirs(dir_output)
    
    dir_csv=os.path.join(dir_job, 'csv-backup')
    if not os.path.exists(dir_csv):
        os.makedirs(dir_csv)
        
    dir_plot=os.path.join(dir_job, 'plots')
    if not os.path.exists(dir_plot):
        os.makedirs(dir_plot)
    
    
    ncases=1
    nrot=2
    lst_cases=[1]
    lst_r=[1,2]
    outputs=output_camrad(lst_cases=lst_cases, lst_r=lst_r)
    
    dict_cases={}
    hover={}
    hover['tow_hov']  = {'accesstext': 'CASE 1%SHELL%AIRFRAME:STRUCTURE%WEIGHT', 'values':list(np.linspace(30,70,9)), 'metadict':{'val':50.}}
    
    edge={}
    edge['speed']  = {'accesstext': 'CASE 1%SHELL%TRIM%SPEED', 'values':list(np.linspace(0,25,11)), 'metadict':{'val':0.}}
    edge['tow_cr']  = {'accesstext': 'CASE 1%SHELL%AIRFRAME:STRUCTURE%WEIGHT', 'values':list(np.linspace(30,70,5)), 'metadict':{'val':50.}}
    edge['rpm']  = {'accesstext': 'CASE 1%SHELL%TRIM%RPM', 'values':list(np.linspace(550,750,3)), 'metadict':{'val':650.}}
    dict_cases=dict(hover=hover,edge=edge)
    
    for case, casedict in dict_cases.items():
        cases=[casedict]
        samples=sample_full_ivc(cases, parallel_cases=False)
        df = camrad_sweep(jobname, jobfile, cases, dir_output=dir_output, outputs=outputs, setenv = {}, samples=samples, replacenan=True)
        df.to_csv(case+'sweep.csv')
    
    return df

df=main()